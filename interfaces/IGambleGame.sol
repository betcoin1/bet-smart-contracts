// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

import "../contracts/BetTypes.sol";

interface IGammbleGame  {
function createEvent(
        uint256 _expire, 
        uint256 _orderDeadLine, 
        string  memory _assetSymbol,
        Outcome[2] memory _outcomes,
        address _creator
)  external;

function createEvent2(
        uint256 _expire, 
        uint256 _orderDeadLine, 
        string  memory _assetSymbol,
        Outcome[] memory _outcomes,
        address _creator
)  external; 
        
function incBetCountForOutcome(uint256 _evevntId, uint256 _outcomeId, uint256 _amount) external returns(bool);

function decBetCountForOutcome(uint256 _evevntId, uint256 _outcomeId, uint256 _amount) external returns(bool);

function checkStateWithSave(uint256 _evevntId) external;

function getEvent(uint256 _id) external view returns(GamblingEvent memory);

function getEventCount() external view returns(uint256); 

function isEventExist(uint256 _id) external view returns(bool);

function isWinWithAmount(
        uint256 _eventId, 
        uint8 _outcomeIndex, 
        uint256 _betAmount
    ) 
        external 
        returns (bool, uint256);

}