// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

interface IRewardModel {
    function getFundPercent() external view returns (uint256);
    function getEventCreatorPercent() external view returns (uint256);
}