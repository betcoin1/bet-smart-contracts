// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

interface IGamblePool  {
    function totalStaked() external view returns(uint256);
    
    // Main acount with amount of tokens that are frozen in bets.
    // Sub Account for totalStaked
    function inBetsAmount() external view returns(uint256);  
    
    //Main acount with amount of tokens in all funds
    function fundBalance() external view returns(uint256);

    function getGamesCount() external view returns (uint256);

    function getUsersBetsCount(address _user) external view returns (uint256);

    function getUsersBetAmountByIndex(address _user, uint256 _index) 
        external 
        view 
        returns (uint256);

    function getUserBalance(address _user) external view returns(uint256);

    //function accruedPoints(address _user) external view returns(uint256 points);

    function SCALE() external view returns (uint256);

    function owner() external view returns (address);

    function withdraw(uint256 _amount) external;

    function projectToken() external view returns(address);
    function lastSettledCreator() external view returns(address);

}