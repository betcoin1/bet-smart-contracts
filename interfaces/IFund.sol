// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;


interface IFund {

    function registerFund() external returns (bool); 

    function joinFund(address _user) external  returns (bool);

    function claimReward(address _user) external returns (uint256);

    function claimRewardForEpoch(address _user, uint256 _epoch) external returns (uint256);

    function updateUserState(address _user) external;

    function newReward(uint256 _amount) external returns (bool);

    function isJoined(address _user) external view returns (bool);

    function getAvailableReward(address _user) external view returns (uint256);

    function getAvailableReward(address _user, uint256 _epoch) external view returns (uint256); 
}
