// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

interface IBetPriceProvider {
    function getLastPrice(address _amulet) external view returns (uint256);
    function getLastPriceByPairName(bytes32 _nameHash) external view returns (int256); 
}