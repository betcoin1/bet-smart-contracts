import pytest
import logging, time
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

START_CUSTOM_PRICE = 5e18
def test_oracle_debug_mode_bystr(accounts, oracle):
    last_price = oracle.getLastPriceByPairNameStr('BTC/USD')
    tx = oracle.setCustomPrice(START_CUSTOM_PRICE)
    last_price = oracle.getLastPriceByPairNameStr('BTC/USD')
    logging.info(last_price)
    assert last_price==START_CUSTOM_PRICE

