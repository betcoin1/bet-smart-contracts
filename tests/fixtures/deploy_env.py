import pytest


@pytest.fixture(scope="module")
def bettoken(accounts, BetToken):
    token = accounts[0].deploy(BetToken)
    yield token

@pytest.fixture(scope="module")
def gamblepool(accounts, bettoken, GamblePool):
    pool = accounts[0].deploy(GamblePool, bettoken.address)
    yield pool

@pytest.fixture(scope="module")
def mockgamblepool(accounts, bettoken, MockGamblePool):
    pool = accounts[0].deploy(MockGamblePool, bettoken.address)
    yield pool

@pytest.fixture(scope="module")
def weth(accounts, TokenMock):
    a = accounts[0].deploy(TokenMock, "Dummy WETH", "WETH")
    yield a 

@pytest.fixture(scope="module")
def oracle(accounts, BetPriceProvider, weth):
    oracle = accounts[0].deploy(BetPriceProvider, weth.address)
    yield oracle


@pytest.fixture(scope="module")
def binaryevents(accounts, BinaryEvents, gamblepool, oracle):
    e = accounts[0].deploy(BinaryEvents, gamblepool.address)
    e.setOracle(oracle.address, {'from':accounts[0]})
    yield e    

@pytest.fixture(scope="module")
def binaryeventMock(accounts, BinaryEvents, oracle):
    e = accounts[0].deploy(BinaryEvents, accounts[0])
    e.setOracle(oracle.address, {'from':accounts[0]})
    yield e

@pytest.fixture(scope="module")
def rewardmodel(accounts, RewardModel01):
    m = accounts[0].deploy(RewardModel01)
    yield m


@pytest.fixture(scope="module")
def fundHolders01(accounts, FundHolders01):
    f = accounts[0].deploy(FundHolders01)
    yield f

@pytest.fixture(scope="module")
def fundHiStake01(accounts, FundHiStaking01):
    f = accounts[0].deploy(FundHiStaking01)
    yield f

@pytest.fixture(scope="module")
def fundBurn01(accounts, FundBurn01, bettoken):
    f = accounts[0].deploy(FundBurn01)
    bettoken.addMinter(f, {'from':accounts[0]})
    yield f

@pytest.fixture(scope="module")
def fundFee(accounts, FundFee):
    f = accounts[0].deploy(FundFee)
    yield f

@pytest.fixture(scope="module")
def fundLottery(accounts, FundFee):
    s = accounts[0].deploy(FundFee)
    yield s

@pytest.fixture(scope="module")
def fundCreators01(accounts, FundCreators01):
    f = accounts[0].deploy(FundCreators01)
    yield f
