import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
from createFundAmount import createFundAmount
from createFunds import createFunds
LOGGER = logging.getLogger(__name__)

rewards = []
total_percent = 0
aCount = 8
amount_mint = 100e18
bet_amount = 10e18


#8 accounts make bets
#amounts of bet are not equal
#acc0 make very small bet than cancel bet
#accounts claim fund
def test_create_Fund_Amount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	#prepare test data
	#create funds
	createFunds(accounts, gamblepool, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery)

	#add game
	gamblepool.addGame(binaryevents.address, 
		'Binary Events1', 
		rewardmodel.address, 
		{"from": accounts[0]})

	# create event for game
	gamblepool.createEvent(
		0, #_gameId
		chain.time() + 100 ,
		chain.time() + 66 ,
		'BTC/USD',
		[
			[1,4815987599256,0,0,0], #MoreOrEqual
			[4,4815987599256,0,0,0]  #less
		],
		{'from':accounts[0]}
	)
	eventId = binaryevents.getEventCount() - 1

	# stake
	i = 0
	while i < aCount:
		bettoken.mint(accounts[i], amount_mint, {"from": accounts[0]})
		bettoken.approve(gamblepool.address, 100e18, {"from": accounts[i]})
		gamblepool.stake(100e18, {"from": accounts[i]})
		i += 1

	i = 1  #because acc0 make very small bet
	#acc 0 makes very small bet
	gamblepool.makeBet(0, eventId, 0, gamblepool.MIN_BET_AMOUNT(), {"from":accounts[0]})
	bet_quantity = 0
	#logging.info('before_balance0 = {}'.format(before_balance0))
	while i < aCount:
		gamblepool.makeBet(0, eventId, i%2, bet_amount, {"from":accounts[i]})
		bet_quantity += 1
		i +=1
	
	#cancel bet for acc 0
	gamblepool.cancelBet(0, {"from":accounts[0]})
	logging.info('canc_bet = {}'.format(gamblepool.getUsersBetByIndex(accounts[0], 0)))

	#set price for pair
	price = 4815987599260
	oracle.setCustomPrice(price, {"from": accounts[0]})

	#move date to after deadline
	chain.sleep(67)
	chain.mine()
	gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
	checkEvent(accounts, binaryevents, eventId, ['state', 'oraclePrice'], [1, 0]) #check status Hold and oraclePrice

	#move date to after expire date
	chain.sleep(101)
	chain.mine()

	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [0]) #check oraclePrice
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	#change status
	binaryevents.checkStateWithSave(eventId, {"from": accounts[1]})

	#check outcome status
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	checkOutcome(accounts, binaryevents, eventId, 0, ['isWin'], [False])  #check outcome0 result
	checkOutcome(accounts, binaryevents, eventId, 1, ['isWin'], [True])  #check outcome1 result
	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [price]) #check oraclePrice
	
	#call climBet
	i = 1   #because acc 0 cancelled
	while i < bet_quantity:
		logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
		all_bets_amount = gamblepool.inBetsAmount()
		tx = gamblepool.claimBet(gamblepool.getUsersBetsCount(accounts[i].address) - 1, {"from": accounts[i]}) 
		checkBet(accounts, gamblepool, accounts[i].address, 0, ['currentState', 'result'], [2, 2-(i%2)])
		if i%2 != 0:
			prize = tx.events['WinClaimed']['amount']
			logging.info('prize = {}'.format(prize))
		i += 1

#close event 0
def test_settle_event_0(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	before_inBetsAmount = gamblepool.inBetsAmount()
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

	fundHiStake01.closeCurrentEpoch()

#claim fund
def test_claim_HiFund_0(accounts, binaryevents,gamblepool, fundHiStake01):
	logging.info('getAvailableReward(accounts[0], 0) = {}'.format(fundHiStake01.getAvailableReward(accounts[0], 0)))
	i = 0
	while i < aCount:
		bba = gamblepool.getUserBalance(accounts[i])
		bbf = gamblepool.getUserBalance(fundHiStake01)
		brc = fundHiStake01.getRewardEpoch(0)[1]
		gamblepool.claimFund(2, 0, {"from": accounts[i]})
		logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(i, fundHiStake01.getUser(accounts[i])))
		logging.info('fundHiStake01.getRewardEpoch(0)= {}'.format(fundHiStake01.getRewardEpoch(0)))
		assert bba < gamblepool.getUserBalance(accounts[i])
		assert bbf > gamblepool.getUserBalance(fundHiStake01)
		assert fundHiStake01.getUser(accounts[i])[0][3] > 0
		assert fundHiStake01.getRewardEpoch(0)[1] > brc

		i += 1
	
	logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(0, fundHiStake01.getUser(accounts[0])))
	logging.info('fundHiStake01.getRewardEpoch(0)= {}'.format(fundHiStake01.getRewardEpoch(0)))
	

	#check rewardAmount and rewardClaimed
	assert round(fundHiStake01.getRewardEpoch(0)[0]/10) == round(fundHiStake01.getRewardEpoch(0)[1]/10)
	assert fundHiStake01.getUser(accounts[0])[0][3] > 0