import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0

def test_gamble_cancelBet(accounts, binaryevents,gamblepool, bettoken):
    gamblepool.addGame(
        binaryevents.address, 
        'Binary Events', 
        '0x0000000000000000000000000000000000000000',
    	{"from": accounts[0]})
    # create event for game
    gamblepool.createEvent(
        0, #_gameId
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        {'from':accounts[0]}
    )
    eventId = binaryevents.getEventCount() - 1

    # stake
    #acc 0
    bettoken.mint(accounts[0], 10000, {"from": accounts[0]})
    bettoken.approve(gamblepool.address, 10000, {"from": accounts[0]})
    gamblepool.stake(10000, {"from": accounts[0]})

    #nonexist bet_id
    with reverts(""):
        gamblepool.cancelBet(1,{'from':accounts[0]})

    #make two bets
    bet_amount = 1000
    tx = gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
    tx = gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
    bet_id = gamblepool.getUsersBetsCount(accounts[0].address) - 1

    #account does not own this bet_id 
    with reverts(""):
        gamblepool.cancelBet(bet_id,{'from':accounts[1]})

    
    # cancel bet one bet, one outcome, status is Active
def test_gamble_cancelBet_1(accounts, binaryevents, gamblepool):
    # change status of event - try to cancel bet for outcome 0 from account0
    before_balance = gamblepool.getUserBalance(accounts[0].address)
    bet_id = gamblepool.getUsersBetsCount(accounts[0].address) - 1
    bet_amount = 1000
    all_bets_amount = gamblepool.inBetsAmount()
    gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 0 #check status Active
    assert binaryevents.getEvent(eventId)[5][0][3] == bet_id; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bet_id + 1 #check bet quantity for acc 0
    penalty = (bet_amount/100)*gamblepool.CANCEL_BET_PENALTY_PERCENT()
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance + bet_amount - penalty  # check balance after the cancellation
    assert gamblepool.fundBalance() == bet_amount/100*gamblepool.CANCEL_BET_PENALTY_PERCENT() #check funds
    assert gamblepool.inBetsAmount() == all_bets_amount - bet_amount # check collected coins of bets 

    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    # check bet
    assert bet[0] == binaryevents.address #check contract address in event
    assert bet[1] == eventId # check event in bet
    assert bet[2] == 0 # check outcome in bet
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 1 # check bet status
    assert bet[6] == 0 # check bet result 

    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id - 1)
    # check bet
    assert bet[0] == binaryevents.address #check contract address in event
    assert bet[1] == eventId # check event in bet
    assert bet[2] == 0 # check outcome in bet
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 0 # check bet status
    assert bet[6] == 0 # check bet result 