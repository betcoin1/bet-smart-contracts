import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
LOGGER = logging.getLogger(__name__)
#one game,  one events,  two acc make 7 bets on different outcomes. Outcome 1 won, acc1 got prize
def test_gamble_claimBet_2(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel):
	#prepare test data
	#add game
	gamblepool.addGame(binaryevents.address, 
		'Binary Events1', 
		rewardmodel.address, 
		{"from": accounts[0]})
	# create event for game
	gamblepool.createEvent(
		0, #_gameId
		chain.time() + 100 ,
		chain.time() + 66 ,
		'BTC/USD',
		[
			[1,4815987599200,0,0,0], #MoreOrEqual
			[4,4815987599200,0,0,0]  #less
		],
		{'from':accounts[0]}
	)
	eventId = binaryevents.getEventCount() - 1

	# stake
	#acc 0
	bettoken.mint(accounts[0], 100000, {"from": accounts[0]})
	bettoken.approve(gamblepool.address, 100000, {"from": accounts[0]})
	gamblepool.stake(100000, {"from": accounts[0]})
	#acc1
	bettoken.mint(accounts[1], 100000, {"from": accounts[0]})
	bettoken.approve(gamblepool.address, 100000, {"from": accounts[1]})
	gamblepool.stake(100000, {"from": accounts[1]})

	i = 0
	bet_amount = 1000
	bets_amount0 = 0
	bets_amount1 = 0
	before_balance0 = gamblepool.getUserBalance(accounts[0].address)
	before_balance1 = gamblepool.getUserBalance(accounts[1].address)
	logging.info('before_balance0 = {}'.format(before_balance0))
	logging.info('before_balance1 = {}'.format(before_balance1))
	while i < 7:
		gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]}) #first outcome
		gamblepool.makeBet(0, eventId, 1, bet_amount, {"from":accounts[1]})	#second outcome
		bets_amount0 += bet_amount
		bets_amount1 += bet_amount
		i +=1

	logging.info('after_balance0 = {}'.format(gamblepool.getUserBalance(accounts[0].address)))
	logging.info('after_balance1 = {}'.format(gamblepool.getUserBalance(accounts[1].address)))
	#set price for pair
	price = 4815987599260
	oracle.setCustomPrice(price, {"from": accounts[0]})

	#move date to after deadline
	chain.sleep(67)
	chain.mine()
	gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
	checkEvent(accounts, binaryevents, eventId, ['state', 'oraclePrice'], [1, 0]) #check status Hold and oraclePrice
	assert gamblepool.getUserBalance(accounts[0].address) + bets_amount0 == before_balance0 #check balance acc 0
	assert gamblepool.getUserBalance(accounts[1].address) + bets_amount1 == before_balance1 #check balance acc 1

	bet_quantity = gamblepool.getUsersBetsCount(accounts[0].address) - 1 #for first event
   
	i = 0
	while i < bet_quantity:
		checkBet(accounts, gamblepool, accounts[0].address, i, ['eventId', 'eventOutcomeIndex', 'betAmount', 'currentState', 'result'], [eventId, 0, bet_amount, 0, 0])
		checkBet(accounts, gamblepool, accounts[1].address, i, ['eventId', 'eventOutcomeIndex', 'betAmount', 'currentState', 'result'], [eventId, 1, bet_amount, 0, 0])
		i += 1

	#move date to after expire date
	chain.sleep(101)
	chain.mine()

	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [0]) #check oraclePrice
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	#change status
	binaryevents.checkStateWithSave(eventId, {"from": accounts[1]})

	#check outcome status
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	checkOutcome(accounts, binaryevents, eventId, 0, ['isWin'], [False])  #check outcome0 result
	checkOutcome(accounts, binaryevents, eventId, 1, ['isWin'], [True])  #check outcome1 result
	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [price]) #check oraclePrice

	#call climBet with nonexist betId for account
	with reverts(""):
		gamblepool.claimBet(0, {"from": accounts[2]})

	#call claimBet
	i = 0
	while i < bet_quantity:
		before_balance0 = gamblepool.getUserBalance(accounts[0].address)
		before_balance1 = gamblepool.getUserBalance(accounts[1].address)
		all_bets_amount = gamblepool.inBetsAmount()
		gamblepool.claimBet(i, {"from": accounts[0]})
		gamblepool.claimBet(i, {"from": accounts[1]})
		checkBet(accounts, gamblepool, accounts[0].address, i, ['currentState', 'result'], [2, 2])
		checkBet(accounts, gamblepool, accounts[1].address, i, ['currentState', 'result'], [2, 1])
		assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 #check balance acc 0
		assert gamblepool.getUserBalance(accounts[1].address) >= before_balance1 + bet_amount * (100 + binaryevents.WIN_DISTRIB_PERCENT()-1) / 100  #check balance acc 1 
		assert gamblepool.inBetsAmount() >= all_bets_amount - bet_amount * (100 + binaryevents.WIN_DISTRIB_PERCENT() + 1) / 100  #check all collected coins
		i += 1

	#settle game event
	before_inBetsAmount = gamblepool.inBetsAmount()
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	assert gamblepool.inBetsAmount() == before_inBetsAmount - rewardmodel.getFundPercent()*bets_amount0/100
	assert gamblepool.fundBalance() == before_fundBalance + rewardmodel.getFundPercent()*bets_amount0 /100
	assert gamblepool.eventsSettlement(0, eventId)[0] == rewardmodel.getFundPercent()*bets_amount0/100

	#check working fuction
	assert binaryevents.comparePrice(1, 400, 300) == True






	



