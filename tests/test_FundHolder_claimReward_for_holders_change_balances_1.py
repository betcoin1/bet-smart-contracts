import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

amount = 1000e18
add_amount = 900e18

#two accounts
#before claiming accounts are not joined to fund
#create new epoch
#make first claim
#accounts became joined to fund
#create new epoch
#acc2 INcreases balance after creation of new epoch
#claim for accounts
def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True, 
        {"from": accounts[0]})

    #stake tokens
    logging.info('fundStartBlock = {}'.format(fundHolders01.fundStartBlock()))
    bettoken.mint(accounts[1], amount, {"from": accounts[0]})

    bettoken.approve(mockgamblepool.address, amount, {"from": accounts[1]})
    logging.info('*****************stake acc1************')

    mockgamblepool.stake(amount, {"from": accounts[1]})
    balance_before = mockgamblepool.getUserBalance(accounts[1])
    assert balance_before == amount

    fundCount = mockgamblepool.getFundCount()

def test_settle_event(accounts, mockgamblepool, bettoken, fundHolders01):
    #for next test case
    #stake tokens
    bettoken.mint(accounts[2], amount, {"from": accounts[0]})

    bettoken.approve(mockgamblepool.address, amount, {"from": accounts[2]})
    logging.info('*****************stake acc2************')
    
    mockgamblepool.stake(amount, {"from": accounts[2]})
    balance_before = mockgamblepool.getUserBalance(accounts[2])
    assert balance_before == amount
    
    logging.info('count_rewards = {}'.format(fundHolders01.getRewardEpochCount()))
    mockgamblepool.settleGameEvent(0, 1e18)
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == 2


####define reward for account when it is holder,
####rewardEpoch does not equal to 0
## acc2 
#claim for epoch 0
def test_fundHolders_claimReward_0(accounts, mockgamblepool, bettoken, fundHolders01):

    before_userData = fundHolders01.getUser(accounts[2])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch() - 1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() -1)
    logging.info('before_user_data_acc2 = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))

    mockgamblepool.claimFund(0, 0, {"from": accounts[2]})
    after_userData = fundHolders01.getUser(accounts[2])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() -1)
    logging.info('after_userData_acc2 = {}'.format(after_userData))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]

####define reward for account when it is holder,
####rewardEpoch does not equal to 0
####account is not joined to fund
## acc1
#claim for epoch 0
def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('fundHolders01.getAvailableReward = {}'.format(fundHolders01.getAvailableReward(accounts[1].address)))


    balance_before = mockgamblepool.getUserBalance(accounts[1])
    before_userData = fundHolders01.getUser(accounts[1])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch()-1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch()-1)
    logging.info('before_user_data_acc1 = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    
    mockgamblepool.claimFund(0, 0, {"from": accounts[1]})
    balance_after = mockgamblepool.getUserBalance(accounts[1])
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch()-1)
    logging.info('after_userData_acc1 = {}'.format(after_userData))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('after_balance = {}'.format(balance_after))
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]
    #assert balance_before + amount == balance_after
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

#close epoch 1
def test_settle_event_again(accounts, mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.settleGameEvent(0, 1e18)
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == 3

####stake additional amount of tokens by acc2 in epoch 2
def test_fundHolders_stake_again(accounts, mockgamblepool, bettoken, fundHolders01):

    before_userData = fundHolders01.getUser(accounts[2])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch()))
    logging.info('****************stake acc2 again*******************')
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch())
    logging.info('before_user_data_acc2 = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))

    bettoken.mint(accounts[2], add_amount, {"from": accounts[0]})
    bettoken.approve(mockgamblepool.address, add_amount, {"from": accounts[2]})
    mockgamblepool.stake(add_amount, {"from": accounts[2]})
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))
    
    after_userData = fundHolders01.getUser(accounts[2])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch())
    logging.info('after_userData_acc2 = {}'.format(after_userData))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

####define reward for account when it is holder,
####rewardEpoch does not equal to 0
####account is joined to fund
#we are in epoch 2
#acc2 claims for epoch 1
def test_fundHolders_claimReward_2_again(accounts, mockgamblepool, bettoken, fundHolders01):

    before_userData = fundHolders01.getUser(accounts[2])
    logging.info('Epoch = {}'.format(fundHolders01.currentEpoch()- 1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('before_user_data_acc2 = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))

    mockgamblepool.claimFund(0, 1, {"from": accounts[2]})
    
    after_userData = fundHolders01.getUser(accounts[2])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('after_userData_acc2 = {}'.format(after_userData))
    logging.info('after_RewardEpoch!!! = {}'.format(after_RewardEpoch))
    assert before_userData[0] == after_userData[0]
    assert before_RewardEpoch[0] == after_RewardEpoch[0]
    assert before_userData[1][3] == 0
    assert after_userData[1][3] >0
    assert before_RewardEpoch[1] < after_RewardEpoch[1]


####define reward for account when it is holder,
####rewardEpoch does not equal to 0
####account is joined to fund
#acc1 claims for epoch 1
def test_fundHolders_claimReward_3_again(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('fundHolders01.getAvailableReward = {}'.format(fundHolders01.getAvailableReward(accounts[1].address)))

    balance_before = mockgamblepool.getUserBalance(accounts[1])
    before_userData = fundHolders01.getUser(accounts[1])
    logging.info('Epoch = {}'.format(fundHolders01.currentEpoch() - 1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('before_user_data_acc1 = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    
    mockgamblepool.claimFund(0, 1, {"from": accounts[1]})

    balance_after = mockgamblepool.getUserBalance(accounts[1])
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('after_userData_acc2 = {}'.format(after_userData))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('after_balance = {}'.format(balance_after))
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]
    #assert balance_before + amount == balance_after
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

