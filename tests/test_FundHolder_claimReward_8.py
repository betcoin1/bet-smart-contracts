import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1000
currEpoch = 0

# two account
#stake in 0 epoch
# acc 1 change stake balance before close 0 epoch
#without other balance moves
#skip 3 epochs
#all acc claim in 3 epoch all epoch
#between epoch 50 blocks

def test_gamble(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[1], amount, {"from": accounts[0]})
    bettoken.mint(accounts[2], amount, {"from": accounts[0]})

    #stake tokens
    bettoken.approve(mockgamblepool.address, 2*amount, {"from": accounts[1]})
    bettoken.approve(mockgamblepool.address, 2*amount, {"from": accounts[2]})
    logging.info('*****************stake acc1************')
    logging.info('currEpoch = {}'.format(currEpoch))
    mockgamblepool.stake(amount, {"from": accounts[1]})
    mockgamblepool.stake(amount, {"from": accounts[2]})

    chain.mine(50)
    mockgamblepool.withdraw(1, {"from": accounts[1]})


def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 2

def test_settle_event_2(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 3

def test_settle_event_3(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 4


def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('////////////////////////CLAIM EPOCH 0/////////////////////////')
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[1]})
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[2]})
    
    logging.info('after_userRewards acc1= {}'.format(fundHolders01.getUser(accounts[1])))
    logging.info('after_userRewards acc2= {}'.format(fundHolders01.getUser(accounts[2])))
    logging.info('after_RewardEpoch_0 = {}'.format(fundHolders01.getRewardEpoch(currEpoch)))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))

    assert fundHolders01.getUser(accounts[1])[0][3] >0
    assert fundHolders01.getUser(accounts[2])[0][3] >0
    assert fundHolders01.getRewardEpoch(currEpoch)[1] > 0

    logging.info('////////////////////////CLAIM EPOCH 1/////////////////////////')
    mockgamblepool.claimFund(fundId, currEpoch+1, {'from': accounts[1]})
    mockgamblepool.claimFund(fundId, currEpoch+1, {'from': accounts[2]})
    
    logging.info('after_userRewards acc1= {}'.format(fundHolders01.getUser(accounts[1])))
    logging.info('after_userRewards acc2= {}'.format(fundHolders01.getUser(accounts[2])))
    logging.info('after_RewardEpoch_1 = {}'.format(fundHolders01.getRewardEpoch(currEpoch+1)))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))

    assert fundHolders01.getUser(accounts[1])[1][3] >0
    assert fundHolders01.getUser(accounts[2])[1][3] >0
    assert fundHolders01.getRewardEpoch(currEpoch+1)[1] > 0

    logging.info('////////////////////////CLAIM EPOCH 2/////////////////////////')
    mockgamblepool.claimFund(fundId, currEpoch+2, {'from': accounts[1]})
    mockgamblepool.claimFund(fundId, currEpoch+2, {'from': accounts[2]})
    
    logging.info('after_userRewards acc1= {}'.format(fundHolders01.getUser(accounts[1])))
    logging.info('after_userRewards acc2= {}'.format(fundHolders01.getUser(accounts[2])))
    logging.info('after_RewardEpoch_2 = {}'.format(fundHolders01.getRewardEpoch(currEpoch)))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))

    assert fundHolders01.getUser(accounts[1])[2][3] >0
    assert fundHolders01.getUser(accounts[2])[2][3] >0
    assert fundHolders01.getRewardEpoch(currEpoch+2)[1] > 0
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    