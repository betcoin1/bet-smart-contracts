import pytest
import logging, time
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
# cases:
    #before expire date:
        # event status Active
    # after expire date:
        #event status Active
        #event status Cancel
        #event status Finished

def test_binaryevents_create(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = 0
    logging.info(binaryeventMock.getEvent(0))
    logging.info(tx.events['NewEvent'])
    assert tx.events['NewEvent']['source'] == binaryeventMock.address
    assert binaryeventMock.getEvent(0)[5][1][0] == 4;# check type of second outcome
    assert binaryeventMock.getEvent(0)[5][0][0] == 1;# check type of first outcome
    assert binaryeventMock.getEvent(0)[1] == 0;# check event status
    assert binaryeventMock.getEvent(0)[4] == 'BTC/USD';# check currency pair

def test_binaryevents_incbet_fail(accounts, binaryeventMock):
    with reverts("Only pool can call it"):
        binaryeventMock.incBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[1]}
    )

    with reverts("Only 2 outcomes for this event type"):
        binaryeventMock.incBetCountForOutcome(
        0,2, 10e18,
        {'from':accounts[0]}
    )

    with reverts(""):
        binaryeventMock.incBetCountForOutcome(
        3,0, 10e18,
        {'from':accounts[0]}
    )

    '''with reverts(""):
        binaryeventMock.incBetCountForOutcome(
        0,0, -1,
        {'from':accounts[0]}
    )'''

def test_binaryevents_incbet_1(accounts, binaryeventMock):
    tx = binaryeventMock.incBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[0]}
    )
    logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    assert binaryeventMock.getEvent(0)[5][0][3] == 1; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][0][2] == 10e18; # check bet amount
    logging.info('Time machine running.....+1 hour...................................')
    chain.sleep(3600)
    chain.mine()
    #expire date is now
    # change event status
    tx = binaryeventMock.incBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[0]}
    )
    logging.info('tx = {}'.format(tx))
    logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    assert binaryeventMock.getEvent(0)[1] == 2 #check status Finished
    assert binaryeventMock.getEvent(0)[5][0][3] == 1; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][0][2] == 10e18; # check bet amount
    assert binaryeventMock.getEvent(0)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(0)[6] > 0; # check price

    # again try to call incBetCountForOutcome - now status is Finished
    
    with reverts("Event is Finished"):
        binaryeventMock.incBetCountForOutcome(
            0,0, 10e18,
            {'from':accounts[0]}
        )
    assert binaryeventMock.getEvent(0)[1] == 2 #check status Finished
    assert binaryeventMock.getEvent(0)[5][0][3] == 1; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][0][2] == 10e18; # check bet amount
    assert binaryeventMock.getEvent(0)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(0)[6] > 0; # check price

def test_binaryevents_incbet_2(accounts, binaryeventMock):
    # create event
    tx  = binaryeventMock.createEvent(
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = 1

    # make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )

    # cancel bet
    tx = binaryeventMock.decBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEvent(eventId)[1] == 0 #check status Active
    assert binaryeventMock.getEvent(eventId)[5][0][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][0][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[6] == 0; # check price

    chain.sleep(3600)
    chain.mine()

    # change event status to Canceled
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEvent(eventId)[1] == 3 #check status Cancelled
    assert binaryeventMock.getEvent(eventId)[5][0][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][0][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[6] == 0; # check price

    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )

    assert binaryeventMock.getEvent(eventId)[1] == 3 #чек статуса
    assert binaryeventMock.getEvent(eventId)[5][0][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][0][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[6] == 0; # check price
