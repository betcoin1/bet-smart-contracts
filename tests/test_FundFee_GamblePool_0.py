import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 10000e18
currEpoch = 0


#register fund of fee
#settle game event
#withdraw fund fee

'''def test_register(accounts, fundFee):
    fundFee.registerFund({"from": accounts[1]})'''

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundFee, fundBurn01):
    mockgamblepool.registerFund(
        fundFee.address,
        3,
        False,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()
    assert  fundCount == 1

    #register other fund
    mockgamblepool.registerFund(
        fundBurn01.address,
        97,
        False,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[0], amount)
    bettoken.approve(mockgamblepool, amount, {"from": accounts[0]})
    mockgamblepool.stake(amount)
    assert mockgamblepool.getUserBalance(accounts[0]) == amount
    assert mockgamblepool.getUserBalance(accounts[0]) == bettoken.balanceOf(mockgamblepool.address)

    with reverts("Cant withdraw zero"):
        fundFee.withdrawFee(0, {"from": accounts[0]})

    with reverts("Too much for withdraw"):
        fundFee.withdrawFee(1, {"from": accounts[0]})

def test_settle_event(accounts, mockgamblepool,fundFee, bettoken):
    
    mockgamblepool.settleGameEvent(1, amount)
    assert fundFee.rewardForCurrentEpoch() == amount*mockgamblepool.getFund(0)[1]/100
    assert mockgamblepool.getUserBalance(fundFee) == amount*mockgamblepool.getFund(0)[1]/100
    assert mockgamblepool.getUserBalance(accounts[0]) == bettoken.balanceOf(mockgamblepool.address)
    assert mockgamblepool.getUserBalance(accounts[0]) == amount


def test_withdraw(accounts, mockgamblepool, bettoken, fundFee):

    with reverts("Onle owner"):
        fundFee.withdrawFee(amount, {"from": accounts[1]})

    with reverts("Too much for withdraw"):
        fundFee.withdrawFee(amount + 1, {"from": accounts[0]})

    tx =fundFee.withdrawFee(mockgamblepool.getUserBalance(fundFee), {"from": accounts[0]})
    amount_withdrawn = tx.events['Withdrawn']['amount']
    assert amount_withdrawn == amount*mockgamblepool.getFund(0)[1]/100
    assert mockgamblepool.getUserBalance(fundFee) == 0
    assert bettoken.balanceOf(mockgamblepool.address) == amount - amount*mockgamblepool.getFund(0)[1]/100
    assert bettoken.balanceOf(accounts[0]) == amount*mockgamblepool.getFund(0)[1]/100
    assert fundFee.rewardForCurrentEpoch() == 0

