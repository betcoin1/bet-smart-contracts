import pytest
import logging
from brownie import Wei, reverts, chain, BinaryEvents
LOGGER = logging.getLogger(__name__)

def test_gamble_createEvent(accounts, binaryevents,gamblepool):
    gamblepool.addGame(
        binaryevents.address, 
        'Binary Events', 
        '0x0000000000000000000000000000000000000000',
    	{"from": accounts[0]})
    #nonexist game
    with reverts(""):
        gamblepool.createEvent(
            1, #_gameId
            chain.time() + 100 ,
            chain.time() + 66 ,
            'BTC/USD',
            [
                [1,4815987599255,0,0,0], #MoreOrEqual
                [4,4815987599255,0,0,0]  #less
            ],
            {'from':accounts[0]}
        )
    #only owner can change game status
    with reverts("Ownable: caller is not the owner"):
        gamblepool.setGameState(0, 1, {"from": accounts[1]})

    #game status is not Active
    gamblepool.setGameState(0, 1, {"from": accounts[0]})
    #wrong game state
    with reverts("Sorry game is not available"):
        gamblepool.createEvent(
            0, #_gameId
            chain.time() + 100 ,
            chain.time() + 66 ,
            'BTC/USD',
            [
                [1,4815987599255,0,0,0], #MoreOrEqual
                [4,4815987599255,0,0,0]  #less
            ],
            {'from':accounts[0]}
        )
    # set Active status
    gamblepool.setGameState(0, 0, {"from": accounts[0]})
    # создаем событие для игры
    gamblepool.createEvent(
        0, #_gameId
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        {'from':accounts[0]}
    )
    addr = gamblepool.games(0)[0]
    gameContract = BinaryEvents.at(addr)
    assert gameContract.getEventCount() == 1
    assert binaryevents.getEvent(0)[0] == accounts[0] # создатель
    assert binaryevents.getEvent(0)[5][1][0] == 4 # тип второго исхода
    assert binaryevents.getEvent(0)[5][0][0] == 1 # тип первого исхода
    assert binaryevents.getEvent(0)[1] == 0 # проверка статуса события
    assert binaryevents.getEvent(0)[4] == 'BTC/USD' # проверка валютной пары
    assert binaryevents.getEvent(0)[6] == 0 # проверка цены оракула
