import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 10000e18
currEpoch = 0


#register fund of burn
#settle game event
#burn

'''def test_register(accounts, fundBurn01):
    fundBurn01.registerFund({"from": accounts[1]})'''

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundBurn01, fundFee):
    mockgamblepool.registerFund(
        fundBurn01.address,
        1,
        False,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()
    assert  fundCount == 1

    #register other fund
    mockgamblepool.registerFund(
        fundFee.address,
        99,
        False,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[0], amount)
    bettoken.approve(mockgamblepool, amount, {"from": accounts[0]})
    mockgamblepool.stake(amount)
    assert mockgamblepool.getUserBalance(accounts[0]) == amount
    assert mockgamblepool.getUserBalance(accounts[0]) == bettoken.balanceOf(mockgamblepool.address)

    with reverts("Cant withdraw zero"):
        fundBurn01.withdrawAndBurn(0, {"from": accounts[0]})

    with reverts("Too much for burn"):
        fundBurn01.withdrawAndBurn(1, {"from": accounts[0]})

def test_settle_event(accounts, mockgamblepool,fundBurn01, bettoken):
    
    mockgamblepool.settleGameEvent(1, amount)
    assert fundBurn01.rewardForCurrentEpoch() == amount*mockgamblepool.getFund(0)[1]/100
    assert mockgamblepool.getUserBalance(fundBurn01) == amount*mockgamblepool.getFund(0)[1]/100
    assert mockgamblepool.getUserBalance(accounts[0]) == bettoken.balanceOf(mockgamblepool.address)
    assert mockgamblepool.getUserBalance(accounts[0]) == amount


def test_withdraw(accounts, mockgamblepool, bettoken, fundBurn01):

    with reverts("Onle owner"):
        fundBurn01.withdrawAndBurn(amount, {"from": accounts[1]})

    with reverts("Too much for burn"):
        fundBurn01.withdrawAndBurn(amount + 1, {"from": accounts[0]})

    tx =fundBurn01.withdrawAndBurn(mockgamblepool.getUserBalance(fundBurn01), {"from": accounts[0]})
    amount_burnt = tx.events['Withdrawn']['amount']
    assert amount_burnt == amount*mockgamblepool.getFund(0)[1]/100
    assert mockgamblepool.getUserBalance(fundBurn01) == 0
    assert bettoken.balanceOf(mockgamblepool.address) == amount - amount*mockgamblepool.getFund(0)[1]/100
    assert bettoken.balanceOf(accounts[0]) == 0
    assert fundBurn01.rewardForCurrentEpoch() == 0
    assert bettoken.totalSupply() == amount - amount*mockgamblepool.getFund(0)[1]/100

