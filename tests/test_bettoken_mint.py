import pytest
import logging
from brownie import Wei, reverts
LOGGER = logging.getLogger(__name__)


def test_betERC20_mint_fail(accounts, bettoken):
	with reverts("MinterRole: caller does not have the Minter role"):
		bettoken.mint(accounts[0], 1, {"from":accounts[1]})
	assert bettoken.balanceOf(accounts[0]) == 0

def test_betERC20_mint_big_supply_failed(accounts, bettoken):
	with reverts("MAX_SUPPLY amount exceed"):
		bettoken.mint(accounts[0], bettoken.MAX_SUPPLY() + 1, {"from":accounts[0]})
	assert bettoken.balanceOf(accounts[0]) == 0

def test_betERC20_mint_success(accounts, bettoken):
	bettoken.mint(accounts[0], 1, {"from":accounts[0]})
	assert bettoken.balanceOf(accounts[0]) == 1
	assert bettoken.totalSupply() == 1

def test_betERC20_mint_fail_zero_address(accounts, bettoken):
	with reverts("ERC20: mint to the zero address"):
		bettoken.mint('0x0000000000000000000000000000000000000000', 1, {"from":accounts[0]})
	assert bettoken.totalSupply() == 1