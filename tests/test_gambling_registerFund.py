import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'


def test_gamble_registerFund(accounts, binaryevents,gamblepool, bettoken, fundHolders01):
    with reverts("Ownable: caller is not the owner"):
        gamblepool.registerFund(
            accounts[1],
            1,
            False,
        	{"from": accounts[1]})
    
    with reverts("Percent must be more then zero"):
        gamblepool.registerFund(
            accounts[1],
            0,
            False,
            {"from": accounts[0]})

    with reverts("No zero fund"):
        gamblepool.registerFund(
            zero_address,
            1,
            False,
            {"from": accounts[0]})
    #address is not gamblepool
    with reverts(""):
        gamblepool.registerFund(
            accounts[1],
            1,
            False,
            {"from": accounts[0]})
    
    gamblepool.registerFund(
        fundHolders01.address,
        1,
        False,
        {"from": accounts[0]})

    with reverts("Already registered"):
        gamblepool.registerFund(
            fundHolders01.address,
            1,
            False,
            {"from": accounts[0]})

    assert gamblepool.getFundCount() == 1
    fundCount = gamblepool.getFundCount()
    assert gamblepool.getFund(fundCount-1)[2] == 0
    assert gamblepool.getFund(fundCount-1)[1] == 1

    logging.info('fund = {}'.format(gamblepool.getFund(fundCount-1)))