import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1000
currEpoch = 0

# one account
#stake in 0 epoch
#without other balance moves
#claim in 3 epochs

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[1], amount, {"from": accounts[0]})

    #stake tokens
    bettoken.approve(mockgamblepool.address, 2*amount, {"from": accounts[1]})
    logging.info('*****************stake acc1************')
    logging.info('currEpoch = {}'.format(currEpoch))
    mockgamblepool.stake(amount, {"from": accounts[1]})
    '''chain.mine(50)
    mockgamblepool.stake(amount, {"from": accounts[1]})'''

def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 2

####define reward for account when it is not holder,
####rewardEpoch does not equal to 0
####account is joined to fund
def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('currEpoch = {}'.format(currEpoch))
    before_userData = fundHolders01.getUser(accounts[1])
    bba1 = mockgamblepool.getUserBalance(accounts[1])
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('before_user_data = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_1!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[1]})
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('after_userData = {}'.format(after_userData))
    logging.info('after_RewardEpoch_0 = {}'.format(after_RewardEpoch))
    logging.info('after_RewardEpoch_1 = {}'.format(fundHolders01.getRewardEpoch(currEpoch + 1)))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    assert after_userData[0][3] == after_RewardEpoch[1]
    assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userData[0][3]
    assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
    assert after_userData[0][3] > 0
    assert bba1 < mockgamblepool.getUserBalance(accounts[1])

def test_settle_event_2(accounts, mockgamblepool, bettoken, fundHolders01):
    currEpoch = fundHolders01.currentEpoch()
    logging.info('currEpoch = {}'.format(currEpoch))
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch+2

def test_fundHolders_claimReward_2(accounts, mockgamblepool, bettoken, fundHolders01):
    currEpoch = fundHolders01.currentEpoch() - 1
    logging.info('currEpoch = {}'.format(currEpoch))
    before_userData = fundHolders01.getUser(accounts[1])
    bba1 = mockgamblepool.getUserBalance(accounts[1])
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('before_user_data = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_2!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[1]})
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('after_userData = {}'.format(after_userData))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    assert after_userData[1][3] == after_RewardEpoch[1]
    assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userData[1][3]
    assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
    assert after_userData[0][3] > 0
    assert bba1 < mockgamblepool.getUserBalance(accounts[1])

def test_settle_event_3(accounts, mockgamblepool, bettoken, fundHolders01):
    currEpoch = fundHolders01.currentEpoch()
    logging.info('currEpoch = {}'.format(currEpoch))
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch+2

def test_fundHolders_claimReward_3(accounts, mockgamblepool, bettoken, fundHolders01):
    currEpoch = fundHolders01.currentEpoch() - 1
    logging.info('currEpoch = {}'.format(currEpoch))
    before_userData = fundHolders01.getUser(accounts[1])
    bba1 = mockgamblepool.getUserBalance(accounts[1])
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('before_user_data = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_3!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[1]})
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('after_userData = {}'.format(after_userData))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    assert after_userData[2][3] == after_RewardEpoch[1]
    assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userData[2][3]
    assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
    assert after_userData[0][3] > 0
    assert bba1 < mockgamblepool.getUserBalance(accounts[1])