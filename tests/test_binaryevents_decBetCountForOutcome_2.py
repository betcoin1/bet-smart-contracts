import pytest
import logging, time
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0

# cases:
    #before expire date:
        #event status Active
        #event status Cancel
        #event status Hold

#event status is Active, Cancelled
def test_binaryevents_create(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 7200 ,
        chain.time() + 3600 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = 0
    #logging.info(binaryeventMock.getEvent(0))
    #logging.info(tx.events['NewEvent'])
    assert tx.events['NewEvent']['source'] == binaryeventMock.address
    assert binaryeventMock.getEvent(0)[5][1][0] == 4;# check type of second outcome 
    assert binaryeventMock.getEvent(0)[5][0][0] == 1;# check type of first outcome 
    assert binaryeventMock.getEvent(0)[1] == 0;# check event status
    assert binaryeventMock.getEvent(0)[4] == 'BTC/USD';# check currency pair

def test_binaryevents_decbet_1(accounts, binaryeventMock):
    # make bet
    tx = binaryeventMock.incBetCountForOutcome(
        0,0, 20e18,
        {'from':accounts[0]}
    )

    #logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    assert binaryeventMock.getEvent(0)[5][0][3] == 1; # чек количества ставок
    assert binaryeventMock.getEvent(0)[5][0][2] == 20e18; # чек суммы ставок
    
    #logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    # partly return bet
    bets_before = binaryeventMock.getEvent(0)[5][0][3]
    amount_before = binaryeventMock.getEvent(0)[5][0][2]
    tx = binaryeventMock.decBetCountForOutcome(
        0,0, 9e18,
        {'from':accounts[0]}
    )
    #logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    #logging.info(binaryeventMock.getEvent(0))
    assert binaryeventMock.getEvent(0)[5][0][3] == bets_before - 1;
    assert binaryeventMock.getEvent(0)[5][0][2] == amount_before - 9e18;

    #logging.info('Time machine running.....+1 hour...................................')
    chain.sleep(3601)
    chain.mine()

    # change event status
    bets_before = binaryeventMock.getEvent(0)[5][0][3]
    amount_before = binaryeventMock.getEvent(0)[5][0][2]
    tx = binaryeventMock.decBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[0]}
    )
    #logging.info('tx = {}'.format(tx))
    #logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    # event will be cancelled - outcomes have bets quantity  equaled 0
    assert binaryeventMock.getEvent(0)[1] == 3 # check status Canceled
    assert binaryeventMock.getEvent(0)[5][0][3] == bets_before; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][0][2] == amount_before; # check bet amount
    assert binaryeventMock.getEvent(0)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(0)[6] == 0; # check price

    # again call decBetCountForOutcome when event status is Cancelled - nothing has changed
    tx = binaryeventMock.decBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[0]}
    )
   
    assert binaryeventMock.getEvent(0)[1] == 3 #check status Canceled
    assert binaryeventMock.getEvent(0)[5][0][3] == bets_before; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][0][2] == amount_before; # check bet amount
    assert binaryeventMock.getEvent(0)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(0)[6] == 0; # check price

# event status  is Hold
def test_binaryevents_decbet_2(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 7200 ,
        chain.time() + 3600 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = 1
    # make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 20e18,
        {'from':accounts[0]}
    )

    #logging.info('Time machine running.....+1 hour...................................')
    chain.sleep(3601)
    chain.mine()

    #cancel bet
    bets_before = binaryeventMock.getEvent(eventId)[5][0][3]
    amount_before = binaryeventMock.getEvent(eventId)[5][0][2]
    tx = binaryeventMock.decBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    #event status is  Hold
    assert binaryeventMock.getEvent(eventId)[1] == 1 #check status Hold
    assert binaryeventMock.getEvent(eventId)[5][0][3] == bets_before; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][0][2] == amount_before; # check bet amount
    assert binaryeventMock.getEvent(eventId)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[6] == 0; # check price

    #try to change status to Finished
    binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 1 #check status Hold
    #cancel bet
    bets_before = binaryeventMock.getEvent(eventId)[5][0][3]
    amount_before = binaryeventMock.getEvent(eventId)[5][0][2]
    logging.info('before binaryeventMock.getEvent(eventId) = {}'.format(binaryeventMock.getEvent(eventId)))
    tx = binaryeventMock.decBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    logging.info('after binaryeventMock.getEvent(eventId) = {}'.format(binaryeventMock.getEvent(eventId)))
    assert binaryeventMock.getEvent(eventId)[1] == 1 #check status Hold
    assert binaryeventMock.getEvent(eventId)[5][0][3] == bets_before; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][0][2] == amount_before; # check bet amount
    assert binaryeventMock.getEvent(eventId)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[6] == 0; # check price

    #logging.info('Time machine running.....+1 hour...................................')
    chain.sleep(3601)
    chain.mine()

    #try to change status to Finished
    binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check status Finished
    assert binaryeventMock.getEvent(eventId)[6] > 0; # check price

    #cancel bet
    bets_before = binaryeventMock.getEvent(eventId)[5][0][3]
    amount_before = binaryeventMock.getEvent(eventId)[5][0][2]
    logging.info('before binaryeventMock.getEvent(eventId) = {}'.format(binaryeventMock.getEvent(eventId)))
    with reverts("Event is Finished"):
        binaryeventMock.decBetCountForOutcome(
            eventId,0, 10e18,
            {'from':accounts[0]}
        )
    logging.info('after binaryeventMock.getEvent(eventId) = {}'.format(binaryeventMock.getEvent(eventId)))
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check status Finish
    assert binaryeventMock.getEvent(eventId)[5][0][3] == bets_before; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][0][2] == amount_before; # check bet amount
    assert binaryeventMock.getEvent(eventId)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[6] > 0; # check price
