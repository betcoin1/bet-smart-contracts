import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
LOGGER = logging.getLogger(__name__)
#one game, one event, one acc make 7 bets for each outcomes. Outcome1 wins, acc0 gets prize for each bet of outcome1
#try to claimBet again with different successfulness of outcomes
def test_gamble_claimBet_1(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel):
	#prepare test data
	#add game
	gamblepool.addGame(binaryevents.address, 
		'Binary Events', 
		rewardmodel.address, 
		{"from": accounts[0]})
	# create event for game
	gamblepool.createEvent(
		0, #_gameId
		chain.time() + 100 ,
		chain.time() + 66 ,
		'BTC/USD',
		[
			[1,4815987599256,0,0,0], #MoreOrEqual
			[4,4815987599256,0,0,0]  #less
		],
		{'from':accounts[0]}
	)
	eventId = binaryevents.getEventCount() - 1

	# stake
	#acc 0
	bettoken.mint(accounts[0], 100000, {"from": accounts[0]})
	bettoken.approve(gamblepool.address, 100000, {"from": accounts[0]})
	gamblepool.stake(100000, {"from": accounts[0]})

	i = 0
	bet_amount = 1000
	bets_amount0 = 0
	before_balance0 = gamblepool.getUserBalance(accounts[0].address)
	logging.info('before_balance0 = {}'.format(before_balance0))
	while i < 7:
		gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]}) #first outcome
		gamblepool.makeBet(0, eventId, 1, bet_amount, {"from":accounts[0]})	#second outcome
		bets_amount0 += 2 * bet_amount
		i +=1

	logging.info('after_balance0 = {}'.format(gamblepool.getUserBalance(accounts[0].address)))
	#set price for pair
	price = 4815987599260
	oracle.setCustomPrice(price, {"from": accounts[0]})

	#move date to after deadline
	chain.sleep(67)
	chain.mine()
	gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
	checkEvent(accounts, binaryevents, eventId, ['state', 'oraclePrice'], [1, 0]) #check status Hold and oraclePrice
	assert gamblepool.getUserBalance(accounts[0].address) + bets_amount0 == before_balance0 #check balance acc 0

	bet_quantity = gamblepool.getUsersBetsCount(accounts[0].address)
   
   	#check bets
	i = 0
	while i < bet_quantity:
		checkBet(accounts, gamblepool, accounts[0].address, i, ['eventId', 'eventOutcomeIndex', 'betAmount', 'currentState', 'result'], [eventId, i%2, bet_amount, 0, 0])
		i += 1

	#move date to after expire date
	chain.sleep(101)
	chain.mine()

	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [0]) #check oraclePrice
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	#change status
	binaryevents.checkStateWithSave(eventId, {"from": accounts[1]})

	#check outcome status
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	checkOutcome(accounts, binaryevents, eventId, 0, ['isWin'], [False])  #check outcome0 result
	checkOutcome(accounts, binaryevents, eventId, 1, ['isWin'], [True])  #check outcome1 result
	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [price]) #check oraclePrice

	#call claimBet
	i = 0
	while i < bet_quantity:
		before_balance0 = gamblepool.getUserBalance(accounts[0].address)
		all_bets_amount = gamblepool.inBetsAmount()
		gamblepool.claimBet(i, {"from": accounts[0]})
		if i%2 == 0:
			checkBet(accounts, gamblepool, accounts[0].address, i, ['currentState', 'result'], [2, 2])
			assert gamblepool.getUserBalance(accounts[0].address) == before_balance0  #check balance acc 0 - winner
			assert gamblepool.inBetsAmount() == all_bets_amount #check all collected coins
		else:
			checkBet(accounts, gamblepool, accounts[0].address, i, ['currentState', 'result'], [2, 1])
			assert gamblepool.getUserBalance(accounts[0].address) > before_balance0 + bet_amount + (binaryevents.WIN_DISTRIB_PERCENT() - 1) * bet_amount / 100  #check balance acc 0 - winner
			assert gamblepool.inBetsAmount() < all_bets_amount - bet_amount - (binaryevents.WIN_DISTRIB_PERCENT() - 1) * bet_amount / 100  #check all collected coins
		i += 1
	#try to climBet again
	before_balance0 = gamblepool.getUserBalance(accounts[0].address)
	all_bets_amount = gamblepool.inBetsAmount()
	with reverts(""):
		gamblepool.claimBet(0, {"from": accounts[0]})
	with reverts(""):
		gamblepool.claimBet(1, {"from": accounts[0]})
	
	#settle game event
	before_inBetsAmount = gamblepool.inBetsAmount()
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	assert gamblepool.inBetsAmount() == before_inBetsAmount - rewardmodel.getFundPercent()*bets_amount0/2/100
	assert gamblepool.fundBalance() == before_fundBalance + rewardmodel.getFundPercent()*bets_amount0/2/100
	assert gamblepool.eventsSettlement(0, eventId)[0] == rewardmodel.getFundPercent()*bets_amount0/2/100



	



