import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
LOGGER = logging.getLogger(__name__)

#registry all funds

def createFunds(accounts, gamblepool, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
    #registry funds
    gamblepool.registerFund(
        fundHolders01.address,
        5,
        True,
        {"from": accounts[0]}
    )
    gamblepool.registerFund(
        fundCreators01.address,
        5,
        True,
        {"from": accounts[0]}
    )

    gamblepool.registerFund(
        fundHiStake01.address,
        1,
        True,
        {"from": accounts[0]}
    )

    gamblepool.registerFund(
        fundFee.address,
        3,
        True,
        {"from": accounts[0]}
    )

    gamblepool.registerFund(
        fundBurn01.address,
        1,
        True,
        {"from": accounts[0]}
    )

    gamblepool.registerFund(
        fundLottery.address,
        5,
        True,
        {"from": accounts[0]}
    )

    assert gamblepool.getFundCount() == 6
