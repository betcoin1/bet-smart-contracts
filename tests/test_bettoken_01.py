import pytest
import logging
from brownie import Wei, reverts
LOGGER = logging.getLogger(__name__)


def test_betERC20(accounts, bettoken):
    assert bettoken.balanceOf(accounts[0]) == 0
    assert bettoken.symbol() == 'BET'
