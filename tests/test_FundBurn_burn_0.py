import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 10000e18
currEpoch = 0


# two account
#stake in 0 epoch
#without other balance moves
#claim in 3 epochs

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundBurn01):
    mockgamblepool.registerFund(
        fundBurn01.address,
        100,
        False,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()
    assert  fundCount == 1

    bettoken.mint(mockgamblepool.address, amount*10, {"from": accounts[0]})
    assert bettoken.balanceOf(mockgamblepool.address) == amount*10 



def test_settle_event_1(accounts, mockgamblepool, bettoken, fundBurn01):
    logging.info('This fund balance in pool:({}):{}'.format(
        fundBurn01.address,
        mockgamblepool.getUserBalance(fundBurn01)
    ))
    mockgamblepool.settleGameEvent(currEpoch, amount*10)
    logging.info('This fund balance in pool:({}):{}'.format(
        fundBurn01.address,
        mockgamblepool.getUserBalance(fundBurn01)
    ))

    logging.info(' mockgamblepool.totalStaked():():{}'.format(
        mockgamblepool.totalStaked()
    ))
    assert mockgamblepool.getUserBalance(fundBurn01) == bettoken.balanceOf(mockgamblepool.address)


def test_burn(accounts, mockgamblepool, bettoken, fundBurn01):

    tx =fundBurn01.withdrawAndBurn(amount)
    amount_withdrown = tx.events['Withdrawn']['amount']
    assert mockgamblepool.getUserBalance(fundBurn01) == amount*10 - amount_withdrown
    assert bettoken.balanceOf(mockgamblepool.address) == amount*10 - amount_withdrown
    assert bettoken.totalSupply() == amount*10 - amount_withdrown