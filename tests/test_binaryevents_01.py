import pytest
import logging, time
from brownie import Wei, reverts, chain
from checkData import checkOutcome
LOGGER = logging.getLogger(__name__)

EXPIRE_DELAY = 100
ORDER_DEADLINE_DELAY = 66


def test_binaryevents_create(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + EXPIRE_DELAY ,
        chain.time() + ORDER_DEADLINE_DELAY ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    logging.info(binaryeventMock.getEvent(0))
    logging.info(tx.events['NewEvent'])
    assert tx.events['NewEvent']['source'] == binaryeventMock.address
    checkOutcome(accounts, binaryeventMock, 0, 1, ['compare'], [4] )


def test_binaryevents_incbet(accounts, binaryeventMock):
    tx = binaryeventMock.incBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[0]}
    )
    logging.info(binaryeventMock.getEvent(0))
    checkOutcome(accounts, binaryeventMock, 0, 0, ['betCount'], [1] )
    tx = binaryeventMock.incBetCountForOutcome(
        0,1, 22e18,
        {'from':accounts[0]}
    )
    tx = binaryeventMock.incBetCountForOutcome(
        0,1, 22e18,
        {'from':accounts[0]}
    )
    checkOutcome(accounts, binaryeventMock, 0, 1, ['betCount'], [2] )

def test_binaryevents_decbet(accounts, binaryeventMock):
    tx = binaryeventMock.decBetCountForOutcome(
        0,1, 22e18,
        {'from':accounts[0]}
    )
    logging.info(binaryeventMock.getEvent(0))
    checkOutcome(accounts, binaryeventMock, 0, 1, ['betCount'], [1] )

def test_binaryevents_update_state(accounts, binaryeventMock):
    logging.info('Time machine running.....+100 sec...............')
    chain.sleep(EXPIRE_DELAY)
    chain.mine()
    logging.info('Chain time {}'.format( chain.time()))
    tx = binaryeventMock.incBetCountForOutcome(
            0,1, 22e18,
            {'from':accounts[0]}
        )
    logging.info(binaryeventMock.getEvent(0))
    logging.info(tx.info())
    with reverts('Event state must be Active (0) or Hold (1)'):
        tx = binaryeventMock.checkStateWithSave(0, {'from':accounts[1]})
    logging.info(binaryeventMock.getEvent(0))
    assert binaryeventMock.getEvent(0)[6] > 0;
