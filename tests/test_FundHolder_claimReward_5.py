import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1000
currEpoch = 0

# one account
# stake in 0 epoch
# skip claiming 0 epoch
# skip claiming 1 epoch
# move balance in 2 epoch
# skip claiming 2 epoch
# skip claiming 3 epoch
# claime all epoch in 4 epoch
# between epochs 50 blocks

#claim in 3 epochs

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[1], amount, {"from": accounts[0]})

    #stake tokens
    bettoken.approve(mockgamblepool.address, 2*amount, {"from": accounts[1]})
    logging.info('*****************stake acc1************')
    logging.info('Epoch = {}'.format(currEpoch))
    mockgamblepool.stake(amount, {"from": accounts[1]})

def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 2


def test_settle_event_2(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch + 1, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 3

    #move balance
    bettoken.mint(accounts[1], amount, {"from": accounts[0]})
    #stake tokens
    bettoken.approve(mockgamblepool.address, 2*amount, {"from": accounts[1]})
    logging.info('*****************stake acc1 again************')
    logging.info('Epoch = {}'.format(currEpoch))
    mockgamblepool.stake(amount, {"from": accounts[1]})

def test_settle_event_3(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch + 2, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 4

def test_settle_event_4(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch + 3, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 5



def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('////////////////////////CLAIM EPOCH 0/////////////////////////')
    logging.info('Epoch = {}'.format(currEpoch))
    before_userRewards = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('before_userRewards = {}'.format(before_userRewards))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_0!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[1]})
    after_userRewards = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('after_userRewards = {}'.format(after_userRewards))
    logging.info('after_RewardEpoch_0 = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    logging.info('////////////////////////CLAIM EPOCH 1/////////////////////////')
    logging.info('Epoch = {}'.format(currEpoch + 1))
    before_userRewards = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 1)
    logging.info('before_userRewards = {}'.format(before_userRewards))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_1!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch + 1, {'from': accounts[1]})
    after_userRewards = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 1)
    logging.info('after_userRewards = {}'.format(after_userRewards))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))


    logging.info('////////////////////////CLAIM EPOCH 2/////////////////////////')
    logging.info('Epoch = {}'.format(currEpoch + 2))
    before_userRewards = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 2)
    logging.info('before_userRewards = {}'.format(before_userRewards))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_2!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch + 2, {'from': accounts[1]})
    after_userRewards = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 2)
    logging.info('after_userRewards = {}'.format(after_userRewards))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    logging.info('////////////////////////CLAIM EPOCH 3/////////////////////////')
    logging.info('Epoch = {}'.format(currEpoch + 3))
    before_userRewards = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 3)
    logging.info('before_userRewards = {}'.format(before_userRewards))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_2!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch + 3, {'from': accounts[1]})
    after_userRewards = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 3)
    logging.info('after_userRewards = {}'.format(after_userRewards))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))


    logging.info('////////////////////////CLAIM EPOCH 4/////////////////////////')
    logging.info('Epoch = {}'.format(currEpoch + 4))
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 4)
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))

    logging.info('***********************AFTER CLAIM_2!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    with reverts("Cant claim from currentEpoch"):
        mockgamblepool.claimFund(fundId, currEpoch + 4, {'from': accounts[1]})
    after_userRewards = fundHolders01.getUser(accounts[1])
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))