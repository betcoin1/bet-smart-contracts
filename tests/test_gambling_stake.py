import pytest
import logging
from brownie import Wei, reverts, chain, web3
LOGGER = logging.getLogger(__name__)

def test_gamblepool_stake(accounts, binaryeventMock, gamblepool, bettoken):
	logging.info('web3.eth.chainId = {}'.format(web3.eth.chainId))
	with reverts("Cant stake zero"):
		gamblepool.stake(0, {"from": accounts[0]})
	with reverts("ERC20: transfer amount exceeds balance"):
		gamblepool.stake(1, {"from": accounts[0]})

	bettoken.mint(accounts[0], 100, {"from": accounts[0]})
	with reverts("ERC20: transfer amount exceeds allowance"):
		gamblepool.stake(1, {"from": accounts[0]})
	bettoken.approve(gamblepool.address, 1000)
	gamblepool.stake(1, {"from": accounts[0]})
	assert gamblepool.totalStaked() == 1
	assert gamblepool.getUserBalance(accounts[0]) == 1 

def test_gamblepool_withdraw(accounts, binaryeventMock, gamblepool, bettoken):
	with reverts("Cant withdraw zero"):
		gamblepool.withdraw(0, {"from": accounts[0]})

	with reverts(""):
		gamblepool.withdraw(1000, {"from": accounts[0]})

	before_balance = bettoken.balanceOf(accounts[0])
	gamblepool.withdraw(1, {"from": accounts[0]})
	assert gamblepool.totalStaked() == 0
	assert gamblepool.getUserBalance(accounts[0]) == 0
