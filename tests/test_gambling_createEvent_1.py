import pytest
import logging
from brownie import Wei, reverts, chain, BinaryEvents
LOGGER = logging.getLogger(__name__)

def test_gamble_createEvent(accounts, binaryevents,gamblepool):
    gamblepool.addGame(
        binaryevents.address, 
        'Binary Events', 
        '0x0000000000000000000000000000000000000000',
    	{"from": accounts[0]})
    
    # создаем событие для игры
    with reverts(""):
        gamblepool.createEvent2(
            0, #_gameId
            chain.time() + 100 ,
            chain.time() + 66 ,
            'BTC/USD',
            [
                [1,4815987599255,0,0,0], #MoreOrEqual
                [4,4815987599255,0,0,0],  #less
                [3,4815987599255,0,0,0]  #
            ],
            {'from':accounts[0]}
        )
    addr = gamblepool.games(0)[0]
    gameContract = BinaryEvents.at(addr)
