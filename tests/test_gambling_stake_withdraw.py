import pytest
import logging
from brownie import Wei, reverts
LOGGER = logging.getLogger(__name__)

BET_MINT_AMOUNT = 200e18

def test_gamble_stake_fail(accounts, bettoken, gamblepool):
    #stake 0
    with reverts('Cant stake zero'):
        gamblepool.stake(0, {'from':accounts[1]})
    
    #not enough balance
    bettoken.approve(gamblepool.address, BET_MINT_AMOUNT/2, {'from':accounts[1]})
    with reverts('ERC20: transfer amount exceeds balance'):
        gamblepool.stake(BET_MINT_AMOUNT, {'from':accounts[1]})
    bettoken.mint(accounts[1], BET_MINT_AMOUNT, {'from':accounts[0]})
    assert bettoken.balanceOf(accounts[1]) == BET_MINT_AMOUNT

    #not enough allowance
    with reverts('ERC20: transfer amount exceeds allowance'):
        gamblepool.stake(BET_MINT_AMOUNT, {'from':accounts[1]})
    assert bettoken.balanceOf(accounts[1]) == BET_MINT_AMOUNT

def test_gamble_withdraw(accounts, bettoken, gamblepool):
    #withdraw 0
    with reverts('Cant withdraw zero'):
        gamblepool.withdraw(0, {'from':accounts[1]})

    #amount is bigger than staked amount
    with reverts(''):
        gamblepool.withdraw(BET_MINT_AMOUNT/2, {'from':accounts[1]})

    bettoken.approve(gamblepool.address, BET_MINT_AMOUNT, {'from':accounts[1]})
    gamblepool.stake(BET_MINT_AMOUNT, {'from':accounts[1]})
    assert gamblepool.getUserBalance(accounts[1]) == BET_MINT_AMOUNT
    assert gamblepool.totalStaked() == BET_MINT_AMOUNT
    assert bettoken.balanceOf(gamblepool.address) == BET_MINT_AMOUNT
    assert bettoken.balanceOf(accounts[1]) == 0
    
    gamblepool.withdraw(BET_MINT_AMOUNT, {'from':accounts[1]})    
    assert gamblepool.getUserBalance(accounts[1]) == 0
    assert gamblepool.totalStaked() == 0
    assert bettoken.balanceOf(gamblepool.address) == 0
    assert bettoken.balanceOf(accounts[1]) == BET_MINT_AMOUNT