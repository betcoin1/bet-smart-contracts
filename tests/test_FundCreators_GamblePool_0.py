import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
from createFundAmount import createFundAmount
from createFunds import createFunds
LOGGER = logging.getLogger(__name__)

rewards = []
total_percent = 0

#several times make events, different creators, all creators claim reward from Creator's fund

#one game,  one events, a lot of acc make bets on different outcomes. Outcome1 wins, same acc get prize for each bet of outcome1
def test_prepare_data_for_event_1(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	global total_percent
	acc_count = 8
	amount_mint = 100e18
	bet_amount = 10e18
	createFunds(accounts, gamblepool, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery)
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[0], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

	i = 0
	while i < 6:
		total_percent = total_percent + gamblepool.getFund(i)[1] 
		i = i + 1

def test_settle_event_1(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	before_inBetsAmount = gamblepool.inBetsAmount()
	rewards.append(before_inBetsAmount)
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))
	assert gamblepool.inBetsAmount() == 0
	assert gamblepool.fundBalance() == before_inBetsAmount
	assert gamblepool.eventsSettlement(0, eventId)[0] == before_inBetsAmount



#one game,  one events, a lot of acc make bets on different outcomes. Outcome1 wins, same acc get prize for each bet of outcome1
def test_prepare_data_for_event_2(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	acc_count = 6
	amount_mint = 100e18
	bet_amount = 10e18
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[1], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

def test_settle_event_2(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))
	before_inBetsAmount = gamblepool.inBetsAmount()
	before_fundBalance = gamblepool.fundBalance()
	rewards.append(before_inBetsAmount)
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))
	assert gamblepool.inBetsAmount() == 0
	assert gamblepool.fundBalance() == before_fundBalance+before_inBetsAmount
	assert gamblepool.eventsSettlement(0, eventId)[0] == before_inBetsAmount

def test_claimFund_1(accounts, binaryevents,gamblepool, bettoken, fundCreators01):
	logging.info('inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('fundBalance() = {}'.format(gamblepool.fundBalance()))
	logging.info('creatorsReward0 = {}'.format(fundCreators01.creatorsReward(accounts[0])))
	logging.info('creatorsReward1 = {}'.format(fundCreators01.creatorsReward(accounts[1])))

	bba0 = gamblepool.getUserBalance(accounts[0])
	bba1 = gamblepool.getUserBalance(accounts[1])
	bbf = gamblepool.getUserBalance(fundCreators01)
	before_fundBalance = gamblepool.fundBalance()


	assert fundCreators01.creatorsReward(accounts[0]) == rewards[0] * gamblepool.getFund(1)[1]/total_percent
	tx = gamblepool.claimFund(1, 888, {'from': accounts[0]})
	logging.info('claimed_amount = {}'.format(tx.events['UserRewardClaimed']['reward']))

	logging.info('rewards[0] = {}'.format(rewards[0]))
	assert tx.events['UserRewardClaimed']['reward'] == rewards[0] * gamblepool.getFund(1)[1]/total_percent
	assert fundCreators01.creatorsReward(accounts[0]) == 0
	assert gamblepool.getUserBalance(accounts[0]) == bba0 + rewards[0] * gamblepool.getFund(1)[1]/total_percent
	assert gamblepool.getUserBalance(fundCreators01) == bbf - rewards[0] * gamblepool.getFund(1)[1]/total_percent
	assert gamblepool.fundBalance() == before_fundBalance - rewards[0] * gamblepool.getFund(1)[1]/total_percent


	bbf = gamblepool.getUserBalance(fundCreators01)
	before_fundBalance = gamblepool.fundBalance()

	assert fundCreators01.creatorsReward(accounts[1]) == rewards[1] * gamblepool.getFund(1)[1]/total_percent
	tx = gamblepool.claimFund(1, 888, {'from': accounts[1]})
	logging.info('claimed_amount = {}'.format(tx.events['UserRewardClaimed']['reward']))

	logging.info('rewards[1] = {}'.format(rewards[1]))
	assert tx.events['UserRewardClaimed']['reward'] == rewards[1] * gamblepool.getFund(1)[1]/total_percent
	assert fundCreators01.creatorsReward(accounts[1]) == 0
	assert gamblepool.getUserBalance(accounts[1]) == bba1 + rewards[1] * gamblepool.getFund(1)[1]/total_percent
	assert gamblepool.getUserBalance(fundCreators01) == bbf - rewards[1] * gamblepool.getFund(1)[1]/total_percent
	assert gamblepool.fundBalance() == before_fundBalance - rewards[1] * gamblepool.getFund(1)[1]/total_percent



	
def test_claimFund_fail(accounts, gamblepool):
	with reverts("No funds"):
		gamblepool.claimFund(1, 888, {'from': accounts[3]})

	with reverts("No funds"):
		gamblepool.claimFund(1, 888, {'from': accounts[0]})




#one game,  one events, a lot of acc make bets on different outcomes. Outcome1 wins, same acc get prize for each bet of outcome1
def test_prepare_data_for_event_3(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	acc_count = 4
	amount_mint = 100e18
	bet_amount = 1e18
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[1], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

def test_settle_event_3(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))
	before_inBetsAmount = gamblepool.inBetsAmount()
	before_fundBalance = gamblepool.fundBalance()
	rewards.append(before_inBetsAmount)
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))
	assert gamblepool.inBetsAmount() == 0
	assert gamblepool.fundBalance() == before_fundBalance+before_inBetsAmount
	assert gamblepool.eventsSettlement(0, eventId)[0] == before_inBetsAmount

def test_claimFund_2(accounts, binaryevents,gamblepool, bettoken, fundCreators01):
	logging.info('inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('fundBalance() = {}'.format(gamblepool.fundBalance()))
	logging.info('creatorsReward0 = {}'.format(fundCreators01.creatorsReward(accounts[0])))
	logging.info('creatorsReward1 = {}'.format(fundCreators01.creatorsReward(accounts[1])))

	bba1 = gamblepool.getUserBalance(accounts[1])
	bbf = gamblepool.getUserBalance(fundCreators01)
	before_fundBalance = gamblepool.fundBalance()

	assert fundCreators01.creatorsReward(accounts[1]) == rewards[2] * gamblepool.getFund(1)[1]/total_percent
	tx = gamblepool.claimFund(1, 888, {'from': accounts[1]})
	logging.info('claimed_amount = {}'.format(tx.events['UserRewardClaimed']['reward']))

	logging.info('rewards[1] = {}'.format(rewards[1]))
	assert tx.events['UserRewardClaimed']['reward'] == rewards[2] * gamblepool.getFund(1)[1]/total_percent
	assert fundCreators01.creatorsReward(accounts[1]) == 0
	assert gamblepool.getUserBalance(accounts[1]) == bba1 + rewards[2] * gamblepool.getFund(1)[1]/total_percent
	assert gamblepool.getUserBalance(fundCreators01) == bbf - rewards[2] * gamblepool.getFund(1)[1]/total_percent
	assert gamblepool.fundBalance() == before_fundBalance - rewards[2] * gamblepool.getFund(1)[1]/total_percent
	