import pytest
import logging, time
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0

def test_binaryevents_create(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = binaryeventMock.getEventCount() - 1
#before deadline, state Active
# after expire date, state Cancelled
# after expire date, state Finished
def test_binaryevents_checkStateWithSave_1(accounts, binaryeventMock):
    global eventId
    #state Active  before deadline
    binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 0 #check state Active
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price

    with reverts(""):
        binaryeventMock.checkStateWithSave(eventId + 2)

    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    # cancel bet
   
    tx = binaryeventMock.decBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEvent(eventId)[1] == 0 #check state Active
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price
    #expiration
    chain.sleep(3600)
    chain.mine()
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEvent(eventId)[1] == 3 #check state Canceled
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price
    #state Canceled
    with reverts("Event state must be Active (0) or Hold (1)"):
        binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 3 #check state Canceled
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price


    tx  = binaryeventMock.createEvent(
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = binaryeventMock.getEventCount() - 1
    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    chain.sleep(3600)
    chain.mine()
    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check state Finished
    assert binaryeventMock.getEvent(eventId)[6] > 0 #check price

    # try to finish again
    with reverts("Event state must be Active (0) or Hold (1)"):
        binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check state Finished
    assert binaryeventMock.getEvent(eventId)[6] > 0 #check price

#between deadline and expire date, state Hold
def test_binaryevents_checkStateWithSave_2(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 7200 ,
        chain.time() + 3600 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = binaryeventMock.getEventCount() - 1

    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    
    chain.sleep(3601)
    chain.mine()
    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEvent(eventId)[1] == 1 ##check state Hold
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price
    # try to finish
    binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 1 #check state Hold
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price

#between deadline and expire date, state Cancelled
def test_binaryevents_checkStateWithSave_3(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 7200 ,
        chain.time() + 3600 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = binaryeventMock.getEventCount() - 1

    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )

    # cancel bet
   
    tx = binaryeventMock.decBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    
    chain.sleep(3601)
    chain.mine()
    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEvent(eventId)[1] == 3 ##check state Cancelled
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price
    # try to finish
    with reverts("Event state must be Active (0) or Hold (1)"):
        binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 3 #check state Cancelled
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price

#between deadline and expire date, state Active
def test_binaryevents_checkStateWithSave_4(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 7200 ,
        chain.time() + 3600 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = binaryeventMock.getEventCount() - 1

    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    
    chain.sleep(3601)
    chain.mine()

    binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 1 #check state Hold
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price

#after expire date, state Active
def test_binaryevents_checkStateWithSave_5(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 100 ,
        chain.time() + 60 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = binaryeventMock.getEventCount() - 1

    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    
    chain.sleep(7201)
    chain.mine()
    # try to finish
    binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check state Finished
    assert binaryeventMock.getEvent(eventId)[6] > 0 #check price

#after expire date, state Hold
def test_binaryevents_checkStateWithSave_6(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 7200 ,
        chain.time() + 3600 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = binaryeventMock.getEventCount() - 1

    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    
    chain.sleep(3601)
    chain.mine()

    #make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEvent(eventId)[1] == 1 #check state Hold
    assert binaryeventMock.getEvent(eventId)[6] == 0 #check price

    chain.sleep(3601)
    chain.mine()
    # try to finish
    binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check state Finished
    assert binaryeventMock.getEvent(eventId)[6] > 0 #check price
