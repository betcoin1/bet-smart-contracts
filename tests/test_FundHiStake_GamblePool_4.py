import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
from createFundAmount import createFundAmount
from createFunds import createFunds
LOGGER = logging.getLogger(__name__)

rewards = []
total_percent = 0
acc_count = 8

#several time make events, 
#acc7 skips claiming and than claim several epoch. 
#0-5 accounts make bets after
#Other accounts claim always in time. 
#After claim for acc 7 all accounts make bets and claim except 6-7 accounts

#one game,  one events, a lot of acc make bets on different outcomes. Outcome1 wins, same acc get prize for each bet of outcome1
def test_prepare_data_for_event_0(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	global total_percent
	global acc_count
	amount_mint = 100e18
	bet_amount = 10e18
	createFunds(accounts, gamblepool, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery)
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[0], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

#close event 0
def test_settle_event_0(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	before_inBetsAmount = gamblepool.inBetsAmount()
	rewards.append(before_inBetsAmount)
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))
	assert gamblepool.inBetsAmount() == 0
	assert gamblepool.fundBalance() == before_inBetsAmount
	assert gamblepool.eventsSettlement(0, eventId)[0] == before_inBetsAmount

	fundHiStake01.closeCurrentEpoch()

 #claim fund, acc7 skip claim in 0 epoch
def test_claim_HiFund_0(accounts, binaryevents,gamblepool, fundHiStake01):
	global acc_count
	i = 0
	while i < acc_count - 1:
		bba = gamblepool.getUserBalance(accounts[i])
		bbf = gamblepool.getUserBalance(fundHiStake01)
		brc = fundHiStake01.getRewardEpoch(0)[1]
		gamblepool.claimFund(2, 0, {"from": accounts[i]})
		logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(i, fundHiStake01.getUser(accounts[i])))
		logging.info('fundHiStake01.getRewardEpoch(0)= {}'.format(fundHiStake01.getRewardEpoch(0)))
		assert bba < gamblepool.getUserBalance(accounts[i])
		assert bbf > gamblepool.getUserBalance(fundHiStake01)
		assert fundHiStake01.getUser(accounts[i])[0][3] > 0
		assert fundHiStake01.getRewardEpoch(0)[1] > brc

		i += 1

#create new epoch 1. Acc 0-5 make bets
def test_prepare_data_for_event_1(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	global total_percent
	global acc_count
	acc_count = 6
	amount_mint = 100e18
	bet_amount = 10e18
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[0], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

#close event 1
def test_settle_event_1(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	before_inBetsAmount = gamblepool.inBetsAmount()
	rewards.append(before_inBetsAmount)
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))
	
	fundHiStake01.closeCurrentEpoch()

 #claim fund, acc7 skip claim in 1 epoch again
def test_claim_HiFund_1(accounts, binaryevents,gamblepool, fundHiStake01):
	global acc_count
	i = 0
	while i < acc_count:
		bba = gamblepool.getUserBalance(accounts[i])
		bbf = gamblepool.getUserBalance(fundHiStake01)
		brc = fundHiStake01.getRewardEpoch(1)[1]
		gamblepool.claimFund(2, 1, {"from": accounts[i]})
		logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(i, fundHiStake01.getUser(accounts[i])))
		logging.info('fundHiStake01.getRewardEpoch(1)= {}'.format(fundHiStake01.getRewardEpoch(1)))
		assert bba < gamblepool.getUserBalance(accounts[i])
		assert bbf > gamblepool.getUserBalance(fundHiStake01)
		assert fundHiStake01.getUser(accounts[i])[1][3] > 0
		assert fundHiStake01.getRewardEpoch(1)[1] > brc

		i += 1

#create new epoch 2, Acc 0-5 make bets
def test_prepare_data_for_event_2(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	global total_percent
	global acc_count
	amount_mint = 100e18
	bet_amount = 10e18
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[0], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))


#close event 2
def test_settle_event_2(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	before_inBetsAmount = gamblepool.inBetsAmount()
	rewards.append(before_inBetsAmount)
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

	fundHiStake01.closeCurrentEpoch()

#claim fund, acc7 claim before epoch 2, than epoch 0, 1
def test_claim_HiFund_2(accounts, binaryevents,gamblepool, fundHiStake01):
	logging.info('getAvailableReward(accounts[7], 0) = {}'.format(fundHiStake01.getAvailableReward(accounts[7], 0)))
	logging.info('getAvailableReward(accounts[7], 1) = {}'.format(fundHiStake01.getAvailableReward(accounts[7], 1)))
	logging.info('getAvailableReward(accounts[7], 2) = {}'.format(fundHiStake01.getAvailableReward(accounts[7], 2)))
	global acc_count
	i = 0
	while i < acc_count:
		bba = gamblepool.getUserBalance(accounts[i])
		bbf = gamblepool.getUserBalance(fundHiStake01)
		brc = fundHiStake01.getRewardEpoch(2)[1]
		gamblepool.claimFund(2, 2, {"from": accounts[i]})
		logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(i, fundHiStake01.getUser(accounts[i])))
		logging.info('fundHiStake01.getRewardEpoch(2)= {}'.format(fundHiStake01.getRewardEpoch(2)))
		assert bba < gamblepool.getUserBalance(accounts[i])
		assert bbf > gamblepool.getUserBalance(fundHiStake01)
		assert fundHiStake01.getUser(accounts[i])[2][3] > 0
		assert fundHiStake01.getRewardEpoch(2)[1] > brc

		i += 1
	#acc 7 claims skiped epoch
	gamblepool.claimFund(2, 0, {"from": accounts[7]})
	gamblepool.claimFund(2, 1, {"from": accounts[7]})
	gamblepool.claimFund(2, 2, {"from": accounts[7]})

	logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(7, fundHiStake01.getUser(accounts[7])))
	logging.info('fundHiStake01.getRewardEpoch(0)= {}'.format(fundHiStake01.getRewardEpoch(0)))
	logging.info('fundHiStake01.getRewardEpoch(1)= {}'.format(fundHiStake01.getRewardEpoch(1)))
	logging.info('fundHiStake01.getRewardEpoch(2)= {}'.format(fundHiStake01.getRewardEpoch(2)))
	
	#check claimed amount in user epoch
	assert fundHiStake01.getUser(accounts[7])[0][3] >0

	#check rewardAmount and rewardClaimed
	assert fundHiStake01.getRewardEpoch(0)[0] == fundHiStake01.getRewardEpoch(0)[1]

#create new epoch 3, all accounts (0-6) make bets
def test_prepare_data_for_event_3(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	global total_percent
	global acc_count
	acc_count = 6
	amount_mint = 100e18
	bet_amount = 10e18
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[0], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))


#close event 3
def test_settle_event_3(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	logging.info('eventId = {}'.format(eventId))
	before_inBetsAmount = gamblepool.inBetsAmount()
	rewards.append(before_inBetsAmount)
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

	fundHiStake01.closeCurrentEpoch()

#claim fund
def test_claim_HiFund_3(accounts, binaryevents,gamblepool, fundHiStake01):
	global acc_count
	i = 0
	while i < acc_count:
		bba = gamblepool.getUserBalance(accounts[i])
		bbf = gamblepool.getUserBalance(fundHiStake01)
		brc = fundHiStake01.getRewardEpoch(3)[1]
		gamblepool.claimFund(2, 3, {"from": accounts[i]})
		logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(i, fundHiStake01.getUser(accounts[i])))
		logging.info('fundHiStake01.getRewardEpoch(3)= {}'.format(fundHiStake01.getRewardEpoch(3)))
		assert bba < gamblepool.getUserBalance(accounts[i])
		assert bbf > gamblepool.getUserBalance(fundHiStake01)
		assert fundHiStake01.getUser(accounts[i])[3][3] > 0
		assert fundHiStake01.getRewardEpoch(3)[1] > brc

		i += 1
	#check rewardAmount and rewardClaimed in 3 epoch
	assert fundHiStake01.getRewardEpoch(3)[0] == fundHiStake01.getRewardEpoch(3)[1]

	logging.info('fundHiStake01.getRewardEpoch(1)= {}'.format(fundHiStake01.getRewardEpoch(1)))
	logging.info('fundHiStake01.getRewardEpoch(2)= {}'.format(fundHiStake01.getRewardEpoch(2)))
	logging.info('fundHiStake01.getRewardEpoch(3)= {}'.format(fundHiStake01.getRewardEpoch(3)))