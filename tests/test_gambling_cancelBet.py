import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0

def test_gamble_cancelBet(accounts, binaryevents,gamblepool, bettoken):
    gamblepool.addGame(
        binaryevents.address, 
        'Binary Events', 
        '0x0000000000000000000000000000000000000000',
    	{"from": accounts[0]})
    # create event for game
    gamblepool.createEvent(
        0, #_gameId
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        {'from':accounts[0]}
    )
    eventId = binaryevents.getEventCount() - 1

    # stake
    #acc 0
    bettoken.mint(accounts[0], 10000, {"from": accounts[0]})
    bettoken.approve(gamblepool.address, 10000, {"from": accounts[0]})
    gamblepool.stake(10000, {"from": accounts[0]})
    #счет 1
    bettoken.mint(accounts[1], 10000, {"from": accounts[0]})
    bettoken.approve(gamblepool.address, 10000, {"from": accounts[1]})
    gamblepool.stake(10000, {"from": accounts[1]})

    #nonexist bet_id
    with reverts(""):
        gamblepool.cancelBet(1,{'from':accounts[0]})

    bet_amount = 1000
    tx = gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
    bet_id = gamblepool.getUsersBetsCount(accounts[0].address) - 1

    #account does not own this bet_id 
    with reverts(""):
        gamblepool.cancelBet(bet_id,{'from':accounts[1]})

    
    # cancel bet with moving date more than expire date, status is Active
def test_gamble_cancelBet_1(accounts, binaryevents, gamblepool):

    logging.info('Time machine running.....+1 hour...................................')
    chain.sleep(3600)
    chain.mine()
    #expire date  has arrived
    # change status of event - try to cancel bet for outcome 0 from account0
    before_balance0 = gamblepool.getUserBalance(accounts[0].address)
    bet_id = gamblepool.getUsersBetsCount(accounts[0].address) - 1
    bet_amount = 1000
    gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 2 #check status Finish
    assert binaryevents.getEvent(eventId)[5][0][3] == 1; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == 1 #check bet quantity for acc 0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after cancellation of bet for acc 0
    assert gamblepool.inBetsAmount() == bet_amount # check collected coins of bets 

    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    # check bet
    assert bet[0] == binaryevents.address #check contract address in event
    assert bet[1] == eventId # check event in bet
    assert bet[2] == 0 # check outcome in bet
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 0 # check bet status
    assert bet[6] == 0 # check bet result 

    #one more time try to cancel bet  - event status is finished, acc0 tries
    before_balance0 = gamblepool.getUserBalance(accounts[0].address)
    with reverts("Event is Finished"):
        tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    logging.info('binaryevents.getEvent(0) = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 2 #check status Finish
    assert binaryevents.getEvent(eventId)[5][0][3] == 1; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == 1 #check bet quantity for acc 0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == bet_amount # check collected coins of bets 

    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    # check bet
    assert bet[0] == binaryevents.address #check contract address in event
    assert bet[1] == eventId # check event in bet
    assert bet[2] == 0 # check outcome in bet
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 0 # check bet status
    assert bet[6] == 0 # check bet result 

#cancelBet when status is Cancelled, current time is more than expire date
def test_test_gamble_cancelBet_2(accounts, binaryevents, gamblepool):
    # create event for game
    gamblepool.createEvent(
        0, #_gameId
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        {'from':accounts[0]}
    )
    eventId = binaryevents.getEventCount() - 1

    # make bet 
    # bet in game 0 event 1 outcome 0
    before_balance = gamblepool.getUserBalance(accounts[0].address)
    all_bets_amount = gamblepool.inBetsAmount()
    bet_amount = 1000
    tx = gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
    logging.info('binaryevents.getEvent(eventId) = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[5][0][3] == 1; # check quantity  of bets  for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check amount of bets for outcome 0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance - bet_amount # check balance of acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount + bet_amount # check collected coins of bets 

    # cancel bet
    bet_id = gamblepool.getUsersBetsCount(accounts[0].address) - 1
    before_balance = gamblepool.getUserBalance(accounts[0].address)
    all_bets_amount = gamblepool.inBetsAmount()
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})

    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert gamblepool.getUsersBetsCount(accounts[0]) == bet_id + 1 #check bets of acc0
    # check bet
    assert bet[0] == binaryevents.address #check contract address in event
    assert bet[1] == eventId # check event in bet
    assert bet[2] == 0 # check outcome in bet
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 1 # check bet status
    assert bet[6] == 0 # check bet result 
    # check balances and funds
    penalty = (bet_amount/100)*gamblepool.CANCEL_BET_PENALTY_PERCENT()
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance + bet_amount - penalty  # check balance after the cancellation
    assert gamblepool.fundBalance() == bet_amount/100*gamblepool.CANCEL_BET_PENALTY_PERCENT() #check funds
    assert gamblepool.inBetsAmount() == all_bets_amount - bet_amount # check collected coins of bets 
    #check event 
    assert binaryevents.getEvent(eventId)[1] == 0 #check status
    assert binaryevents.getEvent(eventId)[5][0][3] == 0; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == 0; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1

    chain.sleep(3600)
    chain.mine()

    # change status - new status Cancelled - account0 tries to cancel bet
    before_balance0 = gamblepool.getUserBalance(accounts[0].address)
    before_balance1 = gamblepool.getUserBalance(accounts[1].address)
    all_bets_amount = gamblepool.inBetsAmount()
    all_funds = gamblepool.fundBalance()
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 1 # check bet status
    assert bet[6] == 0 # check bet result 

    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 3 #check status Cancelled
    assert binaryevents.getEvent(eventId)[5][0][3] == 0; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == 0; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bet_id + 1 #check bets of acc0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount #  check collected coins of bets 
    assert gamblepool.fundBalance() ==  all_funds #check funds

    # try again to cancel bet from acc0
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})

    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 1 # check bet status
    assert bet[6] == 0 # check bet result 

    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 3 #check status Cancelled
    assert binaryevents.getEvent(eventId)[5][0][3] == 0; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == 0; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bet_id + 1 #check bets of acc0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount # check collected coins of bets 
    assert gamblepool.fundBalance() ==  all_funds #check funds

#cancelBet when status is Hold, current time is more than deadline
#cancelBet when status is Hold, current time is more than expire date
def test_test_gamble_cancelBet_3(accounts, binaryevents, gamblepool):
    # create event for game
    gamblepool.createEvent(
        0, #_gameId
        chain.time() + 7200 ,
        chain.time() + 3600 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        {'from':accounts[0]}
    )
    eventId = binaryevents.getEventCount() - 1

    # make bet 
    # bet in game 0 event 2 outcome 0
    before_balance = gamblepool.getUserBalance(accounts[0].address)
    all_bets_amount = gamblepool.inBetsAmount()
    bet_amount = 1000
    tx = gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
    logging.info('binaryevents.getEvent(eventId) = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[5][0][3] == 1; # check quantity  of bets  for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check amount of bets for outcome 0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance - bet_amount # check balance of acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount + bet_amount # check collected coins of bets 

    chain.sleep(3601)
    chain.mine()

    # change status - new status Hold - account0 tries to cancel bet
    bet_id = gamblepool.getUsersBetsCount(accounts[0].address) - 1
    before_balance0 = gamblepool.getUserBalance(accounts[0].address)
    all_bets_amount = gamblepool.inBetsAmount()
    all_funds = gamblepool.fundBalance()
    bets = gamblepool.getUsersBetsCount(accounts[0].address)
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 0 # check bet status
    assert bet[6] == 0 # check bet result 

    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 1 #check status Hold
    assert binaryevents.getEvent(eventId)[5][0][3] == 1; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bets #check bets of acc0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount #  check collected coins of bets 
    assert gamblepool.fundBalance() ==  all_funds #check funds

    # try again to cancel bet from acc0
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 0 # check bet status
    assert bet[6] == 0 # check bet result 

    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 1 #check status Hold
    assert binaryevents.getEvent(eventId)[5][0][3] == 1; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bets #check bets of acc0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount # check collected coins of bets 
    assert gamblepool.fundBalance() ==  all_funds #check funds
    
    chain.sleep(3601)
    chain.mine()

    # try again to cancel bet from acc0
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})

    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 0 # check bet status
    assert bet[6] == 0 # check bet result 
    
    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 2 #check status Finish
    assert binaryevents.getEvent(eventId)[5][0][3] == 1; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bets #check bets of acc0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount # check collected coins of bets 
    assert gamblepool.fundBalance() ==  all_funds #check funds

#cancelbet when status is Cancelled and current time more than deadline/expire date
def test_test_gamble_cancelBet_4(accounts, binaryevents, gamblepool):
    # create event for game
    gamblepool.createEvent(
        0, #_gameId
        chain.time() + 7200 ,
        chain.time() + 3600 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        {'from':accounts[0]}
    )
    eventId = binaryevents.getEventCount() - 1

    # make bet 
    # bet in game 0 event 3 outcome 0
    before_balance = gamblepool.getUserBalance(accounts[0].address)
    all_bets_amount = gamblepool.inBetsAmount()
    bet_amount = 1000
    tx = gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
    logging.info('binaryevents.getEvent(eventId) = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[5][0][3] == 1; # check quantity  of bets  for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == bet_amount; # check amount of bets for outcome 0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance - bet_amount # check balance of acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount + bet_amount # check collected coins of bets 

    # cancel bet
    bet_id = gamblepool.getUsersBetsCount(accounts[0].address) - 1
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 1 # check bet status
    assert bet[6] == 0 # check bet result 

    chain.sleep(3601)
    chain.mine()

    # change status - new status Cancel - account0 tries to cancel bet - deadline has happened
    before_balance0 = gamblepool.getUserBalance(accounts[0].address)
    before_balance1 = gamblepool.getUserBalance(accounts[1].address)
    all_bets_amount = gamblepool.inBetsAmount()
    all_funds = gamblepool.fundBalance()
    bets = gamblepool.getUsersBetsCount(accounts[0].address)
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 1 # check bet status
    assert bet[6] == 0 # check bet result 

    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 3 #check status Cancelled
    assert binaryevents.getEvent(eventId)[5][0][3] == 0; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == 0; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bets #check bets of acc0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount #  check collected coins of bets 
    assert gamblepool.fundBalance() ==  all_funds #check funds

    # try again to cancel bet from acc0 - status is Cancelled - deadline has happened
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 1 # check bet status
    assert bet[6] == 0 # check bet result 

    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 3 #check status Cancelled
    assert binaryevents.getEvent(eventId)[5][0][3] == 0; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == 0; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bets #check bets of acc0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount # check collected coins of bets 
    assert gamblepool.fundBalance() ==  all_funds #check funds
    
    chain.sleep(3601)
    chain.mine()

    # try again to cancel bet from acc0 - expire date has happened, status is Cancelled
    tx = gamblepool.cancelBet(bet_id,{'from':accounts[0]})
    
    # check bet
    bet = gamblepool.getUsersBetByIndex(accounts[0].address, bet_id)
    assert bet[3] == bet_amount # check amount of bet 
    assert bet[5] == 1 # check bet status
    assert bet[6] == 0 # check bet result 

    logging.info('binaryevents.getEvent = {}'.format(binaryevents.getEvent(eventId)))
    assert binaryevents.getEvent(eventId)[1] == 3 #check status Cancelled
    assert binaryevents.getEvent(eventId)[5][0][3] == 0; # check bet quantity for outcome 0
    assert binaryevents.getEvent(eventId)[5][0][2] == 0; # check bet amount for outcome 0
    assert binaryevents.getEvent(eventId)[5][1][3] == 0; # check bet quantity for outcome 1
    assert binaryevents.getEvent(eventId)[5][1][2] == 0; # check bet amount for outcome 1
    assert gamblepool.getUsersBetsCount(accounts[0]) == bets #check bets of acc0
    assert gamblepool.getUserBalance(accounts[0].address) == before_balance0 # check balance after bet for acc 0
    assert gamblepool.inBetsAmount() == all_bets_amount # check collected coins of bets 
    assert gamblepool.fundBalance() ==  all_funds #check funds