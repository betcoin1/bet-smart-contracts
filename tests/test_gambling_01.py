import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

BET_MINT_AMOUNT = 200e18

def test_gamble_stake(accounts, bettoken, gamblepool):
    bettoken.mint(accounts[0], BET_MINT_AMOUNT, {'from':accounts[0]})
    assert bettoken.balanceOf(accounts[0]) == BET_MINT_AMOUNT
    bettoken.transfer(accounts[1], BET_MINT_AMOUNT/2, {'from':accounts[0]})
    bettoken.approve(gamblepool.address, BET_MINT_AMOUNT/2, {'from':accounts[1]})
    gamblepool.stake(BET_MINT_AMOUNT/2, {'from':accounts[1]})
    logging.info(gamblepool.getUserBalance(accounts[1]))
    assert gamblepool.getUserBalance(accounts[1]) == BET_MINT_AMOUNT/2
    assert bettoken.balanceOf(accounts[1]) == 0
    assert gamblepool.totalStaked() == BET_MINT_AMOUNT/2

def test_gamble_withdraw(accounts, bettoken, gamblepool):
    gamblepool.withdraw(BET_MINT_AMOUNT/2, {'from':accounts[1]})
    assert gamblepool.getUserBalance(accounts[1]) == 0
    assert gamblepool.totalStaked() == 0

def test_gamble_add_game(accounts, binaryevents,gamblepool):
    gamblepool.addGame(
        binaryevents.address, 
        'Binary Events', 
        '0x0000000000000000000000000000000000000000',
    )
    logging.info('Game added: {}'.format(gamblepool.games(0)))
    assert gamblepool.getGamesCount() == 1

def test_binaryevents_create(accounts, binaryevents, gamblepool):
    tx  = gamblepool.createEvent(
        0, #_gameId
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        {'from':accounts[0]}

    )
    logging.info(binaryevents.getEvent(0))
    logging.info(tx.events)
    assert tx.events['NewEvent']['source'] == binaryevents.address
    assert binaryevents.getEventCount() == 1

def test_make_bet(accounts, bettoken, binaryevents, gamblepool):
    bettoken.approve(gamblepool.address, bettoken.balanceOf(accounts[1]), {'from':accounts[1]})
    gamblepool.stake(bettoken.balanceOf(accounts[1]), {'from':accounts[1]})
    logging.info('Better balance in pool {} '.format(gamblepool.getUserBalance(accounts[1])))
    gamblepool.makeBet(
            gamblepool.getGamesCount()-1,
            binaryevents.getEventCount()-1,
            0,
            1e18,
        {'from':accounts[1]}

    )
    logging.info('Bet: {}'.format(gamblepool.getUsersBetByIndex(accounts[1], 0)))
    assert gamblepool.getUsersBetsCount(accounts[1]) ==1

