import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1000
currEpoch = 0

# one account
#stake in 0 epoch
#without other balance moves
#skip 3 epochs
#claim after 3 epoch
#between epoch 50 blocks

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[1], amount, {"from": accounts[0]})

    #stake tokens
    bettoken.approve(mockgamblepool.address, 2*amount, {"from": accounts[1]})
    logging.info('*****************stake acc1************')
    logging.info('currEpoch = {}'.format(currEpoch))
    mockgamblepool.stake(amount, {"from": accounts[1]})

def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 2

def test_settle_event_2(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 3

def test_settle_event_3(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 4


def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('////////////////////////CLAIM EPOCH 0/////////////////////////')
    logging.info('currEpoch = {}'.format(currEpoch))
    before_userRewards = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    bba1 = mockgamblepool.getUserBalance(accounts[1])
    logging.info('before_userRewards = {}'.format(before_userRewards))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_0!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[1]})
    after_userRewards = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('after_userRewards = {}'.format(after_userRewards))
    logging.info('after_RewardEpoch_0 = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    assert after_userRewards[0][3] == after_RewardEpoch[1]
    assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userRewards[0][3]
    assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
    assert after_userRewards[0][3] > 0
    assert bba1 < mockgamblepool.getUserBalance(accounts[1])

    logging.info('////////////////////////CLAIM EPOCH 1/////////////////////////')
    logging.info('currEpoch = {}'.format(currEpoch + 1))
    before_userRewards = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 1)
    bba1 = mockgamblepool.getUserBalance(accounts[1])
    logging.info('before_userRewards = {}'.format(before_userRewards))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_1!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch + 1, {'from': accounts[1]})
    after_userRewards = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 1)
    logging.info('after_userRewards = {}'.format(after_userRewards))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    assert after_userRewards[1][3] == after_RewardEpoch[1]
    assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userRewards[1][3]
    assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
    assert after_userRewards[1][3] > 0
    assert bba1 < mockgamblepool.getUserBalance(accounts[1])


    logging.info('////////////////////////CLAIM EPOCH 2/////////////////////////')
    logging.info('currEpoch = {}'.format(currEpoch + 2))
    before_userRewards = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 2)
    bba1 = mockgamblepool.getUserBalance(accounts[1])
    logging.info('before_userRewards = {}'.format(before_userRewards))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_2!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch + 2, {'from': accounts[1]})
    after_userRewards = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 2)
    logging.info('after_userRewards = {}'.format(after_userRewards))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    assert after_userRewards[2][3] == after_RewardEpoch[1]
    assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userRewards[2][3]
    assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
    assert after_userRewards[2][3] > 0
    assert bba1 < mockgamblepool.getUserBalance(accounts[1])

    