import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

amount = 1000e18

#two accounts
#before claiming accounts are not joined to fund
#create new epoch
#make first claim
#accounts became joined to fund
#create new epoch
#acc2 DEcreases balance after creation of new epoch
#claim for accounts
def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):

    tx = mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    #stake tokens
    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 1 stakes tokens!!')
    logging.info('fundStartBlock = {}'.format(fundHolders01.fundStartBlock()))
    bettoken.mint(accounts[1], amount, {"from": accounts[0]})

    bettoken.approve(mockgamblepool.address, amount, {"from": accounts[1]})
    
    mockgamblepool.stake(amount, {"from": accounts[1]})

    balance_before = mockgamblepool.getUserBalance(accounts[1])
    assert balance_before == amount



#stake and sellte epoch 0
def test_settle_event(accounts, mockgamblepool, bettoken, fundHolders01):
    #for next test case
    #stake tokens
    bettoken.mint(accounts[2], amount, {"from": accounts[0]})

    bettoken.approve(mockgamblepool.address, amount, {"from": accounts[2]})
    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 2 stakes tokens!!')
    logging.info('*******************************************************')
    
    mockgamblepool.stake(amount, {"from": accounts[2]})
    balance_before = mockgamblepool.getUserBalance(accounts[2])
    assert balance_before == amount

    logging.info('*******************************************************')
    logging.info('!!Create first epoch!!')
    logging.info('*******************************************************')
    logging.info('count_rewards = {}'.format(fundHolders01.getRewardEpochCount()))
    mockgamblepool.settleGameEvent(0, 1e18)
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == 2


##acc2 claims fund in epoch 0
def test_fundHolders_claimReward_2(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 2 claims tokens!!')
    logging.info('*******************************************************')
    logging.info('//// BEFORE claim////')
    logging.info('*******************************************************')

    before_userData = fundHolders01.getUser(accounts[2])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch() - 1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('userData = {}'.format(before_userData))
    logging.info('RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))
    mockgamblepool.claimFund(0, 0, {"from": accounts[2]})

    logging.info('*******************************************************')
    logging.info('//// AFTER claim////')
    logging.info('*******************************************************')
    after_userData = fundHolders01.getUser(accounts[2])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('userData = {}'.format(after_userData))
    logging.info('RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

    logging.info('*******************************************************')
    logging.info('!!!!ATTENTION!!!!')
    logging.info('*******************************************************')
    logging.info('CHECK RewardEpoch - how many tokens is rewarded and claimed= {}'.format(after_RewardEpoch))
    
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]

####account1 claims in epoch 0
def test_fundHolders_claimReward_3(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 1 claims tokens!!')
    logging.info('*******************************************************')

    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch() - 1))
    before_userData = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch()-1)
    logging.info('user_data = {}'.format(before_userData))
    logging.info('RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))
    
    mockgamblepool.claimFund(0, 0, {"from": accounts[1]})
    logging.info('*******************************************************')
    logging.info('//// AFTER claim////')
    logging.info('*******************************************************')
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)

    logging.info('userData = {}'.format(after_userData))
    logging.info('RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

    logging.info('*******************************************************')
    logging.info('!!!!ATTENTION!!!!')
    logging.info('*******************************************************')
    logging.info('CHECK RewardEpoch - how many tokens is rewarded and claimed= {}'.format(after_RewardEpoch))
    
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]

####withdraw part of staked tokens by acc2
def test_fundHolders_change_balance_1(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 2 change balance!!')
    logging.info('*******************************************************')
    logging.info('//// BEFORE change////')
    logging.info('*******************************************************')

    before_userData = fundHolders01.getUser(accounts[2])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch()))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch())

    mockgamblepool.withdraw(amount/2, {"from": accounts[2]})
    
    logging.info('*******************************************************')
    logging.info('//// AFTER change////')
    logging.info('*******************************************************')
    after_userData = fundHolders01.getUser(accounts[2])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch())
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))
    logging.info('userData = {}'.format(after_userData))
    logging.info('RewardEpoch = {}'.format(after_RewardEpoch))



def test_settle_event_again(accounts, mockgamblepool, bettoken, fundHolders01):
    logging.info('*******************************************************')
    logging.info('!!Close epoch 1!!')
    logging.info('*******************************************************')
    mockgamblepool.settleGameEvent(0, 1e18)
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == 3


####account2 claims in epoch 1
def test_fundHolders_claimReward_2_again(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 2 claims tokens!!')
    logging.info('*******************************************************')
    logging.info('//// BEFORE claim////')
    logging.info('*******************************************************')

    before_userData = fundHolders01.getUser(accounts[2])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch() - 1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('userData = {}'.format(before_userData))
    logging.info('RewardEpoch = {}'.format(before_RewardEpoch))
    mockgamblepool.claimFund(0, 1, {"from": accounts[2]})

    logging.info('*******************************************************')
    logging.info('//// AFTER claim////')
    logging.info('*******************************************************')
    after_userData = fundHolders01.getUser(accounts[2])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('userData = {}'.format(after_userData))
    logging.info('RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

    logging.info('*******************************************************')
    logging.info('!!!!ATTENTION!!!!')
    logging.info('*******************************************************')
    logging.info('CHECK RewardEpoch - how many tokens is rewarded and claimed= {}'.format(after_RewardEpoch))
    
    
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]


####account1 claims in epoch 1
def test_fundHolders_claimReward_3_again(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 1 claims tokens!!')
    logging.info('*******************************************************')
    logging.info('//// BEFORE claim////')
    logging.info('*******************************************************')

    before_userData = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('userData = {}'.format(before_userData))
    logging.info('RewardEpoch = {}'.format(before_RewardEpoch))
    
    mockgamblepool.claimFund(0, 1, {"from": accounts[1]})
    logging.info('*******************************************************')
    logging.info('//// AFTER claim////')
    logging.info('*******************************************************')
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)

    logging.info('userData = {}'.format(after_userData))
    logging.info('RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

    logging.info('*******************************************************')
    logging.info('!!!!ATTENTION!!!!')
    logging.info('*******************************************************')
    logging.info('CHECK RewardEpoch - how many tokens is rewarded and claimed= {}'.format(after_RewardEpoch))
    
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]


def test_settle_event_again_1(accounts, mockgamblepool, bettoken, fundHolders01):
    logging.info('*******************************************************')
    logging.info('!!Create epoch 3 and close epoch 2!!')
    logging.info('*******************************************************')
    mockgamblepool.settleGameEvent(0, 1e18)
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == 4

####account2 claims in epoch 2
def test_fundHolders_claimReward_2_next_time(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 2 claims tokens!!')
    logging.info('*******************************************************')
    logging.info('//// BEFORE claim////')
    logging.info('*******************************************************')

    before_userData = fundHolders01.getUser(accounts[2])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch() - 1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('userData = {}'.format(before_userData))
    logging.info('RewardEpoch = {}'.format(before_RewardEpoch))
    mockgamblepool.claimFund(0, 2, {"from": accounts[2]})

    logging.info('*******************************************************')
    logging.info('//// AFTER claim////')
    logging.info('*******************************************************')
    after_userData = fundHolders01.getUser(accounts[2])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('userData = {}'.format(after_userData))
    logging.info('RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

    logging.info('*******************************************************')
    logging.info('!!!!ATTENTION!!!!')
    logging.info('*******************************************************')
    logging.info('CHECK RewardEpoch - how many tokens is rewarded and claimed= {}'.format(after_RewardEpoch))
    
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]


####account1 claims in epoch 2
def test_fundHolders_claimReward_3_next_time(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('*******************************************************')
    logging.info('!!ACCOUNT 1 claims tokens!!')
    logging.info('*******************************************************')
    logging.info('//// BEFORE claim////')
    logging.info('*******************************************************')

    before_userData = fundHolders01.getUser(accounts[1])
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('userData = {}'.format(before_userData))
    logging.info('RewardEpoch = {}'.format(before_RewardEpoch))
    
    mockgamblepool.claimFund(0, 2, {"from": accounts[1]})
    logging.info('*******************************************************')
    logging.info('//// AFTER claim////')
    logging.info('*******************************************************')
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)

    logging.info('userData = {}'.format(after_userData))
    logging.info('RewardEpoch = {}'.format(after_RewardEpoch))
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))

    logging.info('*******************************************************')
    logging.info('!!!!ATTENTION!!!!')
    logging.info('*******************************************************')
    logging.info('CHECK RewardEpoch - how many tokens is rewarded and claimed= {}'.format(after_RewardEpoch))
    
    assert before_userData != after_userData
    assert before_RewardEpoch != after_RewardEpoch
    assert before_RewardEpoch[1] < after_RewardEpoch[1]



