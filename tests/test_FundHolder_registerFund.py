import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'

def test_FundHolders_registerFund(accounts, binaryevents,gamblepool, bettoken, fundHolders01):
    tx = fundHolders01.registerFund({"from": accounts[0]})
    assert tx.txid != '0'