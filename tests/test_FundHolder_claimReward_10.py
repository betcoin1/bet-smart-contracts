import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1e18
currEpoch = 0

# several accounts
#stake in 0 epoch
# several accounts increase staked balance
# several accounts decrease staked balance
# several accounts dont skip claim
# several accounts skip claim
#skipped claim accounts claim all epoch in 3 epoch


def test_gamble(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    i = 1
    while i < 9:
        bettoken.mint(accounts[i], 10*amount, {"from": accounts[0]})
        bettoken.approve(mockgamblepool.address, 10*amount, {"from": accounts[i]})
        mockgamblepool.stake(i*amount, {"from": accounts[i]})
        chain.mine(i*10)
        i += 1

def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 2

    #two accounts withdraw part of balance
    i = 1
    while i <5:
        if i%2 != 0:
            mockgamblepool.withdraw(amount, {'from': accounts[i]})
            chain.mine(i*10)
        i += 1
    # two accounts increase staked balance
    i = 5
    while i <9:
        if i%2 != 0:
            mockgamblepool.stake(amount, {'from': accounts[i]})
            chain.mine(i*10)
        i += 1

    # four acc claim in time
    i = 1
    while i <9:
        if i%2 == 0:
            mockgamblepool.claimFund(fundId, currEpoch , {'from': accounts[i]})
            chain.mine(i*10)
        i += 1


def test_settle_event_2(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 3

    # four acc claim in time
    i = 1
    while i <9:
        if i%2 == 0:
            mockgamblepool.claimFund(fundId, currEpoch+1 , {'from': accounts[i]})
            chain.mine(i*10)
        i += 1

def test_settle_event_3(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 4

    # four acc claim in time
    i = 1
    while i <9:
        if i%2 == 0:
            mockgamblepool.claimFund(fundId, currEpoch+2 , {'from': accounts[i]})
            chain.mine(i*10)
        i += 1


def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('////////////////////////CLAIM for 4 accounts /////////////////////////')
    # four acc claim in time
    j = 0
    while j < 3:
        i = 1
        while i <9:
            if i%2 != 0:
                mockgamblepool.claimFund(fundId, j, {'from': accounts[i]})
            i += 1
        j += 1
    
    i = 1
    while i < 9:
        assert fundHolders01.getUser(accounts[i])[0][3] >0
        assert fundHolders01.getUser(accounts[i])[1][3] >0
        assert fundHolders01.getUser(accounts[i])[2][3] >0
        logging.info('after_userRewards acc{}= {}'.format(i, fundHolders01.getUser(accounts[i])))
        i += 1

    i = 0
    while i < 3:
        assert fundHolders01.getRewardEpoch(i)[1] > 0
        logging.info('after_RewardEpoch_{} = {}'.format(i, fundHolders01.getRewardEpoch(i)))
        i += 1       
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked())) 
    