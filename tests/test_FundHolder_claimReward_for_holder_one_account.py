import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

amount = 1000

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    #stake tokens
    tx = mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()
    logging.info('fundStartBlock = {}'.format(fundHolders01.fundStartBlock()))
    bettoken.mint(accounts[1], amount, {"from": accounts[0]})

    bettoken.approve(mockgamblepool.address, amount, {"from": accounts[1]})
    logging.info('*****************stake acc1************')
    mockgamblepool.stake(amount, {"from": accounts[1]})
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))
    balance_before = mockgamblepool.getUserBalance(accounts[1])
    assert balance_before == amount
    assert mockgamblepool.getUserBalance(accounts[1]) == amount
    logging.info('balance_acc1 = {}'.format(mockgamblepool.getUserBalance(accounts[1])))

def test_settle_event(accounts, mockgamblepool, bettoken, fundHolders01):
    
    logging.info('count_rewards = {}'.format(fundHolders01.getRewardEpochCount()))
    mockgamblepool.settleGameEvent(0, 1e18)
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == 2

#acc1 claims in epoch 0
def test_fundHolders_claimReward_3(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('fundHolders01.getAvailableReward = {}'.format(fundHolders01.getAvailableReward(accounts[1].address)))

    balance_before = mockgamblepool.getUserBalance(accounts[1])
    before_userData = fundHolders01.getUser(accounts[1])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch() - 1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('before_user_data = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    
    mockgamblepool.claimFund(0, 0, {"from": accounts[1]})
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('after_userData = {}'.format(after_userData))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    assert before_RewardEpoch != after_RewardEpoch
    assert  after_RewardEpoch[1] == 1e18
    assert mockgamblepool.getUserBalance(accounts[1]) == amount + 1e18
    assert after_userData[0][3] == 1e18
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))
    logging.info('balance_acc1 = {}'.format(mockgamblepool.getUserBalance(accounts[1])))

def test_settle_event_again(accounts, mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.settleGameEvent(0, 1e18)
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == 3

#acc1 claims in epoch 1
def test_fundHolders_claimReward_3_again(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('fundHolders01.getAvailableReward = {}'.format(fundHolders01.getAvailableReward(accounts[1].address)))

    before_userData = fundHolders01.getUser(accounts[1])
    logging.info('Epoch() = {}'.format(fundHolders01.currentEpoch() - 1))
    before_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('before_user_data = {}'.format(before_userData))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    
    mockgamblepool.claimFund(0, 1, {"from": accounts[1]})
    after_userData = fundHolders01.getUser(accounts[1])
    after_RewardEpoch = fundHolders01.getRewardEpoch(fundHolders01.currentEpoch() - 1)
    logging.info('after_userData = {}'.format(after_userData))
    logging.info('after_RewardEpoch = {}'.format(after_RewardEpoch))
    assert before_RewardEpoch != after_RewardEpoch
    assert  after_RewardEpoch[1] == 1e18
    assert mockgamblepool.getUserBalance(accounts[1]) == amount + 1e18 + 1e18
    assert after_userData[1][3] == 1e18
    logging.info('totalStaked = {}'.format(mockgamblepool.totalStaked()))
    logging.info('balance_acc1 = {}'.format(mockgamblepool.getUserBalance(accounts[1])))

