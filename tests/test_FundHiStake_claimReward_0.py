import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1e18
oneBet = amount / 10
currEpoch = 0


# two account
#stake in 0 epoch
#without other balance moves
#claim in 3 epochs

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHiStake01):
    mockgamblepool.registerFund(
        fundHiStake01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[1], amount*10, {"from": accounts[0]})
    bettoken.mint(accounts[2], amount*10, {"from": accounts[0]})

    #stake tokens
    bettoken.approve(mockgamblepool.address, 10*amount, {"from": accounts[1]})
    bettoken.approve(mockgamblepool.address, 10*amount, {"from": accounts[2]})
    logging.info('*****************stake acc1************')
    logging.info('currentEpoch = {}'.format(fundHiStake01.currentEpoch()))
    mockgamblepool.stake(amount, {"from": accounts[1]})
    mockgamblepool.makeBet(0,0,0, oneBet , {"from": accounts[1]});
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch(),
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch())
    ))
    logging.info('*User1:{}'.format(
        fundHiStake01.getUser(accounts[1])
    ))
    chain.mine(5)
    mockgamblepool.stake(amount, {"from": accounts[1]})
    mockgamblepool.makeBet(0,0,0, oneBet , {"from": accounts[1]});
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch(),
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch())
    ))
    logging.info('*User1:{}'.format(
        fundHiStake01.getUser(accounts[1])
    ))

    logging.info('*****************stake acc2************')
    mockgamblepool.stake(amount, {"from": accounts[2]})
    mockgamblepool.makeBet(0,0,0, oneBet , {"from": accounts[2]});
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch(),
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch())
    ))
    logging.info('*User2:{}'.format(
        fundHiStake01.getUser(accounts[2])
    ))
    chain.mine(50)
    logging.info('*****mine(50)')
    mockgamblepool.stake(amount, {"from": accounts[2]})
    mockgamblepool.makeBet(0,0,0, oneBet , {"from": accounts[2]});
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch(),
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch())
    ))
    logging.info('*User2:{}'.format(
        fundHiStake01.getUser(accounts[2])
    ))
    logging.info('*inBetsAmount:{}'.format(
        mockgamblepool.inBetsAmount()
    ))
    assert mockgamblepool.inBetsAmount() == oneBet*4 



def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHiStake01):
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHiStake01.currentEpoch()!!!!!! = {}'.format(fundHiStake01.currentEpoch()))
    fundHiStake01.closeCurrentEpoch()
    assert fundHiStake01.getRewardEpochCount() ==  2
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch() -1,
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch()-1)
    ))
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch(),
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch())
    ))

####define reward for account when it is not holder,
####rewardEpoch does not equal to 0
####account is joined to fund
def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHiStake01):

    logging.info('currEpoch = {}'.format(fundHiStake01.currentEpoch()))
    before_userRewards_1 = fundHiStake01.getUser(accounts[1])
    before_userRewards_2 = fundHiStake01.getUser(accounts[2])
    logging.info('******Before reward claim*********')
    logging.info('*User1:{}'.format(
        before_userRewards_1
    ))
    logging.info('*User2:{}'.format(
        before_userRewards_2
    ))

    logging.info('getAvailableReward(1) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[1])
    ))
    logging.info('getAvailableReward(2) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[2])
    ))

    bbc1 = mockgamblepool.getUserBalance(accounts[1])
    bbc2 = mockgamblepool.getUserBalance(accounts[2])
    logging.info('***********************AFTER CLAIM_1!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    
    tx1 = mockgamblepool.claimFund(fundId, fundHiStake01.currentEpoch() - 1, {'from': accounts[1]})
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch() - 1,
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch() - 1)
    ))
    logging.info('*User1:{}'.format(
        fundHiStake01.getUser(accounts[1])
    ))

    
    tx2 = mockgamblepool.claimFund(fundId, fundHiStake01.currentEpoch() - 1, {'from': accounts[2]})
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch() - 1,
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch() - 1)
    ))
    logging.info('*User2:{}'.format(
        fundHiStake01.getUser(accounts[2])
    ))

    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    assert tx1.events['UserRewardClaimed']['reward'] + bbc1 == mockgamblepool.getUserBalance(accounts[1]) 
    assert tx2.events['UserRewardClaimed']['reward'] + bbc2 == mockgamblepool.getUserBalance(accounts[2])
    assert (tx1.events['UserRewardClaimed']['reward'] + tx2.events['UserRewardClaimed']['reward'] == 
         fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch() - 1)[1])



def test_avilable_after_claim(accounts, mockgamblepool, bettoken, fundHiStake01):
    #IN LAST CLOSED ePOCHE
    assert fundHiStake01.getAvailableReward(accounts[1]) == 0
    assert fundHiStake01.getAvailableReward(accounts[2]) == 0 

def test_next_epoch(accounts, mockgamblepool, bettoken, fundHiStake01):
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    fundHiStake01.closeCurrentEpoch()
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch() - 1,
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch() - 1)
    ))

    logging.info('getAvailableReward(1) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[1])
    ))
    logging.info('getAvailableReward(2) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[2])
    ))
    assert fundHiStake01.getRewardEpochCount() == 3

def test_skip_epoch(accounts, mockgamblepool, bettoken, fundHiStake01):
    for i in range(10):
        mockgamblepool.makeBet(0,0,0, oneBet , {"from": accounts[2]});
        mockgamblepool.settleGameEvent(currEpoch, 1e18)
        fundHiStake01.closeCurrentEpoch()
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch() - 1,
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch() - 1)
    ))


    logging.info('getAvailableReward(1) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[1])
    ))
    logging.info('getAvailableReward(2) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[2])
    ))

    logging.info('getAvailableReward(1, 11) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[1], 11)
    ))
    logging.info('getAvailableReward(2, 11) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[2], 11)
    ))
    logging.info('getAvailableReward(1, 12) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[1], 12)
    ))
    logging.info('getAvailableReward(2, 12) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[2], 12)
    ))

    logging.info('getAvailableReward(3) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[3])
    ))
    logging.info('getAvailableReward(3, 12) = {}'.format(
        fundHiStake01.getAvailableReward(accounts[3], 12)
    ))
    assert fundHiStake01.getAvailableReward(accounts[1], 9) == fundHiStake01.getAvailableReward(accounts[1], 10)


def test_claim_after_skip(accounts, mockgamblepool, bettoken, fundHiStake01):
    logging.info('Epoch Count: {}, Last Closed {}, Current {}'.format(
        fundHiStake01.getRewardEpochCount(),
        fundHiStake01.currentEpoch() - 1,
        fundHiStake01.currentEpoch()
    ))
    for i in range(fundHiStake01.getRewardEpochCount()):
        logging.info('*Epoch({} of {}):User1 {}'.format(
            i,
            fundHiStake01.getUserEpochCount(accounts[1]),
            fundHiStake01.getAvailableReward(accounts[1], i)
        ))
        logging.info('*Epoch({} of {}):User2 {}'.format(
            i,
            fundHiStake01.getUserEpochCount(accounts[2]),
            fundHiStake01.getAvailableReward(accounts[2], i)
        ))
        if fundHiStake01.getAvailableReward(accounts[1], i) > 0:
            tx1 = mockgamblepool.claimFund(fundId, i, {'from': accounts[1]})
        if fundHiStake01.getAvailableReward(accounts[2], i) > 0:    
            tx2 = mockgamblepool.claimFund(fundId, i, {'from': accounts[2]})

    logging.info('*User1:{}'.format(
        fundHiStake01.getUser(accounts[1])
    ))        
    logging.info('*User2:{}'.format(
        fundHiStake01.getUser(accounts[2])
    )) 
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch() - 1,
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch() - 1)
    ))
    logging.info('*Epoch({}):{}'.format(
        fundHiStake01.currentEpoch(),
        fundHiStake01.getRewardEpoch(fundHiStake01.currentEpoch())
    ))       
