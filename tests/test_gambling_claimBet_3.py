import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
LOGGER = logging.getLogger(__name__)
#one game,  one events,  one acc make one bet. Attemption to climeBet when event is not finished
def test_gamble_claimBet_3(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel):
	#prepare test data
	#add game
	gamblepool.addGame(binaryevents.address, 
		'Binary Events1', 
		rewardmodel.address, 
		{"from": accounts[0]})
	# create event for game
	gamblepool.createEvent(
		0, #_gameId
		chain.time() + 100 ,
		chain.time() + 66 ,
		'BTC/USD',
		[
			[1,4815987599200,0,0,0], #MoreOrEqual
			[4,4815987599200,0,0,0]  #less
		],
		{'from':accounts[0]}
	)
	eventId = binaryevents.getEventCount() - 1

	# stake
	#acc 0
	bettoken.mint(accounts[0], 100000, {"from": accounts[0]})
	bettoken.approve(gamblepool.address, 100000, {"from": accounts[0]})
	gamblepool.stake(100000, {"from": accounts[0]})

	bet_amount = 1000
	bets_amount0 = 0
	before_balance0 = gamblepool.getUserBalance(accounts[0].address)
	logging.info('before_balance0 = {}'.format(before_balance0))
	gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]}) #first outcome
	bets_amount0 += bet_amount
	
	logging.info('after_balance0 = {}'.format(gamblepool.getUserBalance(accounts[0].address)))

	with reverts("Not Finished yet"):
		gamblepool.claimBet(0, {"from": accounts[0]})

	#settle game event
	with reverts("Event is not finished yet"):
		gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})

	

	







	



