import pytest
import logging, time
from brownie import Wei, reverts, chain
from checkData import checkOutcome
LOGGER = logging.getLogger(__name__)

EXPIRE_DELAY = 100
ORDER_DEADLINE_DELAY = 66


def test_oracle(accounts, oracle):
    oracle.setPriceFeedProviderByPairName('BTC/USD', '0xECe365B379E1dD183B20fc5f022230C044d51404', 2, {"from": accounts[0]} )
    logging.info(oracle.nameHash())
    logging.info(oracle.namedFeed(oracle.nameHash()))    


