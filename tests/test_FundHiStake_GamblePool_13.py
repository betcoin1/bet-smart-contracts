import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
from createFundAmount import createFundAmount
from createFunds import createFunds
LOGGER = logging.getLogger(__name__)

rewards = []
total_percent = 0
aCount = 8
amount_mint = 100e18
bet_amount = 10e18


#8 accounts make bets
#amounts of bet are not equal
#accounts make several bets
#accounts claim fund
def test_create_Fund_Amount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	#prepare test data
	#create funds
	createFunds(accounts, gamblepool, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery)

	#add game
	gamblepool.addGame(binaryevents.address, 
		'Binary Events1', 
		rewardmodel.address, 
		{"from": accounts[0]})

	# create event for game
	gamblepool.createEvent(
		0, #_gameId
		chain.time() + 2000 ,
		chain.time() + 1066 ,
		'BTC/USD',
		[
			[1,4815987599256,0,0,0], #MoreOrEqual
			[4,4815987599256,0,0,0]  #less
		],
		{'from':accounts[0]}
	)
	eventId = binaryevents.getEventCount() - 1

	# stake
	i = 0
	while i < aCount:
		bettoken.mint(accounts[i], amount_mint, {"from": accounts[0]})
		bettoken.approve(gamblepool.address, 100e18, {"from": accounts[i]})
		gamblepool.stake(100e18, {"from": accounts[i]})
		i += 1

	bet_quantity = 13
	#make first bet for acc0 - outcome 1 - winner
	gamblepool.makeBet(0, eventId, 1, bet_amount, {"from":accounts[0]})
	gamblepool.makeBet(0, eventId, 0, bet_amount*2, {"from":accounts[1]})
	gamblepool.makeBet(0, eventId, 0, bet_amount*3, {"from":accounts[2]})
	gamblepool.makeBet(0, eventId, 1, bet_amount*3, {"from":accounts[2]})
	gamblepool.makeBet(0, eventId, 0, bet_amount*4, {"from":accounts[3]})
	gamblepool.makeBet(0, eventId, 1, bet_amount*4, {"from":accounts[4]})
	gamblepool.makeBet(0, eventId, 0, bet_amount*3, {"from":accounts[5]})
	gamblepool.makeBet(0, eventId, 1, bet_amount*2, {"from":accounts[6]})
	gamblepool.makeBet(0, eventId, 1, bet_amount, {"from":accounts[7]})
	gamblepool.makeBet(0, eventId, 0, bet_amount*5, {"from":accounts[7]})
	
	#acc 0 makes three bet
	chain.sleep(200)
	chain.mine()
	gamblepool.makeBet(0, eventId, 1, bet_amount, {"from":accounts[0]})
	chain.sleep(200)
	chain.mine()
	gamblepool.makeBet(0, eventId, 1, bet_amount, {"from":accounts[0]})
	chain.sleep(200)
	chain.mine()
	gamblepool.makeBet(0, eventId, 1, bet_amount, {"from":accounts[0]})

	#set price for pair
	price = 4815987599260
	oracle.setCustomPrice(price, {"from": accounts[0]})

	#move date to after deadline
	chain.sleep(1066)
	chain.mine()
	gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
	checkEvent(accounts, binaryevents, eventId, ['state', 'oraclePrice'], [1, 0]) #check status Hold and oraclePrice

	#move date to after expire date
	chain.sleep(1000)
	chain.mine()

	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [0]) #check oraclePrice
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	#change status
	binaryevents.checkStateWithSave(eventId, {"from": accounts[1]})

	#check outcome status
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	checkOutcome(accounts, binaryevents, eventId, 0, ['isWin'], [False])  #check outcome0 result
	checkOutcome(accounts, binaryevents, eventId, 1, ['isWin'], [True])  #check outcome1 result
	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [price]) #check oraclePrice

#close event 0
def test_settle_event_0(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	before_inBetsAmount = gamblepool.inBetsAmount()
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

	fundHiStake01.closeCurrentEpoch()

#claim fund
def test_claim_HiFund_0(accounts, binaryevents,gamblepool, fundHiStake01):
	logging.info('getAvailableReward(accounts[0], 0) = {}'.format(fundHiStake01.getAvailableReward(accounts[0], 0)))
	i = 0
	while i < aCount:
		logging.info('i= {}'.format(i))
		bba = gamblepool.getUserBalance(accounts[i])
		bbf = gamblepool.getUserBalance(fundHiStake01)
		brc = fundHiStake01.getRewardEpoch(0)[1]
		gamblepool.claimFund(2, 0, {"from": accounts[i]})
		logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(i, fundHiStake01.getUser(accounts[i])))
		logging.info('fundHiStake01.getRewardEpoch(0)= {}'.format(fundHiStake01.getRewardEpoch(0)))
		assert bba < gamblepool.getUserBalance(accounts[i])
		assert bbf > gamblepool.getUserBalance(fundHiStake01)
		assert fundHiStake01.getUser(accounts[i])[0][3] > 0
		assert fundHiStake01.getRewardEpoch(0)[1] > brc

		i += 1
	
	logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(0, fundHiStake01.getUser(accounts[0])))
	logging.info('fundHiStake01.getRewardEpoch(0)= {}'.format(fundHiStake01.getRewardEpoch(0)))
	

	#check rewardAmount and rewardClaimed
	assert round(fundHiStake01.getRewardEpoch(0)[0]/10) == round(fundHiStake01.getRewardEpoch(0)[1]/10)
	assert fundHiStake01.getUser(accounts[0])[0][3] > 0

	logging.info('getUsersBetByIndex(accounts[0], 0) = {}'.format(gamblepool.getUsersBetByIndex(accounts[0], 0)))
	logging.info('getUsersBetByIndex(accounts[0], 1) = {}'.format(gamblepool.getUsersBetByIndex(accounts[0], 1)))
	logging.info('getUsersBetByIndex(accounts[0], 2) = {}'.format(gamblepool.getUsersBetByIndex(accounts[0], 2)))
	logging.info('getUsersBetByIndex(accounts[0], 3) = {}'.format(gamblepool.getUsersBetByIndex(accounts[0], 3)))