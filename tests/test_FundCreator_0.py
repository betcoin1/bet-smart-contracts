import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 10000e18
currEpoch = 0


# two account
#stake in 0 epoch
#without other balance moves
#claim in 3 epochs

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundCreators01):
    mockgamblepool.registerFund(
        fundCreators01.address,
        100,
        False,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()
    assert  fundCount == 1

    bettoken.mint(mockgamblepool.address, amount*10, {"from": accounts[0]})
    assert bettoken.balanceOf(mockgamblepool.address) == amount*10 



def test_settle_event_1(accounts, mockgamblepool, bettoken, fundCreators01):
    logging.info('This fund balance in pool:({}):{}'.format(
        fundCreators01.address,
        mockgamblepool.getUserBalance(fundCreators01)
    ))
    mockgamblepool.settleGameEvent(currEpoch, amount*10)
    logging.info('This fund balance in pool:({}):{}'.format(
        fundCreators01.address,
        mockgamblepool.getUserBalance(fundCreators01)
    ))

    logging.info(' mockgamblepool.totalStaked():():{}'.format(
        mockgamblepool.totalStaked()
    ))
    assert mockgamblepool.getUserBalance(fundCreators01) == bettoken.balanceOf(mockgamblepool.address)


def test_claim(accounts, mockgamblepool, bettoken, fundCreators01):

    tx1 = mockgamblepool.claimFund(fundId, 888, {'from': accounts[0]})
    amount_claimed = tx1.events['UserRewardClaimed']['reward']
    assert mockgamblepool.getUserBalance(fundCreators01) == 0
    assert mockgamblepool.getUserBalance(accounts[0]) == amount_claimed
