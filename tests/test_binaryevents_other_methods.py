import pytest
import logging, time
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

def test_binaryevents_eventCount(accounts, binaryeventMock):
    binaryeventMock.createEvent(
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}
    )
    assert binaryeventMock.getEventCount() == 1

def test_binaryevents_setOracle(accounts, binaryeventMock, oracle):
    with reverts("Ownable: caller is not the owner"):
        binaryeventMock.setOracle(oracle.address, {"from": accounts[1]})