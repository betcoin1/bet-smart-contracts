import pytest
import logging, time
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0

# кейсы:
    #before expire date:
        # event status Active
    # after expire date:
        #event status Active
        #event status Cancel
        #event status Hold
        #event status Finished

def test_binaryevents_create(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = 0
    #logging.info(binaryeventMock.getEvent(0))
    #logging.info(tx.events['NewEvent'])
    assert tx.events['NewEvent']['source'] == binaryeventMock.address
    assert binaryeventMock.getEvent(0)[5][1][0] == 4;# type second outcome
    assert binaryeventMock.getEvent(0)[5][0][0] == 1;# type first outcome
    assert binaryeventMock.getEvent(0)[1] == 0;# check event status
    assert binaryeventMock.getEvent(0)[4] == 'BTC/USD';# check currency pair

def test_binaryevents_decbet_fail(accounts, binaryeventMock):
    with reverts("Only pool can call it"):
        binaryeventMock.decBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[1]}
    )
    # nonexist outcome
    with reverts("Only 2 outcomes for this event type"):
        binaryeventMock.decBetCountForOutcome(
        0,2, 10e18,
        {'from':accounts[0]}
    )
    # nonexist event
    with reverts(""):
        binaryeventMock.decBetCountForOutcome(
        3,0, 10e18,
        {'from':accounts[0]}
    )
    # there are not event bets
    with reverts(""):
        binaryeventMock.decBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[0]}
    )

def test_binaryevents_decbet_1(accounts, binaryeventMock):
    # make bet
    tx = binaryeventMock.incBetCountForOutcome(
        0,0, 20e18,
        {'from':accounts[0]}
    )
    #logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    assert binaryeventMock.getEvent(0)[5][0][3] == 1; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][0][2] == 20e18; # check bet amount
    # outcome does not have bets
    with reverts(''):
        tx = binaryeventMock.decBetCountForOutcome(
            0,1, 22e18,
            {'from':accounts[0]}
        )
    logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    # outcome has bets, but amount of bets less than amount of calling
    with reverts(''):
        tx = binaryeventMock.decBetCountForOutcome(
            0,0, 22e18,
            {'from':accounts[0]}
        )

    logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    #  partly return bet
    bets_before = binaryeventMock.getEvent(0)[5][0][3]
    amount_before = binaryeventMock.getEvent(0)[5][0][2]
    tx = binaryeventMock.decBetCountForOutcome(
        0,0, 9e18,
        {'from':accounts[0]}
    )
    #logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    #logging.info(binaryeventMock.getEvent(0))
    assert binaryeventMock.getEvent(0)[5][0][3] == bets_before - 1;
    assert binaryeventMock.getEvent(0)[5][0][2] == amount_before - 9e18;

    #logging.info('Time machine running.....+1 hour...................................')
    chain.sleep(3600)
    chain.mine()

    # expire date is now
    # change event status
    bets_before = binaryeventMock.getEvent(0)[5][0][3]
    amount_before = binaryeventMock.getEvent(0)[5][0][2]
    tx = binaryeventMock.decBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[0]}
    )
    #logging.info('tx = {}'.format(tx))
    #logging.info('binaryeventMock.getEvent(0) = {}'.format(binaryeventMock.getEvent(0)))
    # event is cancelled - bet quantity  is 0
    assert binaryeventMock.getEvent(0)[1] == 3 #check status Cancelled
    assert binaryeventMock.getEvent(0)[5][0][3] == bets_before; # check bet quantity  
    assert binaryeventMock.getEvent(0)[5][0][2] == amount_before;# check bet amount  
    assert binaryeventMock.getEvent(0)[5][1][3] == 0; # check bet quantity  
    assert binaryeventMock.getEvent(0)[5][1][2] == 0; # # check bet amount  
    assert binaryeventMock.getEvent(0)[6] == 0; # check price

    # again call method decBetCountForOutcome when event status is Cancelled - nothing has to change

    tx = binaryeventMock.decBetCountForOutcome(
        0,0, 10e18,
        {'from':accounts[0]}
    )
   
    assert binaryeventMock.getEvent(0)[1] == 3 #check status Cancelled
    assert binaryeventMock.getEvent(0)[5][0][3] == bets_before; #  check bet quantity  
    assert binaryeventMock.getEvent(0)[5][0][2] == amount_before; # check bet amount  
    assert binaryeventMock.getEvent(0)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(0)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(0)[6] == 0; # check price

# check next case 
def test_binaryevents_decbet_2(accounts, binaryeventMock):
    tx  = binaryeventMock.createEvent(
        chain.time() + 100 ,
        chain.time() + 66 ,
        'BTC/USD',
        [
            [1,4815987599255,0,0,0], #MoreOrEqual
            [4,4815987599255,0,0,0]  #less
        ],
        accounts[0],
        {'from':accounts[0]}

    )
    eventId = 1
    # make bet
    tx = binaryeventMock.incBetCountForOutcome(
        eventId,0, 20e18,
        {'from':accounts[0]}
    )

    #logging.info('Time machine running.....+1 hour...................................')
    chain.sleep(3600)
    chain.mine()

    #cancel bet
    bets_before = binaryeventMock.getEvent(eventId)[5][0][3]
    amount_before = binaryeventMock.getEvent(eventId)[5][0][2]
    tx = binaryeventMock.decBetCountForOutcome(
        eventId,0, 10e18,
        {'from':accounts[0]}
    )
    #status Finished
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check status Finished
    assert binaryeventMock.getEvent(eventId)[5][0][3] == bets_before; # check bet quantity  
    assert binaryeventMock.getEvent(eventId)[5][0][2] == amount_before; # check bet amount  
    assert binaryeventMock.getEvent(eventId)[5][1][3] == 0; # check bet quantity 
    assert binaryeventMock.getEvent(eventId)[5][1][2] == 0; # check bet amount
    assert binaryeventMock.getEvent(eventId)[6] > 0; # check price

    #try to change event status
    with reverts("Event state must be Active (0) or Hold (1)"):
        binaryeventMock.checkStateWithSave(eventId)
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check event status
    assert binaryeventMock.getEvent(eventId)[6] > 0; # check price
    # try to cancel bet 
    bets_before = binaryeventMock.getEvent(eventId)[5][0][3]
    amount_before = binaryeventMock.getEvent(eventId)[5][0][2]
    logging.info('before binaryeventMock.getEvent(eventId) = {}'.format(binaryeventMock.getEvent(eventId)))
    with reverts("Event is Finished"):
        binaryeventMock.decBetCountForOutcome(
            eventId,0, 10e18,
            {'from':accounts[0]}
        )
    logging.info('after binaryeventMock.getEvent(eventId) = {}'.format(binaryeventMock.getEvent(eventId)))
    assert binaryeventMock.getEvent(eventId)[1] == 2 #check status Finish
    assert binaryeventMock.getEvent(eventId)[5][0][3] == bets_before; # check bet quantity  
    assert binaryeventMock.getEvent(eventId)[5][0][2] == amount_before; # check bet amount 
    assert binaryeventMock.getEvent(eventId)[5][1][3] == 0; # check bet quantity
    assert binaryeventMock.getEvent(eventId)[5][1][2] == 0; # check bet amount 
    assert binaryeventMock.getEvent(eventId)[6] > 0; # check price

