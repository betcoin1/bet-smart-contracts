import pytest
import logging
from brownie import Wei, reverts
LOGGER = logging.getLogger(__name__)


def test_betERC20_burn_minter_fail(accounts, bettoken):
    with reverts("MinterRole: caller does not have the Minter role"):
        bettoken.burn(1, {"from":accounts[1]})
    assert bettoken.balanceOf(accounts[0]) == 0

def test_betERC20_burn_amount_failed(accounts, bettoken):
    with reverts("ERC20: burn amount exceeds balance"):
        bettoken.burn(bettoken.MAX_SUPPLY() , {"from":accounts[0]})
    assert bettoken.balanceOf(accounts[0]) == 0

def test_betERC20_burn_success(accounts, bettoken):
    bettoken.mint(accounts[0], 1, {"from":accounts[0]})
    logging.info('acc = {}'.format(bettoken.balanceOf(accounts[0])))
    bettoken.burn(1, {"from":accounts[0]})
    assert bettoken.balanceOf(accounts[0]) == 0
    assert bettoken.totalSupply() == 0