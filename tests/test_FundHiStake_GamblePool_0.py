import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
from createFundAmount import createFundAmount
from createFunds import createFunds
LOGGER = logging.getLogger(__name__)

rewards = []
total_percent = 0
acc_count = 8

#one time make events, all gambler's claim fund

#one game,  one events, a lot of acc make bets on different outcomes. Outcome1 wins, same acc get prize for each bet of outcome1
def test_prepare_data_for_event_1(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	global total_percent
	global acc_count
	amount_mint = 100e18
	bet_amount = 10e18
	createFunds(accounts, gamblepool, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery)
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[0], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

def test_settle_event_1(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	before_inBetsAmount = gamblepool.inBetsAmount()
	rewards.append(before_inBetsAmount)
	before_fundBalance = gamblepool.fundBalance()
	gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))
	assert gamblepool.inBetsAmount() == 0
	assert gamblepool.fundBalance() == before_inBetsAmount
	assert gamblepool.eventsSettlement(0, eventId)[0] == before_inBetsAmount

	fundHiStake01.closeCurrentEpoch()


def test_claim_HiFund(accounts, binaryevents,gamblepool, fundHiStake01):
	global acc_count
	i = 0
	while i < acc_count:
		bba = gamblepool.getUserBalance(accounts[i])
		bbf = gamblepool.getUserBalance(fundHiStake01)
		brc = fundHiStake01.getRewardEpoch(0)[1]
		gamblepool.claimFund(2, 0, {"from": accounts[i]})
		logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(i, fundHiStake01.getUser(accounts[i])))
		logging.info('fundHiStake01.getRewardEpoch(0)= {}'.format(fundHiStake01.getRewardEpoch(0)))
		assert bba < gamblepool.getUserBalance(accounts[i])
		assert bbf > gamblepool.getUserBalance(fundHiStake01)
		assert fundHiStake01.getUser(accounts[i])[0][3] > 0
		assert fundHiStake01.getRewardEpoch(0)[1] > brc

		i += 1