import pytest
import logging, time
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

def test_binaryevents_create_fail(accounts, binaryeventMock):
    with reverts('Only pool can call it'):
        binaryeventMock.createEvent(
            chain.time() + 100 ,
            chain.time() + 66 ,
            'BTC/USD',
            [
                [1,4815987599255,0,0,0], #MoreOrEqual
                [4,4815987599255,0,0,0]  #less
            ],
            accounts[0],
            {'from':accounts[1]}

        )
    '''try:
        binaryeventMock.createEvent(
            chain.time() + 100 ,
            chain.time() + 66 ,
            'BTC/USD',
            [
                [1,4815987599255,0,0] #MoreOrEqual
               
            ],
            {'from':accounts[0]}

        )
    except ValueError:
        print("Sequence has incorrect length, expected 2 but got 1")
    with reverts(''):
        binaryeventMock.createEvent(
            chain.time() + 100 ,
            chain.time() + 66 ,
            'BTC/USD',
            [
                [1,4815987599255,0], #MoreOrEqual
                [4,4815987599255,0]  #less
            ],
            {'from':accounts[0]}

        )
    with reverts(''):
        binaryeventMock.createEvent(
            chain.time() + 100 ,
            chain.time() + 66 ,
            'BTC/USD',
            [
                [10,4815987599255,0,0], #MoreOrEqual
                [4,4815987599255,0,0]  #less
            ],
            {'from':accounts[0]}

        )
    with reverts('r'):
        binaryeventMock.createEvent(
            chain.time() + 100 ,
            chain.time() + 66 ,
            'BTC/USD',
            [
                [1,4815987599255,0,0], #MoreOrEqual
                [4,4815987599255,0,0]  #less
                [3,4815987599255,0,0]  #less
            ],
            {'from':accounts[0]}

        )'''