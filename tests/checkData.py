import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

def checkBet(accounts, gamblepool, user, betId, fields, values):
	#logging.info('betId = {}'.format(betId))
	#logging.info('getUsersBetByIndex(user, betId) = {}'.format(gamblepool.getUsersBetByIndex(user, betId)))
	for i in range(len(fields)):
		#logging.info('i = {}'.format(i))
		#logging.info('fields = {}'.format(fields[i]))
		#logging.info('values = {}'.format(values[i]))
		if fields[i] ==  'eventContract':
			assert gamblepool.getUsersBetByIndex(user, betId)[0] == values[i]
		elif fields[i] ==  'eventId':
			assert gamblepool.getUsersBetByIndex(user, betId)[1] == values[i]
		elif fields[i] ==  'eventOutcomeIndex':
			assert gamblepool.getUsersBetByIndex(user, betId)[2] == values[i]
		elif fields[i] ==  'betAmount':
			assert gamblepool.getUsersBetByIndex(user, betId)[3] == values[i]
		elif fields[i] ==  'betTimestamp':
			assert gamblepool.getUsersBetByIndex(user, betId)[4] == values[i]
		elif fields[i] ==  'currentState':
			assert gamblepool.getUsersBetByIndex(user, betId)[5] == values[i]
		else: #result
			assert gamblepool.getUsersBetByIndex(user, betId)[6] == values[i]

def checkOutcome(accounts, binaryevents, eventId, outcomeId, fields, values):
	for i in range(len(fields)):
		if fields[i] ==  'compare':
			assert binaryevents.getEvent(eventId)[5][outcomeId][0] == values[i]
		elif fields[i] ==  'expectedValue':
			assert binaryevents.getEvent(eventId)[5][outcomeId][1] == values[i]
		elif fields[i] ==  'weiRaised':
			assert binaryevents.getEvent(eventId)[5][outcomeId][2] == values[i]
		elif fields[i] ==  'betCount':
			assert binaryevents.getEvent(eventId)[5][outcomeId][3] == values[i]
		else: #isWin
			assert binaryevents.getEvent(eventId)[5][outcomeId][4] == values[i]

def checkEvent(accounts, binaryevents, eventId, fields, values):
	for i in range(len(fields)):
		if fields[i] ==  'creator':
			assert binaryevents.getEvent(eventId)[0] == values[i]
		elif fields[i] ==  'state':
			assert binaryevents.getEvent(eventId)[1] == values[i]
		elif fields[i] ==  'expirationTime':
			assert binaryevents.getEvent(eventId)[2] == values[i]
		elif fields[i] ==  'orderDeadLineTime':
			assert binaryevents.getEvent(eventId)[3] == values[i]
		elif fields[i] ==  'eventAssetSymbol':
			assert binaryevents.getEvent(eventId)[4] == values[i]
		elif fields[i] ==  'outcome':
			assert binaryevents.getEvent(eventId)[5] == values[i]
		else: #oraclePrice
			assert binaryevents.getEvent(eventId)[6] == values[i]
