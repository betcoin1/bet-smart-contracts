import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
from createFundAmount import createFundAmount
from createFunds import createFunds
LOGGER = logging.getLogger(__name__)

rewards = []
total_percent = 0
acc_count = 8

#several error cases

#one game,  one events, a lot of acc make bets on different outcomes. Outcome1 wins, same acc get prize for each bet of outcome1
def test_prepare_data_for_event_0(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery):
	global total_percent
	global acc_count
	amount_mint = 100e18
	bet_amount = 10e18
	createFunds(accounts, gamblepool, fundHolders01, fundHiStake01, fundBurn01, fundFee, fundCreators01, fundLottery)
	createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, accounts[0], acc_count, amount_mint, bet_amount)
	logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
	logging.info('gamblepool.fundBalance() = {}'.format(gamblepool.fundBalance()))

	with reverts("Cant claim from currentEpoch"):
		gamblepool.claimFund(2, 0, {"from": accounts[0]})

#close event 0
def test_settle_event_0(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, fundHolders01,  fundHiStake01, fundBurn01, fundFee, fundCreators01):
	#settle game event
	eventId = binaryevents.getEventCount() - 1
	fundHiStake01.closeCurrentEpoch()
	gamblepool.claimFund(2, 0, {"from": accounts[8]})

	logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(8, fundHiStake01.getUser(accounts[8])))
	assert len(fundHiStake01.getUser(accounts[8])) == 0

#claim fund, acc7 claim in epoch 3
def test_claim_HiFund_3(accounts, binaryevents,gamblepool, fundHiStake01):
	logging.info('getAvailableReward(accounts[0], 0) = {}'.format(fundHiStake01.getAvailableReward(accounts[0], 0)))
	'''logging.info('getAvailableReward(accounts[7], 1) = {}'.format(fundHiStake01.getAvailableReward(accounts[7], 1)))
	logging.info('getAvailableReward(accounts[7], 2) = {}'.format(fundHiStake01.getAvailableReward(accounts[7], 2)))
	logging.info('getAvailableReward(accounts[7], 2) = {}'.format(fundHiStake01.getAvailableReward(accounts[7], 3)))
	global acc_count
	i = 0
	while i < acc_count:
		bba = gamblepool.getUserBalance(accounts[i])
		bbf = gamblepool.getUserBalance(fundHiStake01)
		brc = fundHiStake01.getRewardEpoch(3)[1]
		gamblepool.claimFund(2, 3, {"from": accounts[i]})
		logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(i, fundHiStake01.getUser(accounts[i])))
		logging.info('fundHiStake01.getRewardEpoch(2)= {}'.format(fundHiStake01.getRewardEpoch(2)))
		assert bba < gamblepool.getUserBalance(accounts[i])
		assert bbf > gamblepool.getUserBalance(fundHiStake01)
		assert fundHiStake01.getUser(accounts[i])[3][3] > 0
		assert fundHiStake01.getRewardEpoch(3)[1] > brc

		i += 1
	#acc 7 claims skiped epoch
	gamblepool.claimFund(2, 0, {"from": accounts[7]})
	gamblepool.claimFund(2, 1, {"from": accounts[7]})
	gamblepool.claimFund(2, 2, {"from": accounts[7]})
	gamblepool.claimFund(2, 3, {"from": accounts[7]})

	logging.info('fundHiStake01.getUser(accounts[{}])= {}'.format(7, fundHiStake01.getUser(accounts[7])))
	logging.info('fundHiStake01.getRewardEpoch(0)= {}'.format(fundHiStake01.getRewardEpoch(0)))
	logging.info('fundHiStake01.getRewardEpoch(1)= {}'.format(fundHiStake01.getRewardEpoch(1)))
	logging.info('fundHiStake01.getRewardEpoch(2)= {}'.format(fundHiStake01.getRewardEpoch(2)))
	logging.info('fundHiStake01.getRewardEpoch(2)= {}'.format(fundHiStake01.getRewardEpoch(3)))
	
	#check claimed amount in user epoch
	assert fundHiStake01.getUser(accounts[7])[0][3] >0
	assert fundHiStake01.getUser(accounts[7])[1][3] >0

	#check rewardAmount and rewardClaimed
	assert fundHiStake01.getRewardEpoch(0)[0] == fundHiStake01.getRewardEpoch(0)[1]
	assert round(fundHiStake01.getRewardEpoch(1)[0]/10) == round(fundHiStake01.getRewardEpoch(1)[1]/10)
	assert fundHiStake01.getRewardEpoch(2)[0] == fundHiStake01.getRewardEpoch(2)[1]
	assert round(fundHiStake01.getRewardEpoch(3)[0]/10) == round(fundHiStake01.getRewardEpoch(3)[1]/10)'''