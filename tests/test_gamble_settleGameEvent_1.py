import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
LOGGER = logging.getLogger(__name__)
#not owner tries settle game event
#event is cancelled. Owner tries to settle game event
def test_gamble_settleGameEvent_1(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel):
	#prepare test data
	#add game
	gamblepool.addGame(binaryevents.address, 
		'Binary Events', 
		rewardmodel.address, 
		{"from": accounts[0]})
	# create event for game
	gamblepool.createEvent(
		0, #_gameId
		chain.time() + 100 ,
		chain.time() + 66 ,
		'BTC/USD',
		[
			[1,4815987599256,0,0,0], #MoreOrEqual
			[4,4815987599256,0,0,0]  #less
		],
		{'from':accounts[0]}
	)
	eventId = binaryevents.getEventCount() - 1

	#set price for pair
	price = 4815987599260
	oracle.setCustomPrice(price, {"from": accounts[0]})

	#move date to after deadline
	chain.sleep(3600)
	
	binaryevents.checkStateWithSave(eventId, {"from": accounts[1]})

	#check outcome status
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	checkOutcome(accounts, binaryevents, eventId, 0, ['isWin'], [False])  #check outcome0 result
	checkOutcome(accounts, binaryevents, eventId, 1, ['isWin'], [True])  #check outcome1 result
	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [price]) #check oraclePrice

	#not owner tries settle game event
	with reverts("Ownable: caller is not the owner"):
		gamblepool.settleGameEvent(0, eventId, {"from": accounts[1]})

	#event is cancelled
	with reverts("Event is not finished yet"):
		gamblepool.settleGameEvent(0, eventId, {"from": accounts[0]})



	




	



