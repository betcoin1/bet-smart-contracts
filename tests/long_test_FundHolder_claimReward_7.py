import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1e18
currEpoch = 0

# 100 accounts
# stake different amount of tokens, different time between staking
#claim epoch 0
def test_gamble_stake(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    #add accounts
    i = 0
    while i < 91:
        accounts.add()
        logging.info('i add= {}'.format(i))
        i += 1

    i = 1
    while i < 101:
        logging.info('i mint= {}'.format(i))
        bettoken.mint(accounts[i], 100*amount, {"from": accounts[0]})        
        bettoken.approve(mockgamblepool.address, 100*amount, {"from": accounts[i]})
        i += 1

    i = 0
    r = 1
    while i < 10:
        j = 1
        logging.info('i stake= {}'.format(i))
        while j < 11:
            if i*10+j < 51:
                mockgamblepool.stake((i*10+j)*amount, {"from": accounts[i*10+j]})
                chain.mine((i*10+j)*10)
            else:
                mockgamblepool.stake((i*10-r)*amount, {"from": accounts[i*10+j]})
                chain.mine((i*10 - r)*10)
                r += 1
            logging.info('j = {}'.format(j))
            j += 1
        i += 1


def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.settleGameEvent(currEpoch, 10e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 2

def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):
    i = 1
    while i < 101:
        mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[i]})
        assert fundHolders01.getUser(accounts[i])[0][3] > 0
        logging.info('userReward epoch 0 account[{}] = {}'.format(i, fundHolders01.getUser(accounts[i])[0]))
        i += 1

    logging.info('after_RewardEpoch_= {}'.format(fundHolders01.getRewardEpoch(0)))
    