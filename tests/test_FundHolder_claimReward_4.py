import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1000
currEpoch = 0

# three account
#stake in 0 epoch
#two accounts move balance before end of epoch
#claim after  end of epoch (in next epoch)
#between staking 50 blocks
# first claiming - acc2, acc3, second claim - acc1

def test_gamble(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[1], 80*amount, {"from": accounts[0]})
    bettoken.mint(accounts[2], 10*amount, {"from": accounts[0]})
    bettoken.mint(accounts[3], 10*amount, {"from": accounts[0]})

    #stake tokens
    bettoken.approve(mockgamblepool.address, 80*amount, {"from": accounts[1]})
    bettoken.approve(mockgamblepool.address, 10*amount, {"from": accounts[2]})
    bettoken.approve(mockgamblepool.address, 10*amount, {"from": accounts[3]})
    logging.info('*****************stake acc************')
    logging.info('currEpoch = {}'.format(currEpoch))
    mockgamblepool.stake(80*amount, {"from": accounts[1]})
    chain.mine(50)

    mockgamblepool.stake(10*amount, {"from": accounts[2]})
    chain.mine(50)

    mockgamblepool.stake(10*amount, {"from": accounts[3]})
    chain.mine(50)

    #change balances acc2 and acc3
    mockgamblepool.withdraw(5*amount, {"from": accounts[2]})
    mockgamblepool.withdraw(5*amount, {"from": accounts[3]})


def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 2

def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('////////////////////////CLAIM EPOCH 0/////////////////////////')
    logging.info('currEpoch = {}'.format(fundHolders01.currentEpoch()))
    before_userRewards_1 = fundHolders01.getUser(accounts[1])
    before_userRewards_2 = fundHolders01.getUser(accounts[2])
    before_userRewards_3 = fundHolders01.getUser(accounts[3])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    bba1 = mockgamblepool.getUserBalance(accounts[1])
    bba2 = mockgamblepool.getUserBalance(accounts[2])
    bba3 = mockgamblepool.getUserBalance(accounts[3])
    logging.info('before_userRewards_1 = {}'.format(before_userRewards_1))
    logging.info('before_userRewards_2 = {}'.format(before_userRewards_2))
    logging.info('before_userRewards_3 = {}'.format(before_userRewards_3))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))
    logging.info('mockgamblepool.getUserBalance(accounts[3]) = {}'.format(mockgamblepool.getUserBalance(accounts[3])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_0!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[2]})
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[3]})
    mockgamblepool.claimFund(fundId, currEpoch, {'from': accounts[1]})
    after_userRewards_1 = fundHolders01.getUser(accounts[1])
    after_userRewards_2 = fundHolders01.getUser(accounts[2])
    after_userRewards_3 = fundHolders01.getUser(accounts[3])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    logging.info('after_userRewards_1 = {}'.format(after_userRewards_1))
    logging.info('after_userRewards_2 = {}'.format(after_userRewards_2))
    logging.info('after_userRewards_3 = {}'.format(after_userRewards_3))
    logging.info('after_RewardEpoch_0 = {}'.format(after_RewardEpoch))
    logging.info('after_RewardEpoch_{} = {}'.format(fundHolders01.currentEpoch(),fundHolders01.getRewardEpoch(fundHolders01.currentEpoch())))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))
    logging.info('mockgamblepool.getUserBalance(accounts[3]) = {}'.format(mockgamblepool.getUserBalance(accounts[3])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    assert after_userRewards_1[0][3] + after_userRewards_2[0][3] + after_userRewards_3[0][3] == after_RewardEpoch[1]
    assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userRewards_1[0][3]
    assert mockgamblepool.getUserBalance(accounts[2]) == bba2 + after_userRewards_2[0][3]
    assert mockgamblepool.getUserBalance(accounts[3]) == bba3 + after_userRewards_3[0][3]
    assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
    assert after_userRewards_1[0][3] > 0
    assert after_userRewards_2[0][3] > 0
    assert after_userRewards_3[0][3] > 0
    assert bba1 < mockgamblepool.getUserBalance(accounts[1])
    assert bba2 < mockgamblepool.getUserBalance(accounts[2])
    assert bba3 < mockgamblepool.getUserBalance(accounts[3])


def test_settle_event_2(accounts, mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.settleGameEvent(currEpoch + 1, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 3

def test_fundHolders_claimReward_2(accounts, mockgamblepool, bettoken, fundHolders01):

    logging.info('////////////////////////CLAIM EPOCH 1/////////////////////////')
    logging.info('currEpoch = {}'.format(currEpoch + 1))
    before_userRewards_1 = fundHolders01.getUser(accounts[1])
    before_userRewards_2 = fundHolders01.getUser(accounts[2])
    before_userRewards_3 = fundHolders01.getUser(accounts[3])
    before_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch)
    bba1 = mockgamblepool.getUserBalance(accounts[1])
    bba2 = mockgamblepool.getUserBalance(accounts[2])
    bba3 = mockgamblepool.getUserBalance(accounts[3])
    logging.info('before_userRewards_1 = {}'.format(before_userRewards_1[1]))
    logging.info('before_userRewards_2 = {}'.format(before_userRewards_2[1]))
    logging.info('before_userRewards_3 = {}'.format(before_userRewards_3[1]))
    logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))
    logging.info('mockgamblepool.getUserBalance(accounts[3]) = {}'.format(mockgamblepool.getUserBalance(accounts[3])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

    logging.info('***********************AFTER CLAIM_1!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
    mockgamblepool.claimFund(fundId, currEpoch + 1, {'from': accounts[1]})
    mockgamblepool.claimFund(fundId, currEpoch + 1, {'from': accounts[2]})
    mockgamblepool.claimFund(fundId, currEpoch + 1, {'from': accounts[3]})
    after_userRewards_1 = fundHolders01.getUser(accounts[1])
    after_userRewards_2 = fundHolders01.getUser(accounts[2])
    after_userRewards_3 = fundHolders01.getUser(accounts[3])
    after_RewardEpoch = fundHolders01.getRewardEpoch(currEpoch + 1)
    logging.info('after_userRewards_1 = {}'.format(after_userRewards_1))
    logging.info('after_userRewards_2 = {}'.format(after_userRewards_2))
    logging.info('after_userRewards_3 = {}'.format(after_userRewards_3))
    logging.info('after_RewardEpoch_0 = {}'.format(after_RewardEpoch))
    logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
    logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))
    logging.info('mockgamblepool.getUserBalance(accounts[3]) = {}'.format(mockgamblepool.getUserBalance(accounts[3])))
    logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

    assert after_userRewards_1[1][3] + after_userRewards_2[1][3] + after_userRewards_3[1][3] == after_RewardEpoch[1]
    assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userRewards_1[1][3]
    assert mockgamblepool.getUserBalance(accounts[2]) == bba2 + after_userRewards_2[1][3]
    assert mockgamblepool.getUserBalance(accounts[3]) == bba3 + after_userRewards_3[1][3]
    assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
    assert after_userRewards_1[1][3] > 0
    assert after_userRewards_2[1][3] > 0
    assert after_userRewards_3[1][3] > 0
    assert bba1 < mockgamblepool.getUserBalance(accounts[1])
    assert bba2 < mockgamblepool.getUserBalance(accounts[2])
    assert bba3 < mockgamblepool.getUserBalance(accounts[3])

