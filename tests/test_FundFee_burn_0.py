import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 10000e18
currEpoch = 0


# two account
#stake in 0 epoch
#without other balance moves
#claim in 3 epochs

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundFee):
    mockgamblepool.registerFund(
        fundFee.address,
        100,
        False,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()
    assert  fundCount == 1

    bettoken.mint(mockgamblepool.address, amount*10, {"from": accounts[0]})
    assert bettoken.balanceOf(mockgamblepool.address) == amount*10 



def test_settle_event_1(accounts, mockgamblepool, bettoken, fundFee):
    logging.info('This fund balance in pool:({}):{}'.format(
        fundFee.address,
        mockgamblepool.getUserBalance(fundFee)
    ))
    mockgamblepool.settleGameEvent(currEpoch, amount*10)
    logging.info('This fund balance in pool:({}):{}'.format(
        fundFee.address,
        mockgamblepool.getUserBalance(fundFee)
    ))

    logging.info(' mockgamblepool.totalStaked():():{}'.format(
        mockgamblepool.totalStaked()
    ))
    assert mockgamblepool.getUserBalance(fundFee) == bettoken.balanceOf(mockgamblepool.address)


def test_burn(accounts, mockgamblepool, bettoken, fundFee):

    tx =fundFee.withdrawFee(amount)
    amount_withdrown = tx.events['Withdrawn']['amount']
    assert mockgamblepool.getUserBalance(fundFee) == amount*10 - amount_withdrown
    assert bettoken.balanceOf(mockgamblepool.address) == amount*10 - amount_withdrown
    assert bettoken.balanceOf(accounts[0]) == amount_withdrown
