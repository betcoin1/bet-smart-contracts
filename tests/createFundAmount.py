import pytest
import logging
from brownie import Wei, reverts, chain
from checkData import checkBet, checkOutcome, checkEvent
LOGGER = logging.getLogger(__name__)

#one game,  one events, a lot of acc make bets on different outcomes. Outcome1 wins, same acc get prize for each bet of outcome1

def createFundAmount(accounts, binaryevents,gamblepool, bettoken, oracle, rewardmodel, creator, aCount, amount_mint, bet_amount):
	#prepare test data
	#add game
	gamblepool.addGame(binaryevents.address, 
		'Binary Events1', 
		rewardmodel.address, 
		{"from": accounts[0]})

	# create event for game
	gamblepool.createEvent(
		0, #_gameId
		chain.time() + 100 ,
		chain.time() + 66 ,
		'BTC/USD',
		[
			[1,4815987599256,0,0,0], #MoreOrEqual
			[4,4815987599256,0,0,0]  #less
		],
		{'from':creator}
	)
	eventId = binaryevents.getEventCount() - 1

	# stake
	i = 0
	while i < aCount:
		bettoken.mint(accounts[i], amount_mint, {"from": accounts[0]})
		bettoken.approve(gamblepool.address, 100e18, {"from": accounts[i]})
		gamblepool.stake(100e18, {"from": accounts[i]})
		i += 1

	i = 0
	bets_amount = []
	before_balance = []
	bet_quantity = 0
	#logging.info('before_balance0 = {}'.format(before_balance0))
	while i < aCount:
		before_balance.append(gamblepool.getUserBalance(accounts[i].address))
		gamblepool.makeBet(0, eventId, i%2, bet_amount, {"from":accounts[i]})
		bets_amount.append(bet_amount)
		bet_quantity += 1
		i +=1
	
	#set price for pair
	price = 4815987599260
	oracle.setCustomPrice(price, {"from": accounts[0]})

	#move date to after deadline
	chain.sleep(67)
	chain.mine()
	gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
	checkEvent(accounts, binaryevents, eventId, ['state', 'oraclePrice'], [1, 0]) #check status Hold and oraclePrice

	#move date to after expire date
	chain.sleep(101)
	chain.mine()

	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [0]) #check oraclePrice
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	#change status
	binaryevents.checkStateWithSave(eventId, {"from": accounts[1]})

	#check outcome status
	logging.info('event = {}'.format(binaryevents.getEvent(eventId)))
	checkOutcome(accounts, binaryevents, eventId, 0, ['isWin'], [False])  #check outcome0 result
	checkOutcome(accounts, binaryevents, eventId, 1, ['isWin'], [True])  #check outcome1 result
	checkEvent(accounts, binaryevents, eventId, ['oraclePrice'], [price]) #check oraclePrice
	
	#call climBet
	i = 0
	before_balance = []
	rewards = 0
	while i < bet_quantity:
		before_balance.append(gamblepool.getUserBalance(accounts[i].address))
		logging.info('gamblepool.inBetsAmount() = {}'.format(gamblepool.inBetsAmount()))
		all_bets_amount = gamblepool.inBetsAmount()
		tx = gamblepool.claimBet(gamblepool.getUsersBetsCount(accounts[i].address) - 1, {"from": accounts[i]}) 
		checkBet(accounts, gamblepool, accounts[i].address, 0, ['currentState', 'result'], [2, 2-(i%2)])
		if i%2 == 0:
			assert gamblepool.getUserBalance(accounts[i].address) == before_balance[i] #check balance acc i
			assert gamblepool.inBetsAmount() == all_bets_amount #check all collected coins
		else:
			prize = tx.events['WinClaimed']['amount']
			logging.info('prize = {}'.format(prize))
			assert gamblepool.getUserBalance(accounts[i].address) > before_balance[i] + bet_amount + (binaryevents.WIN_DISTRIB_PERCENT() - 1) * bet_amount / 100  #check balance acc i - winner
			assert gamblepool.inBetsAmount() < all_bets_amount - bet_amount - (binaryevents.WIN_DISTRIB_PERCENT() - 1) * bet_amount / 100 #check all collected coins
			rewards += rewardmodel.getFundPercent()*bets_amount[i] /100
		i += 1







	



