import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'
fundId = 0 
amount = 1e18
currEpoch = 0

# three account
#stake in 0 epoch
#account0 changes balance in 3 epoch
#all accounts claim in epoch 6 epoch - all epochs

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True,
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[1], 20*amount, {"from": accounts[0]})
    bettoken.mint(accounts[2], 10*amount, {"from": accounts[0]})
    bettoken.mint(accounts[3], 10*amount, {"from": accounts[0]})

    #stake tokens
    bettoken.approve(mockgamblepool.address, 80*amount, {"from": accounts[1]})
    bettoken.approve(mockgamblepool.address, 10*amount, {"from": accounts[2]})
    bettoken.approve(mockgamblepool.address, 10*amount, {"from": accounts[3]})
    logging.info('*****************stake acc************')
    logging.info('currEpoch = {}'.format(currEpoch))
    mockgamblepool.stake(10*amount, {"from": accounts[1]})
    chain.mine(50)

    mockgamblepool.stake(10*amount, {"from": accounts[2]})
    chain.mine(50)

    mockgamblepool.stake(10*amount, {"from": accounts[3]})
    chain.mine(50)

def test_settle_event_1(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    logging.info('*****************settle epoch 0************')
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 2

def test_settle_event_2(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    logging.info('*****************settle epoch 1************')
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 3

def test_settle_event_3(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    logging.info('*****************settle epoch 2************')
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 4

def test_settle_event_4(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    logging.info('*****************stake acc1 again************')
    logging.info('currEpoch = {}'.format(fundHolders01.currentEpoch()))
    mockgamblepool.stake(10*amount, {"from": accounts[1]})
    logging.info('*****************settle epoch 3************')
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 5

def test_settle_event_5(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    logging.info('*****************settle epoch 4************')
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 6

def test_settle_event_6(accounts, mockgamblepool, bettoken, fundHolders01):
    chain.mine(50)
    logging.info('*****************settle epoch 5************')
    mockgamblepool.settleGameEvent(currEpoch, 1e18)
    logging.info('fundHolders01.currentEpoch()!!!!!! = {}'.format(fundHolders01.currentEpoch()))
    fundHolders01.closeCurrentEpoch()
    assert fundHolders01.getRewardEpochCount() == currEpoch + 7

def test_fundHolders_claimReward_1(accounts, mockgamblepool, bettoken, fundHolders01):

    i = 0
    while i < 6:
        bba1 = mockgamblepool.getUserBalance(accounts[1])
        bba2 = mockgamblepool.getUserBalance(accounts[2])
        bba3 = mockgamblepool.getUserBalance(accounts[3])
        mockgamblepool.claimFund(fundId, i, {'from': accounts[1]})
        mockgamblepool.claimFund(fundId, i, {'from': accounts[2]})
        mockgamblepool.claimFund(fundId, i, {'from': accounts[3]})
        after_userRewards_1 = fundHolders01.getUser(accounts[1])
        after_userRewards_2 = fundHolders01.getUser(accounts[2])
        after_userRewards_3 = fundHolders01.getUser(accounts[3])
        after_RewardEpoch = fundHolders01.getRewardEpoch(i)
        assert after_userRewards_1[i][3] + after_userRewards_2[i][3] + after_userRewards_3[i][3] == after_RewardEpoch[1]
        assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userRewards_1[i][3]
        assert mockgamblepool.getUserBalance(accounts[2]) == bba2 + after_userRewards_2[i][3]
        assert mockgamblepool.getUserBalance(accounts[3]) == bba3 + after_userRewards_3[i][3]
        assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
        assert after_userRewards_1[i][3] > 0
        assert after_userRewards_2[i][3] > 0
        assert after_userRewards_3[i][3] > 0
        assert bba1 < mockgamblepool.getUserBalance(accounts[1])
        assert bba2 < mockgamblepool.getUserBalance(accounts[2])
        assert bba3 < mockgamblepool.getUserBalance(accounts[3])

        i += 1

    j = 1
    while j < 4:
        logging.info('////////////////////////ACC {} REWARD EPOCHS//////////////////////////'.format(j))
        i = 0
        while i < 6:
            after_userRewards = fundHolders01.getUser(accounts[j])[i]
            logging.info('EPOCH {} = {}'.format(i, after_userRewards))

            i += 1
        j += 1

    i = 0
    logging.info('////////////////////////REWARD EPOCHS//////////////////////////')
    while i < 6:
        after_RewardEpoch = fundHolders01.getRewardEpoch(i)
        logging.info('after_RewardEpoch {}= {}'.format(i, after_RewardEpoch))
        
        i += 1
'''
    i = 0
    while i < 6:
        logging.info('////////////////////////CLAIM EPOCH {}/////////////////////////'.format(i))
        before_userRewards_1 = fundHolders01.getUser(accounts[1])
        before_userRewards_2 = fundHolders01.getUser(accounts[2])
        before_userRewards_3 = fundHolders01.getUser(accounts[3])
        before_RewardEpoch = fundHolders01.getRewardEpoch(i)
        bba1 = mockgamblepool.getUserBalance(accounts[1])
        bba2 = mockgamblepool.getUserBalance(accounts[2])
        bba3 = mockgamblepool.getUserBalance(accounts[3])
        logging.info('before_userRewards_1 = {}'.format(before_userRewards_1))
        logging.info('before_userRewards_2 = {}'.format(before_userRewards_2))
        logging.info('before_userRewards_3 = {}'.format(before_userRewards_3))
        logging.info('before_RewardEpoch = {}'.format(before_RewardEpoch))
        logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
        logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))
        logging.info('mockgamblepool.getUserBalance(accounts[3]) = {}'.format(mockgamblepool.getUserBalance(accounts[3])))
        logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked() ))

        logging.info('***********************AFTER CLAIM!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        
        mockgamblepool.claimFund(fundId, i, {'from': accounts[1]})
        mockgamblepool.claimFund(fundId, i, {'from': accounts[2]})
        mockgamblepool.claimFund(fundId, i, {'from': accounts[3]})
        after_userRewards_1 = fundHolders01.getUser(accounts[1])
        after_userRewards_2 = fundHolders01.getUser(accounts[2])
        after_userRewards_3 = fundHolders01.getUser(accounts[3])
        after_RewardEpoch = fundHolders01.getRewardEpoch(i)
        logging.info('after_userRewards_1 = {}'.format(after_userRewards_1))
        logging.info('after_userRewards_2 = {}'.format(after_userRewards_2))
        logging.info('after_userRewards_3 = {}'.format(after_userRewards_3))
        logging.info('after_RewardEpoch= {}'.format(after_RewardEpoch))
        logging.info('mockgamblepool.getUserBalance(accounts[1]) = {}'.format(mockgamblepool.getUserBalance(accounts[1])))
        logging.info('mockgamblepool.getUserBalance(accounts[2]) = {}'.format(mockgamblepool.getUserBalance(accounts[2])))
        logging.info('mockgamblepool.getUserBalance(accounts[3]) = {}'.format(mockgamblepool.getUserBalance(accounts[3])))
        logging.info('mockgamblepool.totalStaked()  = {}'.format(mockgamblepool.totalStaked()))

        assert after_userRewards_1[i][3] + after_userRewards_2[i][3] + after_userRewards_3[i][3] == after_RewardEpoch[1]
        assert mockgamblepool.getUserBalance(accounts[1]) == bba1 + after_userRewards_1[i][3]
        assert mockgamblepool.getUserBalance(accounts[2]) == bba2 + after_userRewards_2[i][3]
        assert mockgamblepool.getUserBalance(accounts[3]) == bba3 + after_userRewards_3[i][3]
        assert after_RewardEpoch[0] >= after_RewardEpoch[1] 
        assert after_userRewards_1[i][3] > 0
        assert after_userRewards_2[i][3] > 0
        assert after_userRewards_3[i][3] > 0
        assert bba1 < mockgamblepool.getUserBalance(accounts[1])
        assert bba2 < mockgamblepool.getUserBalance(accounts[2])
        assert bba3 < mockgamblepool.getUserBalance(accounts[3])

        logging.info('**********************************************END CLAIM EPOCH {}********************************************'.format(i))

        i += 1'''