import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

def test_gamble_add_game(accounts, binaryevents,gamblepool):
    with reverts("Ownable: caller is not the owner"):
	    gamblepool.addGame(
	        binaryevents.address, 
	        'Binary Events', 
	        '0x0000000000000000000000000000000000000000',
	    	{"from": accounts[1]})
    assert gamblepool.getGamesCount() == 0
    gamblepool.addGame(
        binaryevents.address, 
        'Binary Events', 
        '0x0000000000000000000000000000000000000000',
    	{"from": accounts[0]})
    assert gamblepool.getGamesCount() == 1
    assert gamblepool.games(0)[0] == binaryevents.address
    assert gamblepool.games(0)[1] == 'Binary Events'
    assert gamblepool.games(0)[3] == 0

def test_gamble_change_game(accounts, binaryevents,gamblepool):
	with reverts("Ownable: caller is not the owner"):
		gamblepool.setGameState(0, 1, {"from": accounts[1]})

	gamblepool.setGameState(0, 1, {"from": accounts[0]})
	assert gamblepool.games(0)[3] == 1



