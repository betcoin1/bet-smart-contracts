import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'

def test_gamble_joinFund(accounts, binaryevents,mockgamblepool, bettoken, fundHolders01):
    mockgamblepool.registerFund(
        fundHolders01.address,
        100,
        True, 
        {"from": accounts[0]})
    fundCount = mockgamblepool.getFundCount()

    bettoken.mint(accounts[1], 1e18, {"from": accounts[0]})
    bettoken.approve(mockgamblepool.address, 1e18, {"from": accounts[1]})
    mockgamblepool.stake(1e18, {"from": accounts[1]})

    mockgamblepool.settleGameEvent(0, 1e18)
    assert fundHolders01.getRewardEpochCount() == 1
    assert fundHolders01.isJoined(accounts[1]) == True


