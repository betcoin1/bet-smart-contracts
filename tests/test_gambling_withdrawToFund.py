import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

#cancel bet and check fundBalance()
def test_gamble_withdrawToFund(accounts, binaryevents,gamblepool, bettoken):

	#prepare test data
	#add game
	gamblepool.addGame(binaryevents.address, 
		'Binary Events', 
		'0x0000000000000000000000000000000000000000', 
		{"from": accounts[0]})
	# create event for game
	gamblepool.createEvent(
		0, #_gameId
		chain.time() + 100 ,
		chain.time() + 66 ,
		'BTC/USD',
		[
			[1,4815987599255,0,0,0], #MoreOrEqual
			[4,4815987599255,0,0,0]  #less
		],
		{'from':accounts[0]}
	)
	eventId = binaryevents.getEventCount() - 1

	# stake
	#acc 0
	bettoken.mint(accounts[0], 100000, {"from": accounts[0]})
	bettoken.approve(gamblepool.address, 100000, {"from": accounts[0]})
	gamblepool.stake(100000, {"from": accounts[0]})

	i = 0
	bet_amount = 1000
	while i < 7:
		gamblepool.makeBet(0, eventId, 0, bet_amount, {"from":accounts[0]})
		i +=1
	penalty = (bet_amount/100)*gamblepool.CANCEL_BET_PENALTY_PERCENT()

	bet_id = gamblepool.getUsersBetsCount(accounts[0].address) - 1
	before_funds = gamblepool.fundBalance()
	before_balance = bettoken.balanceOf(accounts[8].address)
	gamblepool.cancelBet(bet_id,{'from':accounts[0]})
	gamblepool.cancelBet(bet_id - 1,{'from':accounts[0]})
	assert gamblepool.fundBalance() == before_funds + penalty*2

	


