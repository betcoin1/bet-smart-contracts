import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

eventId = 0
zero_address = '0x0000000000000000000000000000000000000000'

def test_gamble_changeFundState(accounts, binaryevents,gamblepool, bettoken, fundHolders01):
    gamblepool.registerFund(
        fundHolders01.address,
        1,
        True, 
        {"from": accounts[0]})
    fundCount = gamblepool.getFundCount()

    with reverts("Ownable: caller is not the owner"):
        gamblepool.changeFundState(
            fundCount - 1,
            1,
            0,
        	{"from": accounts[1]})
    
    with reverts(""):
        gamblepool.changeFundState(
            fundCount,
            1,
            0,
            {"from": accounts[0]})

    
    gamblepool.changeFundState(
        fundCount-1,
        10,
        1,
        {"from": accounts[0]})

    assert gamblepool.getFund(fundCount-1)[2] == 1
    assert gamblepool.getFund(fundCount-1)[1] == 10

    logging.info('fund = {}'.format(gamblepool.getFund(fundCount-1)))