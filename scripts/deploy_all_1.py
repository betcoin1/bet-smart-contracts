from brownie import *
import json

if  web3.eth.chainId  in [3, 4, 42, 97, 43113,80001, 421611]: 
    # Testnets
    private_key='2cdbeadae3122f6b30a67733fd4f0fb6c27ccd85c3c68de97c8ff534c87603c8'
    accounts.add(private_key)
elif  (web3.eth.chainId == 56 
    and web3.eth.chainId == 1 
    and web3.eth.chainId == 137
    and web3.eth.chainId == 43114):
    # Mainnet
    private_key=input('PLease input private key for deployer address..:')
    accounts.add(private_key)

print('Deployer:{}'.format(accounts[0]))
print('web3.eth.chain_id={}'.format(web3.eth.chainId))

# ETH_MAIN_ERC20_COLLATERAL_TOKENS = [
# '0x7728cd70b3dD86210e2bd321437F448231B81733', #NIFTSI ERC20
# '0x6b175474e89094c44da98b954eedeac495271d0f',  #DAI
# '0xdAC17F958D2ee523a2206206994597C13D831ec7',  #USDT
# '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',  #USDC
# ]

# ETH_RINKEBY_ERC20_COLLATERAL_TOKENS = [
# '0x1E991eA872061103560700683991A6cF88BA0028', #NIFTSI ERC20
# '0xc7ad46e0b8a400bb3c915120d284aafba8fc4735',  #DAI
# '0xc778417e063141139fce010982780140aa0cd5ab',  #WETH
# ]

# BSC_TESTNET_ERC20_COLLATERAL_TOKENS = [
# '0xCEFe82aDEd5e1f8c2610256629d651840601EAa8', #NIFTSI ERC20
# ]

# BSC_MAIN_ERC20_COLLATERAL_TOKENS = [
# '0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3',  #DAI
# '0x55d398326f99059fF775485246999027B3197955',  #USDT
# '0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d',  #USDC
# ]

# POLYGON_MAIN_ERC20_COLLATERAL_TOKENS = [
# '0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063',  #DAI
# '0xc2132D05D31c914a87C6611C10748AEb04B58e8F',  #USDT
# '0x2791bca1f2de4661ed88a30c99a7a9449aa84174',  #USDC
# ]

# AVALANCHE_MAIN_ERC20_COLLATERAL_TOKENS = [
# '0xba7deebbfc5fa1100fb055a87773e1e99cd3507a',  #DAI
# '0xde3a24028580884448a5397872046a019649b084',  #USDT
# ]

# TRON_MAIN_ERC20_COLLATERAL_TOKENS = [
# 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t',  #USDT
# ]


zero_address = '0x0000000000000000000000000000000000000000'

CHAIN = {   
    0:{'explorer_base':'io', 'WETH': zero_address},
    1:{'explorer_base':'etherscan.io', 'WETH': '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2'},
    4:{'explorer_base':'rinkeby.etherscan.io', 'WETH': '0xc778417E063141139Fce010982780140Aa0cD5Ab'},
    56:{'explorer_base':'bscscan.com', 'WETH': '0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c'}, #WBNB
    97:{'explorer_base':' testnet.bscscan.com', 'WETH': zero_address},
    137:{'explorer_base':'polygonscan.com', 'WETH': zero_address},
    80001:{'explorer_base':'mumbai.polygonscan.com', 'WETH': zero_address},  
    43114:{'explorer_base':'cchain.explorer.avax.network' , 'WETH': zero_address},
    43113:{'explorer_base':'cchain.explorer.avax-test.network', 'WETH': zero_address },

}.get(web3.eth.chainId, {'explorer_base':'io', 'WETH': zero_address})
print(CHAIN)



def main():
    print('Deployer account= {}'.format(accounts[0]))
    bettoken = BetToken.deploy({'from':accounts[0]})
    gamblepool   = GamblePool.deploy(bettoken.address,{'from':accounts[0]}) 
    oracle   = BetPriceProvider.deploy(CHAIN['WETH'],{'from':accounts[0]})
    binaryevents   = BinaryEvents.deploy(gamblepool.address,{'from':accounts[0]})
    rewardmodel   = RewardModel01.deploy({'from':accounts[0]}) 
    ####Funds##################
    fundHolders01   = FundHolders01.deploy({'from':accounts[0]})
    fundHiStaking01   = FundHiStaking01.deploy({'from':accounts[0]})
    fundFee   = FundFee.deploy({'from':accounts[0]})
    fundLottery   = FundFee.deploy({'from':accounts[0]})
    fundCreators01   = FundCreators01.deploy({'from':accounts[0]})
    fundBurn01   = FundBurn01.deploy({'from':accounts[0]})



    #Init
    bettoken.addMinter(fundBurn01, {'from':accounts[0]})
    binaryevents.setOracle(oracle.address, {'from':accounts[0]})


    

    # Print addresses for quick access from console
    print("----------Deployment artifacts-------------------")
    print("bettoken       = BetToken.at('{}')".format(bettoken.address))
    print("gamblepool     = GamblePool.at('{}')".format(gamblepool.address))
    print("oracle         = BetPriceProvider.at('{}')".format(oracle.address))
    print("binaryevents   = BinaryEvents.at('{}')".format(binaryevents.address))
    print("rewardmodel    = RewardModel01.at('{}')".format(rewardmodel.address))
    print("----------Deployment artifacts-------------------")
    print("fundHolders01  = FundHolders01.at('{}')".format(fundHolders01.address))
    print("fundFee        = FundFee.at('{}')".format(fundFee.address))
    print("fundLottery    = FundFee.at('{}')".format(fundLottery.address))
    print("fundHiStaking01= FundHiStaking01.at('{}')".format(fundHiStaking01.address))
    print("fundCreators01 = FundCreators01.at('{}')".format(fundCreators01.address))
    print("fundBurn01     = FundBurn01.at('{}')".format(fundBurn01.address))
    print("-------------------------------------------------")
    print("-------------------------------------------------")
    print("----------Explorer LInks-------------------------")
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],bettoken))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],gamblepool))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],oracle))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],binaryevents))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],rewardmodel))
    print("----------Funds-------------------")
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],fundHolders01))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],fundFee))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],fundLottery))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],fundHiStaking01))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],fundCreators01))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],fundBurn01))

    if  web3.eth.chainId in [1,4, 56, 97]:
        BetToken.publish_source(bettoken)
        GamblePool.publish_source(gamblepool)
        BetPriceProvider.publish_source(oracle)
        BinaryEvents.publish_source(binaryevents)
        RewardModel01.publish_source(rewardmodel)

        FundHolders01.publish_source(fundHolders01)
        FundFee.publish_source(fundFee)
        FundFee.publish_source(fundLottery)
        FundHiStaking01.publish_source(fundHiStaking01)
        FundCreators01.publish_source(fundCreators01)
        FundBurn01.publish_source(fundBurn01)