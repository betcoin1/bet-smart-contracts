import logging
import time
from brownie import *
LOGGER = logging.getLogger(__name__)


#0-0xE71978b1696a972b1a8f724A4eBDB906d9dA0885
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#1-0x989FA3062bc4329B2E3c5907c48Ea48a38437fB7
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#2-0xbD7E5fB7525ED8583893ce1B1f93E21CC0cf02F6
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#3-0xdf19CC2c917CD70D41071beBE9529E4d58868e27
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#4-0x32086FbD0d9eB9dE3072CFE61b96d89533068c55
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#5-0xe5a99Ea20412980121D7AEC1bF94580d7311Dc3e
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#6-0x2112B698Dc9417F974BA36D30101D1D2C63FEee8
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#7-0xf9eC6c8CCa151Bd3Bc5e1a7c30a1d47eEa40C8f7
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#8-0xd6398a8a367B59A1DF175dDb7ab951ABfDb05b57
private_key='???????????????enter private key???????????????'
accounts.add(private_key)

#9-0xF2171114f796E339513Ed334D7dE81540198C5cA
private_key='???????????????enter private key???????????????'

accounts.add(private_key)

price = "2 gwei"



def main():
	gamblepool     = GamblePool.at('0x39Eb7efE7F05D1cF80FBC11B4438F576FE5DD8B0')
	oracle         = BetPriceProvider.at('0x03321Ee6b6ac10EddB91d0bcD5BcC44EdFFC611c')
	binaryevents   = BinaryEvents.at('0x65c07554965e5592E4B8db6ABede844122EC2a31')
	bettoken       = BetToken.at('0x5ccf9d141Af74b2E5D7f5B8ed4d5Cd218D9e0e85')
	rewardmodel    = RewardModel01.at('0x564D23047e66BcC007dA401C8a736721EDFA3824')
	fundHolders01  = FundHolders01.at('0x2cF769733195aA6d1786663E6088f54e25Bfb611')
	fundFee        = FundFee.at('0x8FA36CF39d641D925a1cb12722EA3577618585c0')
	fundLottery    = FundFee.at('0xA9feF882889998aFC62f6b7Ac83bBe8ac141cA96')
	fundHiStaking01 = FundHiStaking01.at('0x33fE5773caf2AdEdb5c3E17D21563BFC361D75b5')
	fundCreators01 = FundCreators01.at('0xC9802A978bD0b220dE1a8DCC78E21F024A6351D4')
	fundBurn01     = FundBurn01.at('0xF32F9904ea6f7f1F9E3374718C23C5F1718DE05b')
	'''
	#registry funds
	gamblepool.registerFund(
		fundHolders01.address,
		5,
		True,
		{"from": accounts[0]}
	)
	gamblepool.registerFund(
		fundCreators01.address,
		5,
		True,
		{"from": accounts[0]}
	)
	gamblepool.registerFund(
		fundHiStaking01.address,
		1,
		True,
		{"from": accounts[0]}
	)
	gamblepool.registerFund(
		fundFee.address,
		3,
		True,
		{"from": accounts[0]}
	)
	gamblepool.registerFund(
		fundBurn01.address,
		1,
		True,
		{"from": accounts[0]}
	)
	gamblepool.registerFund(
		fundLottery.address,
		5,
		True,
		{"from": accounts[0]}
	)'''
	print('chain_time:{}'.format(chain.time()))

	#*******************************************************************************************************#
	#*********************************************RUN GAME AND BETS*****************************************#
	#*******************************************************************************************************#
	#prepare test data
	#add game
	'''gamblepool.addGame(binaryevents.address, 
		'Binary Events1', 
		rewardmodel.address, 
		{"from": accounts[0]})'''
	
	game_id = gamblepool.getGamesCount() - 1
	
	print('deadline_time:{}'.format(chain.time() + 300))
	print('expire_time:{}'.format(chain.time() + 400))
	
	# stake 8 acc (1-8)
	i = 7
	aCount = 9
	amount_mint = 10e18
	while i < aCount:
		bettoken.mint(accounts[i], amount_mint, {"from": accounts[0],"gas_price": price})
		bettoken.approve(gamblepool.address, amount_mint, {"from": accounts[i],"gas_price": price})
		gamblepool.stake(amount_mint, {"from": accounts[i],"gas_price":  price})
		i += 1

	# create event for game
	gamblepool.createEvent(
		game_id,
		chain.time() + 300 ,
		chain.time() + 400 ,
		'BTC/USD',
		[
			[1,4815987599256,0,0,0], #MoreOrEqual
			[4,4815987599256,0,0,0]  #less
		],
		{'from':accounts[1],"gas_price":"2 gwei"}
	)
	eventId = binaryevents.getEventCount() - 1
	
	
	# make bets acc (1-6)
	i = 1
	aCount = 7
	bet_amount = 4e18
	while i < aCount:
		gamblepool.makeBet(0, eventId, i%2, bet_amount, {"from":accounts[i],"gas_price": price})
		i +=1
	
	event = binaryevents.getEvent(eventId)
	deadline_time = event[3]
	diff_period = deadline_time - chain.time()

	print('current_time_before_sleep:{}'.format(chain.time()))
	#move date to after deadline
	time.sleep(diff_period + 100)
	
	#change status of event
	gamblepool.makeBet(game_id, eventId, 0, 10000, {"from":accounts[1],"gas_price": price})
	
	expire_time = event[2]
	diff_period = expire_time - chain.time()

	#move date to after expire date
	time.sleep(diff_period+100)
	

	#change event status
	binaryevents.checkStateWithSave(eventId, {"from": accounts[1],"gas_price": price})
	
	#check outcome status
	print('outcomeId = 0 IsWin?:{}'.format(binaryevents.getEvent(eventId)[5][0][4]))
	print('outcomeId = 1 IsWin?:{}'.format(binaryevents.getEvent(eventId)[5][1][4]))
	print('oraclePrice = {}'.format(binaryevents.getEvent(eventId)[6])) #check oraclePrice
	
	#call climBet
	i = 1
	aCount = 7
	while i < aCount:
		tx = gamblepool.claimBet(gamblepool.getUsersBetsCount(accounts[i]) - 1, {"from": accounts[i],"gas_price": price}) 
		if i%2 != 0:
			print('balance_acc[{}]={}'.format(i, gamblepool.getUserBalance(accounts[i])))
			prize = tx.events['WinClaimed']['amount']
			print('prize = {}'.format(prize))
		else:
			print('balance_acc[{}]={}'.format(i, gamblepool.getUserBalance(accounts[i])))
		i += 1

	print('fundBalance = {}'.format(gamblepool.fundBalance()))
	print('inBetsAmount = {}'.format(gamblepool.inBetsAmount()))

	#move tokens to funds
	gamblepool.settleGameEvent(game_id, eventId, {"from": accounts[0],"gas_price": price})

	print('fundBalance = {}'.format(gamblepool.fundBalance()))
	print('inBetsAmount = {}'.format(gamblepool.inBetsAmount()))

	'''
	#************************************************************************************************************#
	#************************************************WORK WITH FUNDS*********************************************#
	#************************************************************************************************************#
	#work with fundBurn - owner only
	print('FUNDBURN')
	print('before balance_acc(0)={}'.format(gamblepool.getUserBalance(accounts[0])))
	fundBurn01.withdrawAndBurn(gamblepool.getUserBalance(fundBurn01), {"from": accounts[0]})
	print('after balance_acc(0)={}'.format(gamblepool.getUserBalance(accounts[0])))

	#work with fundCreator - creator gets creators reward. Number of fund in fundRegisty - 1
	print('FUNDCREATOR')
	print('before balance_acc(1)={}'.format(gamblepool.getUserBalance(accounts[1])))
	gamblepool.claimFund(1, 888, {'from': accounts[1]})
	print('after balance_acc(1)={}'.format(gamblepool.getUserBalance(accounts[1])))

	#work with fundFee - only owner
	print('FUNDFEE')
	print('before balance_acc(0)={}'.format(gamblepool.getUserBalance(accounts[0])))
	fundFee.withdrawFee(gamblepool.getUserBalance(fundFee), {"from": accounts[0]})
	print('after balance_acc(0)={}'.format(gamblepool.getUserBalance(accounts[0])))
	
	#work with fundHiStaking
	print('FUNDHISTAKING')
	fundHiStaking01.closeCurrentEpoch({'from': accounts[1]})
	i = 1
	aCount = 7
	while i < aCount:
		print('before balance_acc({})={}'.format(i, gamblepool.getUserBalance(accounts[i])))
		gamblepool.claimFund(2, fundHiStaking01.currentEpoch()-1, {"from": accounts[i]})
		print('after balance_acc({})={}'.format(i, gamblepool.getUserBalance(accounts[i])))
		i += 1
	
	
	#work with fundHolders
	print('FUNDHOLDERS')
	fundHolders01.closeCurrentEpoch({'from': accounts[1]})
	i = 1
	aCount = 9
	while i < aCount:
		print('before balance_acc({})={}'.format(i, gamblepool.getUserBalance(accounts[i])))
		gamblepool.claimFund(0, fundHolders01.currentEpoch() - 1, {"from": accounts[i]})
		print('after balance_acc({})={}'.format(i, gamblepool.getUserBalance(accounts[i])))
		i += 1
	

	#************************************************************************************************************#
	#**********************************************WITHDRAW ALL TOKENS*******************************************#
	#************************************************************************************************************#

	i = 0
	aCount = 9
	print('WITHDRAW TOKENS FROM STARING')
	while i < aCount:
		print('before balance_acc({})={}'.format(i, gamblepool.getUserBalance(accounts[i])))
		if gamblepool.getUserBalance(accounts[i]) != 0:
			gamblepool.withdraw(gamblepool.getUserBalance(accounts[i]), {"from": accounts[i]})
		print('after balance_acc({})={}'.format(i, gamblepool.getUserBalance(accounts[i])))
		i += 1
	

	print('TRANSFER ALL TOKENS TO ACCOUNT[9]')
	i = 1
	aCount = 9
	while i < aCount:
		print('before balance_acc({})={}'.format(i, bettoken.balanceOf(accounts[i])))
		if bettoken.balanceOf(accounts[i]) != 0:
			bettoken.transfer(accounts[0], bettoken.balanceOf(accounts[i]), {'from': accounts[i]})
		print('after balance_acc({})={}'.format(i, bettoken.balanceOf(accounts[i])))
		i += 1

	print('holders_reward_epoch = {}'.format(fundHolders01.getRewardEpoch(fundHolders01.currentEpoch()-1)))
	print('hiStaking_reward_epoch = {}'.format(fundHiStaking01.getRewardEpoch(fundHiStaking01.currentEpoch()-1)))

	i = 1
	aCount = 9
	print('*******************************************************************************************')
	print('****************************************USERDATA in fundHolders01**************************')
	print('*******************************************************************************************')
	while i < aCount:
		print('account[{}] = {}'.format(i, fundHolders01.getUser(accounts[i])))
		i += 1

	print('*******************************************************************************************')
	print('****************************************USERDATA in fundHiSTAKING**************************')
	print('*******************************************************************************************')
	i = 1
	while i < aCount:
		aCount = 7
		print('account[{}] = {}'.format(i, fundHiStaking01.getUser(accounts[i])))
		i += 1'''








