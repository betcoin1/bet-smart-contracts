// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/token/ERC20/ERC20.sol";
import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/access/Ownable.sol";
import "./MinterRole.sol";

contract BetToken is ERC20, MinterRole, Ownable {
	
    uint256 constant public MAX_SUPPLY = 100000000e18;

    constructor()
    ERC20("BET token v0.0.0", "BET")
    MinterRole(msg.sender)
    { 
    }

    function mint(address to, uint256 amount ) external onlyMinter {
        require(totalSupply() + amount <= MAX_SUPPLY, "MAX_SUPPLY amount exceed");
        _mint(to, amount);
    }

    function burn(uint256 amount) external onlyMinter {
        _burn(msg.sender, amount);
    }

    /**
      * @dev Returns the bep token owner. BEP20 extending of ERC20
      */
    function getOwner() external view returns (address) {
        return owner();
    }

	
}