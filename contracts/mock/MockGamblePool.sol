// SPDX-License-Identifier: MIT


pragma solidity ^0.8.6;

import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/token/ERC20/utils/SafeERC20.sol";
import "../GamblePool.sol";


contract MockGamblePool is GamblePool {
    using SafeERC20 for IERC20;

    constructor (address _token) 
        GamblePool(_token)
    {

    }

    function settleGameEvent(uint256 _gameId, uint256 _eventId) external override {

        uint256 fReward = _eventId;
        //Settlement
        //inBetsAmount -= fReward; //Consolidate account dec TODO analit?
        fundBalance  += fReward; //Consolidate account inc

        //Save that this event is settled
        //eventsSettlement[_gameId][_eventId].fundReward = fReward;
        lastSettledCreator = msg.sender;
        //Distribute fReward with all active funds
        uint256 totalPercent = _getFundRegistryTotalPoints();
        uint256 thisFundReward;
        // TODO May be we need check for 100%
        for (uint8 i = 0; i < fundsRegistry.length; i ++ ){
            if (fundsRegistry[i].state == FundState.Active) {
                thisFundReward = fReward 
                * fundsRegistry[i].sharePercent
                / totalPercent;
                _increaseTotalAmount(fundsRegistry[i].contractAddress, thisFundReward);
                require(
                    IFund(fundsRegistry[i].contractAddress).newReward(thisFundReward),
                    "Fail fund topup"
                );
            }

        }

    }

    function makeBet(
        uint256 _gameId,
        uint256 _eventId,
        uint8   _eventOutcomeIndex,
        uint256 _betAmount
    ) 
        external 
        override
        returns (bool)
    {
        //TODO some check
        // require(_betAmount >= MIN_BET_AMOUNT, "Your bet is too small");
        // require(_availableAmount(msg.sender) >= _betAmount, "Need more in stake");
        // Game storage g = games[_gameId];
        // require(IGammbleGame(g.eventContract).isEventExist(_eventId), 
        //     "Seems like this event does not exist yet"
        // );
        // //1. Register new bet in Game contract
        // if (IGammbleGame(g.eventContract).incBetCountForOutcome(
        //         _eventId,
        //         _eventOutcomeIndex, 
        //         _betAmount
        //     )
        // ) 
        //If incBetCountForOutcome returns true then:
            //2. Add new bet in betStorage
        _addBet(msg.sender, Bet({
            eventContract: address(0),
            eventId: _eventId,
            eventOutcomeIndex: _eventOutcomeIndex,
            betAmount: _betAmount,
            betTimestamp: block.timestamp,
            currentState: BetState.Done,
            result: BetResult.Undefined
        }
        ));
     
        //3. change internal balance
        balances[msg.sender] -= _betAmount;
        inBetsAmount += _betAmount;
        emit NewBet(
            msg.sender,
            _gameId,
            _eventId,
            _eventOutcomeIndex,
            _betAmount
        );
        _updateRewardPoints(msg.sender);
        return true;
    }

    function cancelBet(uint256 _betId) external override {
        //TODO more  checks
        Bet storage b = userBets[msg.sender][_betId];
        
        //3. change internal balance
        inBetsAmount -= b.betAmount;
        uint256 penalty = b.betAmount / 100 * CANCEL_BET_PENALTY_PERCENT;
        fundBalance += penalty;
        balances[msg.sender] += b.betAmount - penalty; 
        _cancelBet(msg.sender, _betId);
        _updateRewardPoints(msg.sender);
        
    }


}
    