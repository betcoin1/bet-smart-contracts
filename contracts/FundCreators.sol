// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

//import "./BetTypes.sol";
//import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/access/Ownable.sol";
import "../interfaces/IGamblePool.sol";
import "../interfaces/IFund.sol";

contract FundCreators01 is IFund {

    
    uint256 constant public SCALE = 1e18;
    uint256 immutable public fundStartBlock; 

    bool public registeredInPool;

    IGamblePool public  gamblePool;

    mapping(address => uint256) public creatorsReward;

    event UserRewardClaimed(address pool, address user, uint256 epoch, uint256 reward);
    
    constructor () {
            fundStartBlock = block.number;
    }

    modifier onlyFund() {
      require(msg.sender == address(gamblePool));
      _;
    }

    function registerFund() external override returns (bool) {
        require(!registeredInPool, "Already registered");
        gamblePool = IGamblePool(msg.sender);
        registeredInPool = true;
        return registeredInPool;
    }

    function joinFund(address _user) external override returns (bool) {
        return true;
    }

    function newReward(uint256 _amount) external override onlyFund returns (bool) {
        creatorsReward[gamblePool.lastSettledCreator()] += _amount;
        return true;
    }

    ///  Claim from last available 
    function claimReward(address _user) external override onlyFund 
        returns 
    (uint256 userRewardAmount) 
    {
        return claimRewardForEpoch(_user, 0);
    }

    function claimRewardForEpoch(address _user, uint256 _epoch) 
        public 
        override 
        onlyFund 
        returns 
    (uint256 userRewardAmount)
    {
        require(creatorsReward[_user] > 0 , "No funds");
        // calc reward
        userRewardAmount = _getUserRewardInEpoch(_user, _epoch);
        creatorsReward[_user] = 0;
        emit UserRewardClaimed(address(this), _user, _epoch, userRewardAmount);
        return userRewardAmount;
    }

    
    function updateUserState(address _user) external override onlyFund {
        registeredInPool;
    }

    function getAvailableReward(address _user) external view override returns (uint256) {
        return _getUserRewardInEpoch(_user, 0);
    }

    function getAvailableReward(address _user, uint256 _epoch) external view override returns (uint256) {
        return _getUserRewardInEpoch(_user, _epoch);
    }

    function getUserEpochCount(address _user) public view returns (uint256) {
        return 0;
    }

    function isJoined(address _user) external view override returns (bool joined) {
        return true;
    } 

    ////////////////// Internals ////////////////////////// 

    /// Main reward calclogiс 
    function _getUserRewardInEpoch(address _user, uint256 _epoch) internal view returns (uint256 r) {
            r = creatorsReward[_user];
        return r;    
    }    
}
