// SPDX-License-Identifier: MIT
// Degen Farm. Collectible NFT game
pragma solidity ^0.8.6;

import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/access/Ownable.sol";
import "../interfaces/IUniswapV2Pair.sol";
import "../interfaces/IUniswapV2Factory.sol";
import "../interfaces/AggregatorV3Interface.sol"; 

contract BetPriceProvider is Ownable {

    address immutable public WETH;// for MainNet 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
    address constant UNI_FACTORY = 0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f;
    uint256 custom_price;
    
    //Only UniSwap price feed is implemented!!!!
    enum OracleType {Custom, Uniswap, ChainLink, Compound}
    
    struct PriceFeedProvider {
        address provider;
        OracleType providerType;
    }


    mapping(address => PriceFeedProvider) public feed;

    mapping(bytes32 => PriceFeedProvider) public namedFeed;
    bytes32 public nameHash;


    
    event NewOracleAdded(address indexed _amulet, address _provider, uint8 _providerType);
    event NewPairOracleAdded(string  pairName, address provider, uint8 providerType);

    constructor(address _weth) {
        WETH = _weth;
    }

    function setCustomPrice(uint256 price) external onlyOwner {
        custom_price = price;
    }

    function getLastPrice(address _amulet) external view returns (uint256) {
        address token0;
        address token1;
        address pair;
        uint112 _reserve0;
        uint112 _reserve1;
        uint32  _timeStamp;
        uint256 rate;
        require(_amulet != address(0), "Please ask me NON zero asset");
        if  (feed[_amulet].providerType == OracleType.Uniswap) {
            if (feed[_amulet].provider == address(0)){
                //Try find pair address on UniSwap
                pair = IUniswapV2Factory(UNI_FACTORY).getPair(_amulet, WETH);
                require(pair != address(0), "Can't find pair on UniSwap");
            }   
            token0 = IUniswapV2Pair(pair).token0();
            token1 = IUniswapV2Pair(pair).token1();
            (_reserve0, _reserve1, _timeStamp) = IUniswapV2Pair(pair).getReserves();
            //Let's check what token is WETH  and calc rate
            if  (token0 == WETH) {
                rate = _reserve0*1e18/_reserve1;
            } else {
                rate = _reserve1*1e18/_reserve0;
            }
        } else if (feed[_amulet].providerType == OracleType.Custom) {
            //Dummy MAGIC Price Feed - BULL TREND Forever ;-)
            //It may be used for tests
            rate = uint256(block.timestamp);
        } else {
            //Other Oracle types are not implemented yet
            rate = 0;
        }
        return rate;
    }


    function setPriceFeedProvider(address _token, address _provider, uint8 _providerType) 
        external
        onlyOwner 
    {
        _setPriceFeedProvider(_token, _provider, _providerType);
    }

    function setBatchPriceFeedProvider(address[] memory _token, address[] memory _provider, uint8[] memory _providerType) 
        external
        onlyOwner 
    {
        require(_token.length == _provider.length , 'Arguments must have same length');
        require(_token.length == _providerType.length , 'Arguments2 must have same length');
        require(_token.length < 255 , 'To long array');
        for (uint8 i = 0; i < _token.length; i++) {
            _setPriceFeedProvider(_token[i], _provider[i], _providerType[i]);
        } 
     
    }
    
    function setPriceFeedProviderByPairName(string memory _pairName, address _provider, uint8 _providerType) 
        external
        onlyOwner 
    {
        _setPriceFeedProviderByPairName( _pairName, _provider, _providerType);
    }

    function getLastPriceByPairNameStr(string memory _pairName) public view returns (int256) {
        return getLastPriceByPairName(
           keccak256(abi.encodePacked(_pairName))
        );   
    }

    function getLastPriceByPairName(bytes32 _nameHash) public view returns (int256) {
        //TODO  SOft Chcek of existence
        int256 rate;
        if  (namedFeed[_nameHash].providerType == OracleType.ChainLink) {
            ( 
                uint80 roundID, 
                int price,
                uint startedAt,
                uint timeStamp,
                uint80 answeredInRound
            ) = AggregatorV3Interface(namedFeed[_nameHash].provider).latestRoundData();
            rate = price;
        } else if (namedFeed[_nameHash].providerType == OracleType.Custom) {
            //Dummy MAGIC Price Feed - BULL TREND Forever ;-)
            //It may be used for tests
            //rate = int256(block.timestamp);
            if (custom_price > 0) {
                rate = int256(custom_price);
            }
            else {rate = int256(block.timestamp);
            }
        } else {
            //Other Oracle types are not implemented yet
            rate = 0;
        }
        return rate;
    }

    //function isPairExist(bytes32 _nameHash)

    //////////////////////////////////////////////////////////
    //////   Internals                                    ////
    //////////////////////////////////////////////////////////

    function _setPriceFeedProvider(address _token, address _provider, uint8 _providerType) 
        internal
    {
        require(_token != address(0), "Can't add oracle for None asset");
        //Some checks for available oracle types
        if  (OracleType(_providerType) == OracleType.Uniswap) {
            require(
                keccak256(abi.encodePacked(IUniswapV2Pair(_provider).name())) ==
                keccak256(abi.encodePacked('Uniswap V2')), "It seems NOT like Uniswap pair");
            require(IUniswapV2Pair(_provider).token0() == WETH || IUniswapV2Pair(_provider).token1() == WETH,
                "One token in pair must be WETH"
            );
        }

        feed[_token].provider     = _provider;
        feed[_token].providerType = OracleType(_providerType);
        emit NewOracleAdded(_token, _provider, _providerType);
    }

    function _setPriceFeedProviderByPairName(string memory _pairName, address _provider, uint8 _providerType) 
        internal
    {
        //Some checks for available oracle types
        if  (OracleType(_providerType) == OracleType.Uniswap) {
            require(
                keccak256(abi.encodePacked(IUniswapV2Pair(_provider).name())) ==
                keccak256(abi.encodePacked('Uniswap V2')), "It seems NOT like Uniswap pair");
            require(IUniswapV2Pair(_provider).token0() == WETH || IUniswapV2Pair(_provider).token1() == WETH,
                "One token in pair must be WETH"
            );
        }
        //TODO   Check ChainLInk provider
        nameHash = keccak256(abi.encodePacked(_pairName));
        namedFeed[keccak256(abi.encodePacked(_pairName))].provider     = _provider;
        namedFeed[keccak256(abi.encodePacked(_pairName))].providerType = OracleType(_providerType);
        emit NewPairOracleAdded(_pairName, _provider, _providerType);
    }

}