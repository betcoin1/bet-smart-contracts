// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

import "./BetTypes.sol";
import "../interfaces/IBetPriceProvider.sol";
import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/access/Ownable.sol";

contract BinaryEvents is  Ownable {
    
    uint256 constant public WIN_DISTRIB_PERCENT = 80;
    bool public is_valid = true;

    address public pool;
    address public oracle;

    GamblingEvent[] eventRegistry;

    event NewEvent(uint256 indexed eventId, address source);

    constructor (address _pool) {
        pool = _pool;
    }

    /// @dev Create new binary gambling event 
    /// @param _expire Event expiration unix datetime
    /// @param _orderDeadLine No more bets after this, unix datetime
    /// @param _assetSymbol pair symbol for bet i.e. `BTS/USD` that may be used
    /// with chain link oracles
    function createEvent(
        uint256 _expire, 
        uint256 _orderDeadLine, 
        string  memory _assetSymbol,
        Outcome[2] memory _outcomes,
        address _creator
    ) 
       external 
    {
        //TODO  Add integrety and logic checks(etc _assetSymbol existence)
        //TODO Check _orderDeadLine  diff from e3xpirationbrownie test
        require(msg.sender == pool, "Only pool can call it");
        require(_outcomes.length == 2, "Only 2 outcomes for this event type");
        require(
            _outcomes[0].compare == CompareSign.MoreOrEqual && _outcomes[1].compare == CompareSign.Less || 
            _outcomes[1].compare == CompareSign.MoreOrEqual && _outcomes[0].compare == CompareSign.Less 
            ,"Only binary outcome for this contract"
        );
        require(
            _outcomes[0].expectedValue == _outcomes[1].expectedValue
            ,"Only binary outcome for this contract - one expectedValue"
        );
        GamblingEvent storage e = eventRegistry.push();
        e.outcome[0] = _outcomes[0];
        e.outcome[0].isWin = false;
        e.outcome[1] = _outcomes[1];
        e.outcome[0].isWin = false;
        e.creator = _creator;
        e.state = EventState.Active;
        e.expirationTime = _expire;
        e.orderDeadLineTime = _orderDeadLine;
        e.eventAssetSymbol = _assetSymbol;

        emit NewEvent(eventRegistry.length - 1, address(this));

    }
    
    //temprory,  for tests.  It cant be call without token transfer
    function incBetCountForOutcome(uint256 _eventId, uint256 _outcomeId, uint256 _amount) external returns(bool) {
        require(msg.sender == pool, "Only pool can call it");
        _updateEventState(_eventId);
        // require(
        //     (eventRegistry[_eventId].state == EventState.Active),
        //     "Sorry, No more bets"
        // );
        if  (eventRegistry[_eventId].state != EventState.Active){
            return false;
        }
        require(
            (_outcomeId == 0 || _outcomeId == 1),
            "Only 2 outcomes for this event type"
        );
        GamblingEvent storage e = eventRegistry[_eventId];
        //One more state check  incase when _updateEventState(_eventId) 
        // have change state
        if  (e.state == EventState.Active) {
            e.outcome[_outcomeId].weiRaised += int256(_amount);
            e.outcome[_outcomeId].betCount  += 1;
            return true;
        }  else {
           return false;  
        } 

    }

    function decBetCountForOutcome(uint256 _eventId, uint256 _outcomeId, uint256 _amount) external returns(bool) {
        require(msg.sender == pool, "Only pool can call it");
        _updateEventState(_eventId);
        // require(
        //     (eventRegistry[_eventId].state == EventState.Active),
        //     "Sorry, No more bets"
        // );
        if  (eventRegistry[_eventId].state != EventState.Active){
            return false;
        }
        require(
            (_outcomeId == 0 || _outcomeId == 1),
            "Only 2 outcomes for this event type"
        );
        GamblingEvent storage e = eventRegistry[_eventId];
        require(e.state == EventState.Active, "No more cancels");
        require(e.outcome[_outcomeId].weiRaised >= int256(_amount), "You are hacker");
        e.outcome[_outcomeId].weiRaised -= int256(_amount);
        e.outcome[_outcomeId].betCount  -= 1;
        return true;

    }

    function checkStateWithSave(uint256 _eventId) external {
        require(
            (
              eventRegistry[_eventId].state == EventState.Hold ||
              eventRegistry[_eventId].state == EventState.Active   
            ),
            "Event state must be Active (0) or Hold (1)"
        );
        _updatePriceFromOracle(_eventId);
        _updateEventState(_eventId);
        
    }

    function setOracle(address _oracle) external onlyOwner {
        oracle = _oracle;
    }
    
    function isEventExist(uint256 _id) public view returns(bool) {
        if (eventRegistry.length <= _id) {
            return false;
        } else {

          return bool(eventRegistry[_id].expirationTime > 0);    
        }
        
    }
    
    function getEvent(uint256 _id) public view returns(GamblingEvent memory) {
        return eventRegistry[_id];
    }

    function getEventCount() public view returns(uint256) {
        return eventRegistry.length;
    }

    function isWinWithAmount(
        uint256 _eventId, 
        uint8 _outcomeIndex, 
        uint256 _betAmount
    ) 
        external 
        returns (bool, uint256) 
    {
        GamblingEvent memory e = eventRegistry[_eventId];
        require(e.state == EventState.Finished, "Not Finished yet");
        Outcome memory o = e.outcome[_outcomeIndex];
        if (o.isWin) {
            // !!!!!!!!  define loss outcom weiraised
            // formula may differ for difrent GameEventType. For Binary - this is  
            uint256 loss = uint256(e.outcome[_outcomeIndex == 0 ? 1 : 0].weiRaised);
            /*uint256 winAmount = 
            (loss * WIN_DISTRIB_PERCENT / 100)  //Winners prize share 
            * (_betAmount * 1e6 / uint256(o.weiRaised))  //bet's weight in all from this outcome. 1e6 -scale denominator
            / 1e6; //devide  scale denominator*/
            uint256 winAmount = 
            loss * WIN_DISTRIB_PERCENT*_betAmount*1e6/uint256(o.weiRaised)/1e6/100; //bet's weight in all from this outcome. 1e6 -scale denominator
            //Lets define Win Amount
            return (true, winAmount);
        } else {
            return (false, 0);
        }

    }

    function comparePrice(CompareSign _compareSign, int256 _outcomePrice, int256 _oraclePrice) external pure returns (bool) {
        return  _compare(_compareSign, _outcomePrice, _oraclePrice); 
    }
  
    //////////////////////////////////////////////////////////////
    ////////// Internals                                    //////
    //////////////////////////////////////////////////////////////
    function _updateEventState(uint256 _eventId) internal {
        require(
            eventRegistry[_eventId].state != EventState.Finished,
            "Event is Finished"
        );
        GamblingEvent storage e = eventRegistry[_eventId];
        if  (
              eventRegistry[_eventId].orderDeadLineTime <= block.timestamp &&
              eventRegistry[_eventId].expirationTime > block.timestamp
            ) 
            {
                //Just update eventstate
                if (e.outcome[0].betCount == 0 && e.outcome[1].betCount == 0) {
                    e.state = EventState.Canceled;    
                } else {
                    e.state = EventState.Hold;    
                }
            }  
        else if 
            (
                eventRegistry[_eventId].expirationTime < block.timestamp
            )
            {
                if (e.outcome[0].betCount == 0 && e.outcome[1].betCount == 0) {
                    e.state = EventState.Canceled;    
                } else {
                    _updatePriceFromOracle(_eventId);
                    e.state = EventState.Finished;    
                }

            } 

    }

    function _updatePriceFromOracle(uint256 _eventId) internal {
        if  (
              eventRegistry[_eventId].expirationTime <= block.timestamp &&
              eventRegistry[_eventId].state != EventState.Finished
            ) {
                GamblingEvent storage e = eventRegistry[_eventId];
                int256 price = IBetPriceProvider(oracle).getLastPriceByPairName(
                    keccak256(abi.encodePacked(e.eventAssetSymbol))
                );
                if (e.oraclePrice == 0) { 
                    e.oraclePrice = price;
                    //Lets define which outcome(s) is(are) win
                    //for this w
                    for (uint8 i = 0; i < e.outcome.length; i ++) {
                        if (_compare(e.outcome[i].compare, e.outcome[i].expectedValue, e.oraclePrice) == true) {
                            e.outcome[i].isWin = true;
                        } else {
                            e.outcome[i].isWin = false;
                        }

                    }

                }   
            }

    }


    function _compare(
        CompareSign _compareSign, 
        int256 _outcomePrice, 
        int256 _oraclePrice
    ) 
        internal pure returns (bool) 
    {
        if (_compareSign == CompareSign.Equal) {
            return bool(_outcomePrice == _oraclePrice);
        
        } else if (_compareSign == CompareSign.MoreOrEqual) {
            return bool(_outcomePrice >= _oraclePrice);
        
        } else if (_compareSign == CompareSign.LessOrEqual) {
            return bool(_outcomePrice <= _oraclePrice);
        
        } else if (_compareSign == CompareSign.More) {
            return bool(_outcomePrice > _oraclePrice);
        
        } else if (_compareSign == CompareSign.Less) {
            return bool(_outcomePrice < _oraclePrice);
        
        } else if (_compareSign == CompareSign.NotEqual) {
            return bool(_outcomePrice != _oraclePrice);
        }

    }
}