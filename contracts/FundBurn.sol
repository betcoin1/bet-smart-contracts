// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

import "../interfaces/IGamblePool.sol";
import "../interfaces/IFund.sol";
import "../interfaces/IBurn.sol";

contract FundBurn01 is IFund {


    bool public registeredInPool;
    uint256 public rewardForCurrentEpoch;

    IGamblePool public  gamblePool;


    modifier onlyFund() {
      require(msg.sender == address(gamblePool));
      _;
    }

    function registerFund() external override returns (bool) {
        require(!registeredInPool, "Already registered");
        gamblePool = IGamblePool(msg.sender);
        registeredInPool = true;
        return registeredInPool;
    }

    function newReward(uint256 _amount) external override onlyFund returns (bool) {
        rewardForCurrentEpoch += _amount;
        return true;
    }

    function withdrawAndBurn(uint256 _amount) external {
        require(msg.sender == gamblePool.owner(), "Onle owner");
        require(_amount <= rewardForCurrentEpoch, "Too much for burn");
        rewardForCurrentEpoch -= _amount;
        gamblePool.withdraw(_amount);
        IBurn(gamblePool.projectToken()).burn(_amount);
    }


    /////////////////////////////////////////////////////////////////
    //  All functions below for interface compatibilty ONLY       ///
    /////////////////////////////////////////////////////////////////

    function joinFund(address _user) external override returns (bool) {
        return true;
    }

    function claimReward(address _user) external override onlyFund 
        returns 
    (uint256 userRewardAmount) 
    { 
        return 0;
    }

    function claimRewardForEpoch(address _user, uint256 _epoch) 
        public 
        override 
        onlyFund 
        returns 
    (uint256 userRewardAmount)
    {
        return 0;
    }

    function updateUserState(address _user) external override onlyFund {
        registeredInPool;
    }

    function getAvailableReward(address _user) external view override returns (uint256) {
       return 0;
    }

    function getAvailableReward(address _user, uint256 _epoch) external view override returns (uint256) {
        return 0;
    }

    function isJoined(address _user) external view override returns (bool joined) {
        return true;
    } 
 
}
