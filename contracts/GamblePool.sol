// SPDX-License-Identifier: MIT

import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/token/ERC20/IERC20.sol";
import "./BetTypes.sol";
import "../interfaces/IGambleGame.sol";
import "../interfaces/IRewardModel.sol";
import "../interfaces/IFund.sol";
import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/access/Ownable.sol";
import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/token/ERC20/utils/SafeERC20.sol";

pragma solidity ^0.8.6;

contract GamblePool is Ownable {
    using SafeERC20 for IERC20;
    
    
    uint256 constant public CANCEL_BET_PENALTY_PERCENT = 30;
    uint256 constant public MIN_BET_AMOUNT = 1000;
    uint256 constant public SCALE = 1e18; 
    address immutable public projectToken;

    address public lastSettledCreator;

    
    /////////////////////////////////////////
    //      Accounting  plan          ///////
    /////////////////////////////////////////

    //*************************************// 
    //  Consolidate  accounts              //
    //*************************************//
    //  totalStaked include  inBetsAmount, fundBalance
    uint256 public totalStaked;
    
    // Main acount with amount of tokens that are frozen in bets.
    uint256 public inBetsAmount;  
    
    //Main acount with amount of tokens in all funds
    // Sub Account for totalStaked
    uint256 public fundBalance;
    //*************************************//

    //*************************************// 
    //  analytical accounts                //
    //*************************************//  
    //Users balance in this pool (analiticals for totalStaked)
    mapping(address => uint256) internal balances;

    //**************************************//

    //Users bets ()
    mapping(address => Bet[]) internal userBets;

    

    //map from gameId from eventid to fund share amount
    mapping(uint256 => mapping(uint256 => EventSettle)) public eventsSettlement;




    Game[] public games;
    Fund[] internal fundsRegistry;

    

    event Staked(address indexed user, uint256 amount);
    event Withdrawn(address indexed user, uint256 amount);
    event NewBet(
        address indexed better,
        uint256 _gameId,
        uint256 _eventId,
        uint8   _eventOutcomeIndex,
        uint256 _betAmount
    );
    event WinClaimed(
        address indexed better,
        uint256 betId,
        uint256 amount 
    );

    event FundStateChanged(address fund, uint256 percentShare, FundState state);
    
    constructor (address _token) {
        projectToken = _token;
    }

    function stake(uint256 _amount) external virtual {
        require(_amount > 0, "Cant stake zero");
        require(
            IERC20(projectToken).transferFrom(msg.sender, address(this), _amount)
        );
        _increaseTotalAmount(msg.sender, _amount);
        _updateRewardPoints(msg.sender);        
        emit Staked(msg.sender, _amount);
    }

    function withdraw(uint256 _amount) external virtual {
        require(_amount > 0, "Cant withdraw zero");
        require(_availableAmount(msg.sender) >= _amount);
        _decreaseTotalAmount(msg.sender, _amount);
        _updateRewardPoints(msg.sender);
        require(
            IERC20(projectToken).transfer(msg.sender, _amount)
        );
        emit Withdrawn(msg.sender, _amount);
    }

    //TODO  add restrictions:fee or who can create
    function createEvent(
        uint256 _gameId,
        uint256 _expire, 
        uint256 _orderDeadLine, 
        string  memory _assetSymbol,
        Outcome[2] memory _outcomes
    )
        external
        virtual
    {
        Game memory g = games[_gameId];
        require(g.state == GameState.Active, "Sorry game is not available");
        //TODO some  checs  and fee if need
        IGammbleGame(g.eventContract).createEvent(_expire, _orderDeadLine, _assetSymbol, _outcomes, msg.sender);

    }

    function createEvent2(
        uint256 _gameId,
        uint256 _expire, 
        uint256 _orderDeadLine, 
        string  memory _assetSymbol,
        Outcome[] memory _outcomes
    )
        external
        virtual
    {
        Game memory g = games[_gameId];
        require(g.state == GameState.Active, "Sorry game is not available");
        //TODO some  checs  and fee if need
        if (_outcomes.length == 2) {
            Outcome[2] memory oc2;
            oc2[0] = _outcomes[0];
            oc2[1] = _outcomes[1];
            IGammbleGame(g.eventContract).createEvent(_expire, _orderDeadLine, _assetSymbol, oc2, msg.sender);
        } else {
            IGammbleGame(g.eventContract).createEvent2(_expire, _orderDeadLine, _assetSymbol, _outcomes, msg.sender);
        }    

    }

    function makeBet(
        uint256 _gameId,
        uint256 _eventId,
        uint8   _eventOutcomeIndex,
        uint256 _betAmount
    ) 
        external 
        virtual
        returns (bool)
    {
        //TODO some check
        require(_betAmount >= MIN_BET_AMOUNT, "Your bet is too small");
        require(_availableAmount(msg.sender) >= _betAmount, "Need more in stake");
        Game storage g = games[_gameId];
        require(IGammbleGame(g.eventContract).isEventExist(_eventId), 
            "Seems like this event does not exist yet"
        );
        //1. Register new bet in Game contract
        if (IGammbleGame(g.eventContract).incBetCountForOutcome(
                _eventId,
                _eventOutcomeIndex, 
                _betAmount
            )
        ) 
        //If incBetCountForOutcome returns true then:
        {
            //2. Add new bet in betStorage
            _addBet(msg.sender, Bet({
                eventContract: g.eventContract,
                eventId: _eventId,
                eventOutcomeIndex: _eventOutcomeIndex,
                betAmount: _betAmount,
                betTimestamp: block.timestamp,
                currentState: BetState.Done,
                result: BetResult.Undefined
            }
            ));
         
            //3. change internal balance
            balances[msg.sender] -= _betAmount;
            inBetsAmount += _betAmount;
            emit NewBet(
                msg.sender,
                _gameId,
                _eventId,
                _eventOutcomeIndex,
                _betAmount
            );
            _updateRewardPoints(msg.sender);
            return true;
        } else {
            return false;
        }    
    }

    function cancelBet(uint256 _betId) external virtual {
        //TODO more  checks
        Bet storage b = userBets[msg.sender][_betId];
        if (IGammbleGame(b.eventContract).decBetCountForOutcome(
                b.eventId,
                b.eventOutcomeIndex, 
                b.betAmount
            ))
        {
            //3. change internal balance
            inBetsAmount -= b.betAmount;
            uint256 penalty = b.betAmount / 100 * CANCEL_BET_PENALTY_PERCENT;
            fundBalance += penalty;
            balances[msg.sender] += b.betAmount - penalty; 
            _cancelBet(msg.sender, _betId);
            _updateRewardPoints(msg.sender);
        }

        
    }

    function claimBet(uint256 _betId) external virtual {
        Bet storage b = userBets[msg.sender][_betId];
        require(b.currentState == BetState.Done);
        // GamblingEvent memory e = 
        //     IGammbleGame(b.eventContract).getEvent(b.eventId);
        (bool isWin, uint256 prize) = IGammbleGame(b.eventContract).isWinWithAmount(
            b.eventId, 
            b.eventOutcomeIndex, 
            b.betAmount
        );
        if (isWin == true) {
            ///////////////// WIN CASE /////////////////
            //1. Lets return better own amount + prize
            inBetsAmount -= (b.betAmount + prize);
            balances[msg.sender] += (b.betAmount + prize);
            //save result in bet
            b.result = BetResult.Win;
            _updateRewardPoints(msg.sender);
            emit WinClaimed(msg.sender, _betId, prize);
        } else {
            ///////////////// LOSS CASE /////////////////
            //save result in bet
            b.result = BetResult.Lose;
        }
        //save new (and last) state in bet
        b.currentState = BetState.Claimed;
        
    }
    //Depricated due new fundRegisrty concept
    // function withdrawToFund(address _fund, uint256 _amount) external {
    //     require(enabledFunds[_fund], "This fund is disabled or unknown");
    //     IERC20(projectToken).safeTransfer(_fund, _amount);
    //     fundBalance -= _amount;
    // }

    function joinFund(uint256 _fundId) external virtual{

        Fund memory f = fundsRegistry[_fundId];
        require(
            IFund(f.contractAddress).joinFund(msg.sender),
            "Fail join fund"
        );
    }

    function claimFund(uint256 _fundId, uint256 _epoch) external virtual {
        uint256 claimed = IFund(
            fundsRegistry[_fundId].contractAddress
        ).claimRewardForEpoch(msg.sender, _epoch);
        if (claimed > 0) {
            _internalTransfer(
                fundsRegistry[_fundId].contractAddress,
                msg.sender,
                claimed
            );
            //decrease consolidate fund balance
            fundBalance -= claimed;
            _updateRewardPoints(msg.sender);            
        }
        
    }

    //////////////////////////////////////////////////////
    ////    Admin functions    ///////////////////////////
    //////////////////////////////////////////////////////

    function addGame(
        address _eventContract, 
        string memory _name, 
        address _rewardModel
    ) 
        external onlyOwner
    {
        games.push(
            Game({
                eventContract: _eventContract,
                name: _name,
                rewardModelContract: _rewardModel,
                state: GameState.Active
            })
        );
        //TODO  add event
    }

    function setGameState(uint256 _id, GameState _state) external virtual onlyOwner {
        Game storage g = games[_id];
        g.state = _state;
    }

    function settleGameEvent(uint256 _gameId, uint256 _eventId) external virtual onlyOwner {
        require(eventsSettlement[_gameId][_eventId].fundReward == 0, "Event already settled");
        Game memory g = games[_gameId];
        uint256 fundPercent = IRewardModel(g.rewardModelContract).getFundPercent();
        uint256 fReward;
        GamblingEvent memory e = 
            IGammbleGame(g.eventContract).getEvent(_eventId);
        require(e.state == EventState.Finished, "Event is not finished yet"); 
        // lets get fund reward share from this event   
        for (uint8 i = 0; i < e.outcome.length; i ++) {
            if (e.outcome[i].isWin == false) {
                fReward += 
                    uint256(e.outcome[i].weiRaised)
                    * fundPercent //funds share, %
                    * 1e6 //scale denominator
                    /100  //%
                    /1e6; //scale denominator
            }
        }


        //Settlement
        inBetsAmount -= fReward; //Consolidate account dec TODO analit?
        fundBalance  += fReward; //Consolidate account inc

        //Save that this event is settled
        eventsSettlement[_gameId][_eventId].fundReward = fReward;
        lastSettledCreator = e.creator;
        //Distribute fReward with all active funds
        uint256 totalPercent = _getFundRegistryTotalPoints();
        uint256 thisFundReward;
        // TODO May be we need check for 100%
        for (uint8 i = 0; i < fundsRegistry.length; i ++ ){
            if (fundsRegistry[i].state == FundState.Active) {
                thisFundReward = fReward 
                * fundsRegistry[i].sharePercent
                / totalPercent;
                _increaseTotalAmount(fundsRegistry[i].contractAddress, thisFundReward);
                //balances[fundsRegistry[i].contractAddress] += thisFundReward;
                require(
                    IFund(fundsRegistry[i].contractAddress).newReward(thisFundReward),
                    "Fail fund topup"
                );
            }

        }
    }

    
    function registerFund(address _newFund, uint256 _fundPercent, bool _needUpdates) external virtual onlyOwner {
        require(_fundPercent > 0, "Percent must be more then zero");
        require(_newFund != address(0), "No zero fund");
        fundsRegistry.push(
            Fund({
                contractAddress: _newFund,
                sharePercent: _fundPercent,
                state: FundState.Active,
                needUpdateWithUserState: _needUpdates
            })
        );
        require(IFund(_newFund).registerFund(), "Fail new fund register");
        emit FundStateChanged(_newFund, _fundPercent ,FundState.Active);
    }


    function changeFundState(
        uint256 _fundId, 
        uint256 _fundPercent, 
        FundState _state
    ) 
        external virtual onlyOwner 
    {
        Fund storage f = fundsRegistry[_fundId];
        f.sharePercent = _fundPercent;
        f.state = _state; 
        emit FundStateChanged(f.contractAddress, _fundPercent , _state);
    }

    

    //////////////////////////////////////////////////////////////////////////////////

    function getGamesCount() external view returns (uint256) {
        return games.length;
    }

    function getUsersBetsCount(address _user) external view returns(uint256) {
        return userBets[_user].length;
    }

    function getUsersBetByIndex(address _user, uint256 _index) external view returns (Bet memory) {
        return userBets[_user][_index];
    }

    function getUsersBetAmountByIndex(address _user, uint256 _index) external view returns (uint256) {
        return userBets[_user][_index].betAmount;
    }

    function getUserBalance(address _user) external view returns(uint256) {
        //TODO  may be need to refactor.just balances[_user]
        return _availableAmount(_user);
    }

    function getFund(uint256 _fundId) public view returns (Fund memory) {
        return fundsRegistry[_fundId];
    }

    function getFundCount() public view returns (uint256 fundsCount) {
        return fundsRegistry.length;
    }



    ////////////////////////////////////////////////////////
    /////                         Internals         ////////
    ////////////////////////////////////////////////////////

    /// used for update user's state in all funds that need  
    function _updateRewardPoints(address _user) internal {
        for (uint8 i = 0; i < fundsRegistry.length; i ++ ){
            if (
                   fundsRegistry[i].state == FundState.Active
                && fundsRegistry[i].needUpdateWithUserState
                && !isFund(_user)
            ) 
            
            {
                IFund(fundsRegistry[i].contractAddress).updateUserState(
                    _user
                );
            }
        }
    }

    function _addBet(address _user, Bet memory _bet) internal {
        Bet[] storage bets = userBets[_user];
        bets.push(_bet);

    }
     
    function _cancelBet(address _user, uint256 _betId) internal {
        Bet[] storage bets = userBets[_user];
        bets[_betId].currentState = BetState.Canceled;
    }

    function _increaseTotalAmount(address _user, uint256 _amount) internal {
        balances[_user] += _amount;
        totalStaked += _amount;
    }

    function _decreaseTotalAmount(address _user, uint256 _amount) internal {
        balances[_user] -= _amount;
        totalStaked -= _amount;
    }

    function _internalTransfer(address _from, address _to, uint256 _amount) internal {
        balances[_from] -= _amount;
        balances[_to]   += _amount;
    }


    function _availableAmount(address _user) internal view returns (uint256) {
        return balances[_user]; 
    }
    
    /// In most case thgis function must returns 100000 (100%)
    /// But there is NO hard coded - any points  make  take place
    function _getFundRegistryTotalPoints() internal view returns (uint256) {
        uint256 sum; 
        for (uint8 i = 0; i < fundsRegistry.length; i ++) {
            if (fundsRegistry[i].state == FundState.Active) {
               sum += fundsRegistry[i].sharePercent;
            }
        }
        return sum;
    }

    function isFund(address _user) internal view returns (bool) {
        for (uint256 i; i < fundsRegistry.length; i ++) {
            if (fundsRegistry[i].contractAddress == _user) {
                return true;
            }
        }
        return false;
    }

}