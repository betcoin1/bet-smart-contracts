// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

//import "./BetTypes.sol";
//import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/access/Ownable.sol";
import "../interfaces/IGamblePool.sol";
import "../interfaces/IFund.sol";

contract FundHiStaking01 is IFund {

    struct UserInfo {
        uint256 accumulatedUserPoints;  // represent user's accruedPoints
        uint256 lastUpdatedAtBlock;     // last block when user's state has changed
        uint256 lastBalance;            // subj
        uint256 claimed;                // claimed reward amount
    }

    struct RewardEpoch {
        uint256 rewardAmount;
        uint256 rewardClaimed;
        uint256 closedAtBlock;
        uint256 accumulatedTotalPoints;
        uint256 totalStakedAtEpochClose; // TODO think anout remove
    }
    
    uint256 constant public SCALE = 1e18;
    uint256 immutable public fundStartBlock; 

    bool public registeredInPool;
    uint256 public currentEpoch;
    uint256 public rewardForCurrentEpoch;

    IGamblePool public  gamblePool;

    mapping(address => mapping(uint256 => UserInfo)) public usersReward;
    mapping(address => uint256) public userBetCounts;
    RewardEpoch[] internal rewardEpochs;

    event UserRewardUpdated(address pool, address user, uint256 currentEpoch, uint256 newBalance);
    event UserRewardClaimed(address pool, address user, uint256 epoch, uint256 reward);
    event NewEpoch(uint256 closedEpoch, uint256 closedEpochReward);
    
    constructor () {
            fundStartBlock = block.number;
            rewardEpochs.push();
    }

    modifier onlyFund() {
      require(msg.sender == address(gamblePool));
      _;
    }

    function registerFund() external override returns (bool) {
        require(!registeredInPool, "Already registered");
        gamblePool = IGamblePool(msg.sender);
        registeredInPool = true;
        return registeredInPool;
    }

    function joinFund(address _user) external override returns (bool) {
        return true;
    }

    ///  Claim from last available 
    function claimReward(address _user) external override onlyFund 
        returns 
    (uint256 userRewardAmount) 
    {
        if (currentEpoch > 0) {
            return claimRewardForEpoch(_user, currentEpoch - 1);
        } else {
            return 0;
        }    
    }

    function claimRewardForEpoch(address _user, uint256 _epoch) 
        public 
        override 
        onlyFund 
        returns 
    (uint256 userRewardAmount)
    {
        require(_epoch < currentEpoch, "Cant claim from currentEpoch");
        require(usersReward[_user][_epoch].claimed == 0, "Epoch already claimed");
       
        // calc reward
        userRewardAmount = _getUserRewardInEpoch(_user, _epoch);
        usersReward[_user][_epoch].claimed = userRewardAmount;
        rewardEpochs[_epoch].rewardClaimed += userRewardAmount;
        emit UserRewardClaimed(address(this), _user, _epoch, userRewardAmount);
        return userRewardAmount;
    }

    function newReward(uint256 _amount) external override onlyFund returns (bool) {
        rewardForCurrentEpoch += _amount;
        return true;
    }

    function updateUserState(address _user) external override onlyFund {
        uint256 newBetCount = gamblePool.getUsersBetsCount(_user);
        
        // Check that user have new bets
        if (newBetCount > userBetCounts[_user]) {
            uint256 newBetAmount = gamblePool.getUsersBetAmountByIndex(_user, newBetCount -1);
            usersReward[_user][currentEpoch].accumulatedUserPoints += newBetAmount;
            userBetCounts[_user] = newBetCount;
            rewardEpochs[currentEpoch].accumulatedTotalPoints += newBetAmount;
        }
        emit UserRewardUpdated(address(this), _user, currentEpoch, usersReward[_user][currentEpoch].accumulatedUserPoints);
    
    }

    /// Close current epoch and make it available for reward claim
    function closeCurrentEpoch() external  {
        // TODO  maybe need restrict users who can call this
        // for example only pool owner-  check befor production
        // uint256 accTotalP = rewardEpochs[currentEpoch].accumulatedTotalPoints;
        rewardEpochs[currentEpoch].closedAtBlock = block.number;
        rewardEpochs[currentEpoch].rewardAmount = rewardForCurrentEpoch;
        rewardEpochs[currentEpoch].totalStakedAtEpochClose = gamblePool.inBetsAmount();
        emit NewEpoch(currentEpoch, rewardForCurrentEpoch);
        rewardForCurrentEpoch = 0;
        rewardEpochs.push();
        currentEpoch = rewardEpochs.length - 1;
        // rewardEpochs[currentEpoch].accumulatedTotalPoints = accTotalP;
    }

    function getAvailableReward(address _user) external view override returns (uint256) {
        if (currentEpoch == 0) {
            return 0;
        }
        return _getUserRewardInEpoch(_user, currentEpoch - 1);
    }

    function getAvailableReward(address _user, uint256 _epoch) external view override returns (uint256) {
        if (currentEpoch == 0) {
            return 0;
        }
        return _getUserRewardInEpoch(_user, _epoch);
    }

    function getUser(address _user) external view returns (UserInfo[] memory user) {
        uint256 count = getUserEpochCount(_user);
        UserInfo[] memory ui = new UserInfo[](count);
        count = 0;
        for (uint256 i; i <= currentEpoch; i ++) {
             if (usersReward[_user][i].accumulatedUserPoints >0 ){
                ui[count] = usersReward[_user][i];
                count ++;
            }
        }

        return ui;
    }

    // function getUserLastEpoch(address _user) public view returns (UserInfo memory user) {
    //     if (usersReward[_user].length == 0) {
    //        user = UserInfo(0,0,0,0);
    //     }  else {
    //        user = usersReward[_user][usersReward[_user].length -1];    
    //     }
    //     return user;
    // }

    function getUserEpochCount(address _user) public view returns (uint256) {
        uint256 count = 0;
        for (uint256 i; i <= currentEpoch; i ++) {
            // First we need calc epoch where user have make bets
            if (usersReward[_user][i].accumulatedUserPoints >0 ){
                count ++;
            }
        }
        return count;
    }

    function getRewardEpoch(uint256 _index) external view returns (RewardEpoch memory rewardEpoch) {
        rewardEpoch = rewardEpochs[_index];
        return rewardEpoch;    
    }
     
    function getRewardEpochCount() external view returns (uint256 rewardEpochCount) {
        rewardEpochCount = rewardEpochs.length;
        return rewardEpochCount;    
    } 

    function isJoined(address _user) external view override returns (bool joined) {
        return true;
    } 

    ////////////////// Internals ////////////////////////// 

    /// Main reward calclogiс 
    function _getUserRewardInEpoch(address _user, uint256 _epoch) internal view returns (uint256 r) {
        if (
              usersReward[_user][_epoch].accumulatedUserPoints == 0 ||
              _epoch >= currentEpoch
            ) 
        {
            r = 0;
        } else {

            r = usersReward[_user][_epoch].accumulatedUserPoints 
                * rewardEpochs[_epoch].rewardAmount 
                / rewardEpochs[_epoch].accumulatedTotalPoints
                - usersReward[_user][_epoch].claimed;
        }
        return r;    
    }    
}
