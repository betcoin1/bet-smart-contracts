// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

import "./BetTypes.sol";
import "OpenZeppelin/openzeppelin-contracts@4.1.0/contracts/access/Ownable.sol";

contract RewardModel01 is  Ownable {
    function getFundPercent() external view returns (uint256) {
        return 20;
    }

    function getEventCreatorPercent() external view returns (uint256) {
        return 20;
    }
}
