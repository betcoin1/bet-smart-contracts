## Betting application smart contracts
We are developing a Web 3.0 application for creating bets (binary only at the first stage) on cryptocurrency pairs available from oracles on the Binance Smart Chain. To participate in bets, users need to own the BEP20 `BET` token, which are deposited in a smart contract.

### Contracts
`BetToken`   - BEP20 and ERC20 compatible token
`GamblePool` - BET token staking pool for gambling participants
`BetPriceProvider` - price Oracle with chainlink integration  
`BetTypes` - abstruction for base types and enums  
`BinaryEvents` - game contract for simple binary bets

### Deployments

#### 20211108 Rinkeby TestNET, Game Logic with Binary Events and All Funds(ALFA)
**BetToken**  
https://rinkeby.etherscan.io/address/0x5ccf9d141Af74b2E5D7f5B8ed4d5Cd218D9e0e85#code  

**GamblePool**  
https://rinkeby.etherscan.io/address/0x39Eb7efE7F05D1cF80FBC11B4438F576FE5DD8B0#code  

**BetPriceProvider**  
https://rinkeby.etherscan.io/address/0x03321Ee6b6ac10EddB91d0bcD5BcC44EdFFC611c#code  

**BinaryEvents**  
https://rinkeby.etherscan.io/address/0x65c07554965e5592E4B8db6ABede844122EC2a31#code  

**RewardModel01**  
https://rinkeby.etherscan.io/address/0x564D23047e66BcC007dA401C8a736721EDFA3824#code  
----------Funds-------------------  

**FundHolders01**  
https://rinkeby.etherscan.io/address/0x2cF769733195aA6d1786663E6088f54e25Bfb611#code  


**FundFee**    
https://rinkeby.etherscan.io/address/0x8FA36CF39d641D925a1cb12722EA3577618585c0#code    

**fundLottery**  
https://rinkeby.etherscan.io/address/0xA9feF882889998aFC62f6b7Ac83bBe8ac141cA96#code   

**FundHiStaking01.**  
https://rinkeby.etherscan.io/address/0x33fE5773caf2AdEdb5c3E17D21563BFC361D75b5#code  

**FundCreators01**  
https://rinkeby.etherscan.io/address/0xC9802A978bD0b220dE1a8DCC78E21F024A6351D4#code  

**FundBurn01**  
https://rinkeby.etherscan.io/address/0xF32F9904ea6f7f1F9E3374718C23C5F1718DE05b#code  



#### 20210626 Rinkeby TestNET, Game Logic with Binary Events and One Fund
**GamblePool**
https://rinkeby.etherscan.io/address/0x02e5A4E9380d9d8D2111617e805A06329e00EB66#code

**BinaryEvents**
https://rinkeby.etherscan.io/address/0xf1b81C9A17A93134c76aa689bdDCce17ce1a4517#code

**BetToken**
https://rinkeby.etherscan.io/address/0x2873507dd1c70d036dcef5056b5d4fd8650b0d05#code

**BetPriceProvider**
https://rinkeby.etherscan.io/address/0x69A46708917ee34C996Ebf55E8891b135A202f69#code